
extension FormValidator on String {
  /// Check validity of email using regexp
  /// Must be like this: something@something.code
  bool isValidEmail() {
    return RegExp(
      r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$')
      .hasMatch(this);
  }

  /// Check validity of username using regexp
  /// Must have no special characters and must have a length between 3 and 30
  bool isValidUsername() {
    return RegExp(r"^[\w\W]{3,30}$").hasMatch(this);
  }

  /// Check validity of activity name using regexp
  /// Must have no special characters and must have a length between 1 and 30
  bool isValidActivityName() {
    return RegExp(r"^[\w\W]{1,30}$").hasMatch(this);
  }

  bool isAlertMessageValid() {
    return RegExp(r"^[\w\W]{0,140}$").hasMatch(this);
  }
}