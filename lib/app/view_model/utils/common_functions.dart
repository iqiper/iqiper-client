import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/view_model/utils/constraintList.dart';

/// Initialize a list of constraints of type [listType] and data [constraintListJson]
/// Return this list
List<Constraint> getConstraintsFromStringList(List<String> listType, dynamic constraintListJson) {
  List<Constraint> cl = [];
  for (int i = 0 ; i < listType.length ; i++) {
    cl = addSelectedConstraintsWithData(cl, listType[i], constraintListJson[i]);
  }
  return cl;
}

/// Take the list of constraints [clist], add a constraint of type [typeValue] and data [constraintJson]
/// [offsetValuesFromServerToClient] must be true if fetched timer needs to be corrected
/// Return the list with the newly added constraint
List<Constraint> addSelectedConstraintsWithData(List<Constraint> clist, String typeValue, dynamic constraintJson) {
  List<Constraint> tmp =
  listOfConstraints(null).where((element) => element.type == typeValue).toList();

  Constraint tmpConstraint = new Constraint(type: tmp[0].type, icon: tmp[0].icon, description: tmp[0].description);
  if (constraintJson['id'] != null) {
    tmpConstraint.setId(constraintJson['id']);
  }
  tmpConstraint.setData(constraintJson);
  clist.add(tmpConstraint);
  return clist;
}

/// Return the list of constraints [clist] without the constraint with index [index]
List<Constraint> removeSelectedConstraints(List<Constraint> clist, int index) {
  clist.removeAt(index);
  return clist;
}
