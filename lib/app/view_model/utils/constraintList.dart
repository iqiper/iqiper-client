import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/Constraint.dart';

/// Return a list of all possible types of constraints
List<Constraint> listOfConstraints(BuildContext context) {
  return <Constraint>[
    new Constraint(type: "timer", icon: 60031, description: 'timer description'),
    // new Constraint("geolocalisation", Icon(Icons.edit_location)),
    // new Constraint("path", Icon(Icons.departure_board)),
    // new Constraint("batterie", Icon(Icons.battery_alert)),
    // new Constraint("IOT", Icon(Icons.child_care))
  ];
}