import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/view/custom_widget/contactFormOpener.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:iqiperclient/app/view_model/utils/validator.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:iqiperclient/app/view/form/add_contact.dart';

class ContactForm extends StatefulWidget {
  final String form;
  final bool isPost;
  final String intro;
  final String label;
  final String hint;
  final String button;
  final Contact contact;

  const ContactForm({Key key, this.form, this.isPost, this.intro, this.label, this.hint, this.button, this.contact}) : super(key: key);

  @override
  ContactFormState createState() => ContactFormState();
}

class ContactFormState extends State<ContactForm> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _input = TextEditingController();
  AddContactState addContactState;
  ContactFormOpenerState contactFormOpenerState;

  bool serverDown;
  String messageSnack;

  @override
  void dispose() {
    _input.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    serverDown = false;
    messageSnack = "";
    addContactState = context.findAncestorStateOfType<AddContactState>();
    contactFormOpenerState = context.findAncestorStateOfType<ContactFormOpenerState>();
  }

  /// This widget is a form that is used to setup a new contact or update an existing one
  /// The user can choose to add a contact via email or iqiper user
  /// The user can setup a custom nickname for this contact and set its favorite status
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);
    
    return Container(
      width: 320,
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15.0),
              child: SizedBox(
                width: double.infinity,
                child: Text(
                  widget.intro,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    color: DynamicTheme.of(context).data.accentColor,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20.0),
              child: TextFormField(
                key: Key(widget.form + 'ContactsForm'),
                controller: _input,
                validator: (value) {
                  if (widget.form == "email") {
                    return value.isValidEmail() ? null : AppLocalizations.of(context).translate('contact-mail-not-correct');
                  } else if (widget.form == "username" || widget.form == "nickname") {
                    return value.isValidUsername() ? null : AppLocalizations.of(context).translate('username-not-correct');
                  }
                  return null;
                },
                style: TextStyle(
                  color: DynamicTheme.of(context).data.accentColor,
                ),
                cursorColor: DynamicTheme.of(context).data.primaryColorLight,
                decoration: InputDecoration(
                  labelText: widget.label,
                  labelStyle:TextStyle(color: DynamicTheme.of(context).data.accentColor),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                  ),
                  focusedErrorBorder: UnderlineInputBorder(borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight)),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                  ),
                  border: UnderlineInputBorder(
                    borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                  ),
                  hintText: widget.hint,
                  hintStyle:TextStyle(color: DynamicTheme.of(context).data.primaryColorLight),
                  errorStyle: TextStyle(color: DynamicTheme.of(context).data.primaryColorLight),
                  errorBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: DynamicTheme.of(context).data.accentColor),
                  ),
                ),
              ),
            ),
            widget.form == "nickname" && widget.isPost
              ? Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:
                    [
                      Text(
                        AppLocalizations.of(context).translate('contact-set-favorite'),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 18.0,
                          color: DynamicTheme.of(context).data.primaryColorLight,
                        ),
                      ),
                      IconButton(
                        hoverColor: Color.fromRGBO(0, 0, 0, 0),
                        onPressed: () {
                          setState(() {
                            addContactState.newContact.setFavorite();
                          });
                        },
                        icon: addContactState.newContact.favorite
                            ? Icon(
                                IconData(59938, fontFamily: 'MaterialIcons'),
                                color: DynamicTheme.of(context).data.accentColor,
                              )
                            : Icon(
                                IconData(59939, fontFamily: 'MaterialIcons'),
                                color: DynamicTheme.of(context).data.primaryColorLight,
                              ),
                      )
                    ]
                  ),
                )
            : Container(),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 35, 0, 35),
              child: Container(
                height: 50,
                width: 200,
                child:
                contactFormOpenerState.isLoading
                ? Loading()
                : RaisedButton(
                  padding: EdgeInsets.only(top: 1),
                  child: Text(
                    widget.button,
                    key: Key(widget.form + 'ValidContactsForm'),
                    style: TextStyle(
                      color: DynamicTheme.of(context).data.primaryColor,
                    ),
                  ),
                  color: DynamicTheme.of(context).data.primaryColorLight,
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      if (widget.isPost) {
                        if (widget.form == "email") {
                          addContactState.newContact.setEmail(removeDiacritics(_input.text));
                          addContactState.setState(() {
                            addContactState.steps = 1;
                          });
                        } else if (widget.form == "username") {
                          addContactState.setState(() {
                            addContactState.newContact.setUsername(removeDiacritics(_input.text));
                          });
                          // searchByUsername
                          contactFormOpenerState.setState(() {
                            contactFormOpenerState.isLoading = true;
                          });
                          final response = await addContactState.newContact.searchByUsername(ms.api);
                          if (response.statusCode == 200) {
                            addContactState.setState(() {
                              addContactState.potentialUsers = (json.decode(response.body) as List).map((data) => new Contact.userFromJson(data)).toList();
                              // get a list so open search by user widget
                              addContactState.steps = 1;
                            });
                          } else {
                            if (response.statusCode == 400) {
                              messageSnack = AppLocalizations.of(context).translate('error-occured');
                            } else if (response.statusCode == 500) {
                              serverDown = true;
                              messageSnack = "";
                            }
                            addContactState.setState(() {
                              addContactState.steps = 0;
                            });
                          }
                          contactFormOpenerState.setState(() {
                            contactFormOpenerState.isLoading = false;
                          });
                        }
                        if (serverDown) {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(content: Text(
                              AppLocalizations.of(context).translate('server-error'),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                ),
                              ),
                              backgroundColor: DynamicTheme.of(context).data.errorColor,
                            )
                          );
                        }
                        if (messageSnack != "") {
                          showDurationDialog(context, messageSnack, 1);
                        }
                      }
                      if (widget.form == "nickname") {
                        contactFormOpenerState.setState(() {
                          contactFormOpenerState.isLoading = true;
                        });
                        if (widget.isPost) {
                          addContactState.newContact.setNickname(removeDiacritics(_input.text));
                          final response = await addContactState.newContact.createContact(ms.api);
                          if (response.statusCode == 201) {
                            messageSnack = AppLocalizations.of(context).translate('activityform-contacts');
                            Provider.of<ContactProvider>(context, listen: false).add(Contact.fromJson(json.decode(response.body), 'id'));
                          } else {
                            if (response.statusCode == 400) {
                              messageSnack = AppLocalizations.of(context).translate('activityform-contacts-fail');
                            } else if (response.statusCode == 500) {
                              serverDown = true;
                              messageSnack = "";
                            }
                          }
                        } else {
                          final response = await widget.contact.modifyContactNickname(removeDiacritics(_input.text), ms.api);
                          if (response.statusCode == 200) {
                            messageSnack = AppLocalizations.of(context).translate('activityform-contacts');
                            widget.contact.nickname = removeDiacritics(_input.text);
                            Provider.of<ContactProvider>(context, listen: false).update(widget.contact);
                          } else {
                            if (response.statusCode == 400) {
                              messageSnack = AppLocalizations.of(context).translate('activityform-contacts-fail');
                            } else if (response.statusCode == 500) {
                              serverDown = true;
                              messageSnack = "";
                            }
                          }
                        }
                        contactFormOpenerState.setState(() {
                          _input.text = "";
                          contactFormOpenerState.isLoading = false;
                        });
                        if (serverDown) {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(content: Text(
                              AppLocalizations.of(context).translate('server-error'),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                ),
                              ),
                              backgroundColor: DynamicTheme.of(context).data.errorColor,
                            )
                          );
                        }
                        if (messageSnack != "") {
                          showDurationDialog(context, messageSnack, 1);
                        }
                          Future.delayed(Duration(seconds: 1), () {
                            WidgetsBinding.instance.addPostFrameCallback((_) => setState(() {
                              Navigator.pop(context, messageSnack); 
                            }));
                          });
                      }
                      setState(() {
                        messageSnack = "";
                        serverDown = false;
                      });
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
