import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/rendering.dart';
import 'package:iqiperclient/app/view/custom_widget/activityFormOpener.dart';
import 'package:iqiperclient/app/view/custom_widget/contactSelectionForm.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/FormValues.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/view_model/utils/validator.dart';
import 'package:iqiperclient/app/view_model/utils/common_functions.dart';
import 'package:iqiperclient/app/view_model/utils/constraintList.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:iqiperclient/app/view/custom_widget/timer.dart';
import 'package:iqiperclient/app/view/custom_widget/iconSelector.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:diacritic/diacritic.dart';

class ActivityForm extends StatefulWidget {
  final FormValues initValues;
  final bool isPost;
  final bool isModel;
  ActivityForm({Key key, this.initValues, this.isPost, this.isModel}) : super(key: key);

  @override
  ActivityFormState createState() => ActivityFormState();
}

class ActivityFormState extends State<ActivityForm> {
  FormValues _formValue;
  bool serverDown;

  // msg form
  String oldActivityMessage;
  bool _msgError = false;
  String _msgErrorText;

  String messageSnack;
  String description;
  bool info;
  List<String> message;
  TextEditingController _inputTitle = TextEditingController();
  TextEditingController _inputMsg = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      serverDown = false;
      messageSnack = "";
      description = "";
      info = false;
      _formValue = new FormValues.clone(widget.initValues);
      oldActivityMessage = widget.initValues.message;
      _inputMsg.text = (_inputMsg.text.isEmpty) ? widget.initValues.message : _inputMsg.text;
    });
  }

  @override
  void dispose() {
    _inputTitle.dispose();
    _inputMsg.dispose();
    super.dispose();
  }

  void changeIcon(int newIcon) {
    setState(() {
      _formValue.icon = newIcon;
    });
  }

  /// This widget is a form that is used to add or update an activity or an activity model
  /// It contains 4 step: title and icon, contacts, alert_message, constraints
  @override
  Widget build(BuildContext context) {
    ActivityFormOpenerState activityFormOpenerState = context.findAncestorStateOfType<ActivityFormOpenerState>();

    message = [
      AppLocalizations.of(context).translate('activityform-info'),
      AppLocalizations.of(context).translate('activityform-info-fail'),
      AppLocalizations.of(context).translate('activityform-contacts'),
      AppLocalizations.of(context).translate('activityform-contacts-fail'),
      AppLocalizations.of(context).translate('activityform-constraints'),
      AppLocalizations.of(context).translate('activityform-constraints-fail'),
      widget.isModel ? AppLocalizations.of(context).translate('activityform-activity-model') :  AppLocalizations.of(context).translate('activityform-activity'),
      widget.isModel ? AppLocalizations.of(context).translate('activityform-activity-model-fail') :  AppLocalizations.of(context).translate('activityform-activity-fail'),
      AppLocalizations.of(context).translate('activityform-msg'),
      AppLocalizations.of(context).translate('activityform-msg-fail'),
    ];
    MainState ms = Provider.of<MainState>(context, listen: false);

    /// Return a false if any of the constraints in list [constraints] are not valid
    bool selectedConstraintsSetUp(List<Constraint> constraints) {
      bool tmp = true;
      constraints.forEach((element) {
        if (!element.isValid) {
          tmp = false;
        }
      });
      return tmp;
    }

    /// Setup a constraint widget form its type
    /// If no type are valid then return a null container
    settingConstraint(BuildContext context, Constraint c, int i, bool test) {
      if (c.type == "timer") {
        return new Timer(constraint: c, isModel: widget.isModel, index: i, test: test, isPost: widget.isPost);
      }
      return Container();
    }

    /// Delete the contact [cTmp] from this activityForm
    deleteContact(Contact cTmp) async {
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = true;
      });
      var response;
      if (widget.isModel) {
        response = await _formValue.deleteContactFromModel(cTmp.uid, ms.api);
      } else {
        response = await widget.initValues.deleteActivityContact(cTmp.uid, ms.api);
      }
      if (response.statusCode == 200) {
        messageSnack = message[2];
      } else {
        if (response.statusCode == 400) {
          messageSnack = message[3];
        } else if (response.statusCode == 500) {
          serverDown = true;
          messageSnack = "";
        }
      }
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = false;
      });
      return response;
    }

    /// Check any changement in the title, alert_message and icon
    /// If changements are detected then update those on the server
    updateInfos() async {
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = true;
      });
      var response;
      if (widget.initValues.title != _formValue.title) {
        if (widget.isModel) {
          response = await _formValue.updateActivityModelName(ms.api);
        } else {
          response = await _formValue.updateActivityName(ms.api);
        }
        if (response.statusCode == 200) {
          messageSnack = message[0];
        } else {
          if (response.statusCode == 400) {
            messageSnack = message[1];
          } else if (response.statusCode == 500) {
            serverDown = true;
            messageSnack = "";
          }
        }
        activityFormOpenerState.setState(() {
          activityFormOpenerState.isLoading = false;
        });
      }
      if (_formValue.message != oldActivityMessage) {
        if (widget.isModel) {
          response = await _formValue.updateActivityModelAlertMessage(ms.api);
        } else {
          response = await _formValue.updateActivityAlertMessage(ms.api);
        }
        if (response.statusCode == 200) {
          messageSnack = message[8];
        } else {
          if (response.statusCode == 400) {
            messageSnack = message[9];
          } else if (response.statusCode == 500) {
            serverDown = true;
            messageSnack = "";
          }
        }
        activityFormOpenerState.setState(() {
          activityFormOpenerState.isLoading = false;
        });
      }
      if (widget.initValues.icon != _formValue.icon) {
        if (widget.isModel) {
          response = await  _formValue.updateActivityModelIcon(ms.api);
        } else {
          response = await _formValue.updateActivityIcon(ms.api);
        }
        if (response.statusCode == 200) {
          messageSnack = message[0];
        } else {
          if (response.statusCode == 400) {
            messageSnack = message[1];
          } else if (response.statusCode == 500) {
            serverDown = true;
            messageSnack = "";
          }
        }
      }
      return response;
    }

    /// Determine if form is for activities or models
    /// Then add contact [cTmp] to server
    addContact(Contact cTmp) async {
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = true;
      });
      var response;
      if (widget.isModel) {
        response =  await _formValue.postContactToModel(cTmp.uid, ms.api);
      } else {
        response = await _formValue.postContact(cTmp.uid, ms.api);
      }
      if (response.statusCode == 200) {
        messageSnack = message[2];
      } else {
        if (response.statusCode == 400) {
          messageSnack = message[3];
        } else if (response.statusCode == 500) {
          serverDown = true;
          messageSnack = "";
        }
      }
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = false;
      });
      return response;
    }

    /// Determine if form is for activities or models
    /// Then add first constraint from list [tmpCl] to server
    addConstraint(Constraint tmpCl) async {
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = true;
      });
      var response;
      if (widget.isModel) {
        response = await _formValue.postConstraintToModel(json.encode(tmpCl.postConstraintModelToJson()), ms.api);
      } else {
        response = await _formValue.postConstraint(json.encode(tmpCl.postConstraintToJson()), ms.api);
      }
      if (response.statusCode == 201) {
        messageSnack = message[4];
      } else {
        if (response.statusCode == 400) {
          messageSnack = message[5];
        } else if (response.statusCode == 500) {
          serverDown = true;
          messageSnack = "";
        }
      }
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = false;
      });
      return response;
    }

    /// Determine if form is for activities or models
    /// Then delete constraint with index [key] from list [tmpCl]
    deleteConstraint(Constraint constraint, int key) async {
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = true;
      });
      var response;
      if (widget.isModel) {
        response = await _formValue.deleteConstraintFromModel(constraint.id, ms.api);
      } else {
        response = await _formValue.deleteActivityConstraint(constraint.id, ms.api);
      }
      if (response.statusCode == 200) {
        messageSnack = message[4];
        setState(() {
          _formValue.constraints = removeSelectedConstraints(_formValue.constraintsEdit, key);
          _formValue.constraintError = false;
        });
      } else {
        if (response.statusCode == 400) {
          messageSnack = message[5];
        } else if (response.statusCode == 500) {
          serverDown = true;
          messageSnack = "";
        }
      }
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = false;
      });
      return response;
    }

    /// Determine if form is for activities or models
    /// Then update constraint [constraint] in this form
    updateConstraint(Constraint constraint) async {
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = true;
      });
      var response;
      if (widget.isModel) {
        response = await _formValue.updateConstraintFromModel(constraint.id, json.encode(constraint.postConstraintModelToJson()), ms.api);
      } else {
        response = await _formValue.updateActivityConstraint(constraint.id, json.encode(constraint.postConstraintToJson()), ms.api);
      }
      if (response.statusCode == 200) {
        messageSnack = message[4];
      } else {
        if (response.statusCode == 400) {
          messageSnack = message[5];
        } else if (response.statusCode == 500) {
          serverDown = true;
          messageSnack = "";
        }
      }
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = false;
      });
      return response;
    }

    /// Post activity from this form to server
    postActivity() async {
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = true;
      });
      var response;
      if (widget.isModel) {
        response = await _formValue.postNewActivityModel(ms.api);
      } else {
        response = await _formValue.postNewActivity(ms.api);
      }
      if (response.statusCode == 201) {
        messageSnack = message[6];
      } else {
        if (response.statusCode == 400) {
          messageSnack = message[7];
        } else if (response.statusCode == 500) {
          serverDown = true;
          messageSnack = "";
        }
      }
      activityFormOpenerState.setState(() {
        activityFormOpenerState.isLoading = false;
      });
      return response;
    }

    /// This widget is an item for the list of contacts that can be selected and be added to an activity or a model
    /// The concerned contact is from the list of contacts in formValue with index [index]
    Widget _selectedContactListItem(BuildContext context, int index) {
      return Container(
          margin: EdgeInsets.symmetric(vertical: 5),
          child: _formValue.contacts[index].isSelected
              ? Center(
                  child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 300,
                        minHeight: 100,
                        maxWidth: 400,
                        maxHeight: 100,
                      ),
                      child: Container(
                          key:  (index + 1 == _formValue.contacts.length) ? Key("FinalActivityContactsListTile") : Key("ActivityContactsListTile" + index.toString()),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(
                              color: DynamicTheme.of(context).data.primaryColor,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.all(Radius.circular(25.0))),
                          child: ListTile(
                            leading: Padding(
                              padding: EdgeInsets.fromLTRB(0, 13, 0, 0),
                                child: Icon(
                                  IconData(62399, fontFamily: 'MaterialIcons'),
                                  key:  (index + 1 == _formValue.contacts.length) ? Key("FinalActivityContactsListTileLeading") : Key("ActivityContactsListTileLeading" + index.toString()),
                                  size: 45,
                                  color: DynamicTheme.of(context).data.accentColor,
                              ),
                            ),
                            title: Padding(
                            padding: _formValue.contacts[index].subtitle == "" 
                            ? EdgeInsets.fromLTRB(0, 34, 0, 0)
                            :  EdgeInsets.fromLTRB(0, 17, 0, 0),
                            child: Text(
                              _formValue.contacts[index].nickname,
                              key:  (index + 1 == _formValue.contacts.length) ? Key("FinalActivityContactsListTileNickname") : Key("ActivityContactsListTileNickname" + index.toString()),
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 20.0,
                                color: DynamicTheme.of(context).data.primaryColorLight,
                              ),
                            ),
                            ),
                            subtitle: Padding(
                              padding: EdgeInsets.fromLTRB(0, 13, 0, 0),
                              child: Text(
                                _formValue.contacts[index].subtitle == null ? "" : _formValue.contacts[index].subtitle,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 15.0,
                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                ),
                              ),
                            ),
                            trailing: Padding(
                              padding: EdgeInsets.fromLTRB(0, 9, 0, 0),
                              child: IconButton(
                                key:  (index + 1 == _formValue.contacts.length) ? Key("FinalActivityContactsListTileTrailing") : Key("ActivityContactsListTileTrailing" + index.toString()),
                                icon: Icon(IconData(59041, fontFamily: 'MaterialIcons'), color: DynamicTheme.of(context).data.primaryColorDark),
                                tooltip: AppLocalizations.of(context).translate('contact-delete'),
                                hoverColor: Color.fromRGBO(0, 0, 0, 0),
                                onPressed: () async {
                                  Contact tmpContact = _formValue.contacts[index];
                                  // if at least one contact is selected
                                  if (_formValue.contacts.length > 1) {
                                    setState(() {
                                        // mark contact unselected
                                        _formValue.contactSelectionState.forEach((element) {
                                          if (element.uid == tmpContact.uid) {
                                            element.isSelected = false;
                                          }
                                        });
                                      });
                                      // if updating an existing activity or model, post directly the modification
                                      if (widget.isPost) {
                                        _formValue.contacts.removeWhere((item) => item.uid == tmpContact.uid);
                                      } else {
                                        final response = await deleteContact(tmpContact);
                                        if (response.statusCode == 200) {
                                          // remove contact from activity contact list and change it in provider
                                          _formValue.contacts.removeWhere((item) => item.uid == tmpContact.uid);
                                          if (widget.isModel) {
                                            Provider.of<ModelProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                          } else {
                                            Provider.of<ActivityProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                          }
                                        } else {
                                          setState(() {
                                            _formValue.contactSelectionState.forEach((element) {
                                              if (element.uid == tmpContact.uid) {
                                                element.isSelected = true;
                                              }
                                            });
                                          });
                                        if (serverDown) {
                                          Scaffold.of(context).showSnackBar(
                                            SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                                              style: TextStyle(
                                                  fontSize: 18.0,
                                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                                ),
                                              ),
                                              backgroundColor: DynamicTheme.of(context).data.errorColor,
                                            )
                                          );
                                        }
                                      }
                                    }
                                    setState(() {
                                      serverDown = false;
                                      messageSnack = "";
                                      _formValue.contactError = false;
                                    });
                                  } else {
                                    setState(() {
                                      _formValue.contactError = true;
                                    });
                                  }
                                },
                              ),
                            ),
                          ))))
              : Container());
    }

    // Step 1
    /// This widget is used to set or update the name of the activity or model
    Widget _nameForm(BuildContext context) {
      final _formKey = GlobalKey<FormState>();
      String tmpTitle = _formValue.title;
      
      return LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      width: double.infinity,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                        Text(
                          "1.",
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColorLight),
                        ),
                        Expanded(
                          child: Text(
                            AppLocalizations.of(context).translate('activityform-confirm-activity-title'),
                            textAlign: TextAlign.right,
                            style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColorLight),
                          ),
                        ),
                      ])),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    padding: EdgeInsets.only(bottom: 0.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 0),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 300,
                        minHeight: 100,
                        maxWidth: 400,
                        maxHeight: 150,
                      ),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                            color: DynamicTheme.of(context).data.primaryColor,
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(25.0))),
                        child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          alignment: WrapAlignment.center,
                          direction: Axis.horizontal,
                          runAlignment: WrapAlignment.center,
                          spacing: 5,
                          children: [
                            IconButton(
                              icon: Icon(IconData(_formValue.icon, fontFamily: 'MaterialIcons'),
                                  color: DynamicTheme.of(context).data.accentColor, size: 35),
                              hoverColor: Color.fromRGBO(0, 0, 0, 0),
                              tooltip: AppLocalizations.of(context).translate('activity-edit-icon'),
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return IconSelector(changeIcon);
                                  }
                                );
                              },
                            ),
                            Form(
                              key: _formKey,
                              child: SizedBox(
                                width: 150,
                                child: TextFormField(
                                  key: Key('ActivityTitleForm'),
                                  controller: _inputTitle,
                                  validator: (value) {
                                    if (value.isEmpty) return null;
                                    return value.isValidActivityName() ? null : AppLocalizations.of(context).translate('activity-name-error');
                                  },
                                  onChanged: (value) {
                                    tmpTitle = removeDiacritics(value);
                                  },
                                  cursorColor: DynamicTheme.of(context).data.primaryColorLight,
                                  style: TextStyle(
                                    color: DynamicTheme.of(context).data.accentColor, fontSize: 18
                                  ),
                                  decoration: InputDecoration(
                                    labelText: _formValue.title,
                                    labelStyle: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight)),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                    ),
                                    hintText: AppLocalizations.of(context).translate('activity-edit-title'),
                                    hintStyle: TextStyle(color: DynamicTheme.of(context).data.primaryColorLight),
                                    errorStyle: TextStyle(color: DynamicTheme.of(context).data.primaryColorLight),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: DynamicTheme.of(context).data.accentColor),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ]),
                ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: 100,
                    minHeight: 50,
                    maxWidth: 400,
                    maxHeight: 150,
                  ),
                  child: Center(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 320,
                        minHeight: 70,
                        maxWidth: 400,
                        maxHeight: widget.isModel ? 85 : 70,
                      ),
                      child: activityFormOpenerState.isLoading
                        ? Loading()
                        : RaisedButton(
                          key: Key('ValidActivityTitleForm'),
                          child: Text(
                            AppLocalizations.of(context).translate('save'),
                            style: TextStyle(
                              color: ms.theme.appThemeName == "Standard" 
                              ? DynamicTheme.of(context).data.primaryColor 
                              : DynamicTheme.of(context).data.accentColor,
                            ),
                          ),
                          color: DynamicTheme.of(context).data.primaryColorLight,
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              tmpTitle = (_inputTitle.text.isEmpty) ? tmpTitle : _inputTitle.text;
                              _formValue.title = removeDiacritics(tmpTitle);
                              var response;
                              if (widget.isPost) {
                              } else {
                                response = await updateInfos();
                              }
                              if (response != null) {
                                if (response.statusCode == 200) {
                                  // update activity or model in provider
                                  if (widget.isModel) {
                                    Provider.of<ModelProvider>(context, listen: false).update(_formValue.getNewActivity((json.decode(response.body))['id'], widget.isModel));
                                  } else {
                                    Provider.of<ActivityProvider>(context, listen: false).update(_formValue.getNewActivity((json.decode(response.body))['id'], widget.isModel));
                                  }
                                } 
                                if (serverDown) {
                                  Scaffold.of(context).showSnackBar(
                                    SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          color: DynamicTheme.of(context).data.primaryColorLight,
                                        ),
                                      ),
                                      backgroundColor: DynamicTheme.of(context).data.errorColor,
                                    )
                                  );
                                }
                                if (messageSnack != "") {
                                  showDurationDialog(context, messageSnack, 1);
                                }
                              }
                              setState(() {
                                activityFormOpenerState.isLoading = false;
                                _inputTitle.text = "";
                                if (serverDown) {
                                  serverDown = false;
                                } else {
                                  _formValue.step += 1;
                                }
                              });
                            }
                        }),
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: widget.isPost
                          ? null
                          : () {
                              setState(() {
                                _formValue.step = 1;
                              });
                            },
                      child: Container(
                          key: Key('ActivityFrom0to1'),
                          padding: EdgeInsets.symmetric(vertical: 3),
                          width: double.infinity,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                            Text(
                              "2.",
                              textAlign: TextAlign.left,
                              style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                            Expanded(
                              child: Text(
                                AppLocalizations.of(context).translate('activityform-confirm-recipient'),
                                textAlign: TextAlign.right,
                                style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                              ),
                            ),
                          ])),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                      ),
                    ),
                    GestureDetector(
                      onTap: widget.isPost
                          ? null
                          : () {
                              setState(() {
                                _formValue.step = 2;
                              });
                            },
                      child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(vertical: 3),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "3.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activity-select-setup-messages'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ]),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                      ),
                    ),
                    GestureDetector(
                      onTap: widget.isPost
                          ? null
                          : () {
                              setState(() {
                                _formValue.step = 3;
                              });
                            },
                      child: Container(
                        key: Key('ActivityFrom0to2'),
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(vertical: 3),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "4.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activity-select-setup-contraints'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ]),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      });
    }

    // Step 2
    /// This widget is used to set or update the list of contacts that is associated with the activity or model
    Widget _setUpContacts(BuildContext context) {
      return LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: 
                  Column(
                  mainAxisSize: MainAxisSize.max, mainAxisAlignment: 
                  MainAxisAlignment.start, 
                  children: <Widget>[
                    GestureDetector(
                      onTap: widget.isPost
                          ? null
                        : () {
                            setState(() {
                              _formValue.step = 0;
                            });
                          },
                    child: Container(
                        key: Key('ActivityFrom1to0'),
                        padding: EdgeInsets.symmetric(vertical: 3),
                        width: double.infinity,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "1.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activityform-confirm-activity-title'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ])),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                      width: double.infinity,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                        Text(
                          "2.",
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColorLight),
                        ),
                        Expanded(
                          child: Text(
                            AppLocalizations.of(context).translate('activityform-confirm-recipient'),
                            textAlign: TextAlign.right,
                            style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColorLight),
                          ),
                        ),
                      ])),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 0),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 200,
                        minHeight: 70,
                        maxWidth: 400,
                        maxHeight: 85,
                      ),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 5.0),
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        child: RaisedButton(
                          key: Key('ActivityAddContactButton'),
                          elevation: 5,
                          padding: EdgeInsets.only(top: 1),
                          child: Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              alignment: WrapAlignment.center,
                              direction: Axis.horizontal,
                              children: [
                                Text(AppLocalizations.of(context).translate('contact-add') + ' ', style: TextStyle(color: DynamicTheme.of(context).data.accentColor, fontSize: 18)),
                                Icon(IconData(59294, fontFamily: 'MaterialIcons'), color: DynamicTheme.of(context).data.accentColor),
                              ]),
                          color: DynamicTheme.of(context).data.primaryColor,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                          onPressed: () async {
                            // SELECT CONTACT FORM
                            final contacting = await showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return ContactSelection(contactSelectionState: _formValue.contactSelectionState);
                            });
                            if (contacting != null) {
                              // add a contact to the previous list
                              bool tmp = !contacting.isSelected;
                              // if contact selected
                                // and if contact is not in previous list
                                if (tmp && !_formValue.contacts.contains(contacting)) {
                                  // mark contact selected
                                  setState(() {
                                    _formValue.contactSelectionState.forEach((element) {
                                      if (element.uid == contacting.uid) {
                                        element.isSelected = tmp;
                                      }
                                    });
                                  });
                                  if (widget.isPost) {
                                    _formValue.contacts.add(contacting);
                                  } else {
                                    // if updating an existing activity or model, post it directly
                                    final response = await addContact(contacting);
                                    if (response != null) {
                                      if (response.statusCode == 200) {
                                        // add contact to final list
                                        contacting.isSelected = tmp;
                                        _formValue.contacts.add(contacting);
                                        // update contact in activity in provider list
                                        if (widget.isModel) {
                                          Provider.of<ModelProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                        } else {
                                          Provider.of<ActivityProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                        }
                                      } else {
                                        setState(() {
                                          _formValue.contactSelectionState.forEach((element) {
                                            if (element.uid == contacting.uid) {
                                              element.isSelected = !tmp;
                                            }
                                          });
                                        });
                                      }
                                    }
                                  }
                              } else {
                                // contact was already selected
                                // check if there is at least one contact in list
                                if (_formValue.contacts.length > 1) {
                                // if (_formValue.contacts.contains(contacting)) {
                                  // if not remove from previous list
                                  setState(() {
                                    // mark contact unselected
                                    _formValue.contactSelectionState.forEach((element) {
                                      if (element.uid == contacting.uid) {
                                        element.isSelected = tmp;
                                      }
                                    });
                                  });
                                  // if updating an existing activity or model, post directly the modification
                                  if (widget.isPost) {
                                      _formValue.contacts.removeWhere((item) => item.uid == contacting.uid);
                                  } else {
                                    final response = await deleteContact(contacting);
                                    if (response.statusCode == 200) {
                                      // remove contact from activity contact list and change it in provider
                                      _formValue.contacts.removeWhere((item) => item.uid == contacting.uid);
                                      if (widget.isModel) {
                                        Provider.of<ModelProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                      } else {
                                        Provider.of<ActivityProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                      }
                                    } else {
                                      setState(() {
                                        _formValue.contactSelectionState.forEach((element) {
                                          if (element.uid == contacting.uid) {
                                            element.isSelected = !tmp;
                                          }
                                        });
                                      });
                                    }
                                  }
                                } else {
                                  setState(() {
                                    _formValue.contactError = true;
                                  });
                                }
                              }
                              if (serverDown) {
                                Scaffold.of(context).showSnackBar(
                                  SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        color: DynamicTheme.of(context).data.primaryColorLight,
                                      ),
                                    ),
                                    backgroundColor: DynamicTheme.of(context).data.errorColor,
                                  )
                                );
                              }
                              if (messageSnack != "") {
                                showDurationDialog(context, messageSnack, 1);
                              }
                              setState(() {
                                serverDown = false;
                                _formValue.contactError = false;
                                messageSnack = "";
                              });
                            }
                          },
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: _formValue.contactError
                        ? Text(
                            AppLocalizations.of(context).translate('activity-assign-contact'),
                          key: new Key('ActivityContactsError'),
                          style: TextStyle(
                              fontSize: 12.0,
                              color: DynamicTheme.of(context).data.accentColor,
                            ),
                          )
                        : null,
                  ),
                  Column(
                    children: <Widget>[
                      ListView.builder(
                        scrollDirection: Axis.vertical,
                        key: Key('ActivityContactListView'),
                        shrinkWrap: true,
                        itemCount: _formValue.contacts.length,
                        itemBuilder: _selectedContactListItem
                      ),
                ]),
                ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: 100,
                    minHeight: 50,
                    maxWidth: 400,
                    maxHeight: 150,
                  ),
                  child: Center(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 320,
                        minHeight: 70,
                        maxWidth: 400,
                        maxHeight: 85,
                      ),
                      child: activityFormOpenerState.isLoading
                          ? Loading()
                          : RaisedButton(
                          key: Key('ValidActivityContactsForm'),
                          padding: EdgeInsets.only(top: 1),
                          child: Text(
                            AppLocalizations.of(context).translate('save'),
                            style: TextStyle(
                              color: ms.theme.appThemeName == "Standard" 
                              ? DynamicTheme.of(context).data.primaryColor 
                              : DynamicTheme.of(context).data.accentColor,
                            ),
                          ),
                          color: DynamicTheme.of(context).data.primaryColorLight,
                          onPressed: () {
                            if (_formValue.contacts.length > 0) {
                              if (messageSnack != "") {
                                showDurationDialog(context, messageSnack, 1);
                              }
                              setState(() {
                                _formValue.step += 1;
                                messageSnack = "";
                              });
                            } else {
                              setState(() {
                                _formValue.contactError = true;
                              });
                            }
                          }),
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: widget.isPost
                          ? null
                          : () {
                              setState(() {
                                _formValue.step = 2;
                              });
                            },
                      child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(vertical: 3),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "3.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activity-select-setup-messages'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ]),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                      ),
                    ),
                GestureDetector(
                  onTap: widget.isPost
                      ? null
                      : () {
                          setState(() {
                            _formValue.step = 3;
                          });
                        },
                  child: Container(
                    key: Key('ActivityFrom1to2'),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                    width: double.infinity,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                      Text(
                        "4.",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                      ),
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context).translate('activity-select-setup-contraints'),
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                        ),
                      ),
                    ]),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                  ),
                ),
                  ],
                )
              ],
            ),
            ),
          ]),
        );
      });
    }

    // Step 3
    /// This widget is used to set or update the alert_message that is associated with the activity or model
    Widget _setUpMessage(BuildContext context) {
      final _formKey = GlobalKey<FormState>();
      String tmpMsg = _formValue.message;

      return LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: viewportConstraints.maxHeight,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                  GestureDetector(
                    onTap: widget.isPost
                        ? null
                        : () {
                      setState(() {
                        _formValue.step = 0;
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(vertical: 3),
                        width: double.infinity,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "1.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activityform-confirm-activity-title'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ])),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                    ),
                  ),
                  GestureDetector(
                    onTap: widget.isPost
                        ? null
                        : () {
                      setState(() {
                        _formValue.step = 1;
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(vertical: 3),
                        width: double.infinity,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "2.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activityform-confirm-recipient'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ])),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                    ),
                  ),
                  Container(
                      width: double.infinity,
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 20),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                        Text(
                          "3.",
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColorLight),
                        ),
                        Expanded(
                          child: Text(
                            AppLocalizations.of(context).translate('activityform-confirm-activity-message'),
                            textAlign: TextAlign.right,
                            style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColorLight),
                          ),
                        ),
                      ])),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    padding: EdgeInsets.only(bottom: 0.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 0),
                        child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          alignment: WrapAlignment.center,
                          direction: Axis.horizontal,
                          runAlignment: WrapAlignment.center,
                          children: [
                            Form(
                              key: _formKey,
                                child: TextField(
                                  keyboardType: TextInputType.multiline,
                                  maxLines: null,
                                  controller: _inputMsg,
                                  onChanged: (value) {
                                    tmpMsg = removeDiacritics(value);
                                  },
                                  cursorColor: DynamicTheme.of(context).data.primaryColorLight,
                                  style: TextStyle(
                                    color: DynamicTheme.of(context).data.accentColor, fontSize: 18
                                  ),
                                  decoration: InputDecoration(
                                    isDense: true,
                                    contentPadding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
                                    filled: true,
                                    fillColor: DynamicTheme.of(context).data.primaryColorDark,
                                    labelStyle: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                    ),
                                    // hintText: _formValue.message,
                                    errorText: _msgError ? _msgErrorText : null,
                                    hintStyle: TextStyle(color: DynamicTheme.of(context).data.primaryColorLight),
                                    errorStyle: TextStyle(color: DynamicTheme.of(context).data.primaryColorLight),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: DynamicTheme.of(context).data.accentColor),
                                    ),
                                  ),
                                ),
                            ),
                          ],
                        ),
                    ),
                ]),
                ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: 100,
                    minHeight: 50,
                    maxWidth: 400,
                    maxHeight: 150,
                  ),
                  child: Center(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 320,
                        minHeight: 70,
                        maxWidth: 400,
                        maxHeight: widget.isModel ? 85 : 70,
                      ),
                      child: activityFormOpenerState.isLoading
                        ? Loading()
                        : RaisedButton(
                          key: Key('ValidActivityMessageForm'),
                          child: Text(
                            AppLocalizations.of(context).translate('save'),
                            style: TextStyle(
                              color: ms.theme.appThemeName == "Standard" 
                              ? DynamicTheme.of(context).data.primaryColor 
                              : DynamicTheme.of(context).data.accentColor,
                            ),
                          ),
                          color: DynamicTheme.of(context).data.primaryColorLight,
                          onPressed: () async {
                            tmpMsg = _inputMsg.text == "" 
                                  ? AppLocalizations.of(context).translate('activity-default-message') 
                                  : _inputMsg.text;
                            _formValue.message = removeDiacritics(tmpMsg);
                            if (_formValue.message.isAlertMessageValid() || _formValue.message == "") {
                              if (widget.isPost) {
                                if (_formValue.message != oldActivityMessage) {
                                  if (widget.isPost) {
                                    for (int i = 0; i < _formValue.constraintsEdit.length; i++) {
                                      if (_formValue.constraintsEdit[i].message == oldActivityMessage
                                            && _formValue.constraintsEdit[i].message != _formValue.message
                                          ) {
                                        _formValue.constraintsEdit[i].message = _formValue.message;
                                      }
                                    }
                                  } 
                                } 
                                /// if just a new model initiate form part 4 with a default constraint
                                if (widget.isPost && widget.isModel) {
                                  _formValue.constraintsEdit.add(
                                    Constraint.fromForm(
                                      type: 'timer',
                                      icon: 60031,
                                      startOffset: 0,
                                      timerOffsetCheck: 3300,
                                      endOffset: 3600,
                                      message: _formValue.message
                                    )
                                  );
                                }
                              } else {
                                final response = await updateInfos();
                                if (response != null) {
                                  bool hasError = false;
                                  if (response.statusCode == 200) {
                                    /// then else if message has been modified, update the existing ones
                                    if (_formValue.message != oldActivityMessage) {
                                      for (int i = 0; i < _formValue.constraintsEdit.length; i++) {
                                        if (_formValue.constraintsEdit[i].message == oldActivityMessage
                                            && _formValue.constraintsEdit[i].message != _formValue.message
                                          ) {
                                        _formValue.constraintsEdit[i].message = _formValue.message;
                                        final response1 = await updateConstraint(_formValue.constraintsEdit[i]);
                                        if (response1.statusCode != 200 || response1 == null) {
                                          hasError = true;
                                          break ;
                                        }
                                      }
                                    }
                                  }
                                  if (!hasError) {
                                    /// update activity or model in provider to update front
                                    _formValue.constraints = _formValue.constraintsEdit
                                                        .map((item) => new Constraint.clone(item))
                                                        .toList();
                                    if (widget.isModel) {
                                      Provider.of<ModelProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                    } else {
                                      Provider.of<ActivityProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                    }
                                  }
                                } 
                                if (serverDown) {
                                  Scaffold.of(context).showSnackBar(
                                    SnackBar(content: Text(
                                      AppLocalizations.of(context).translate('server-error'),
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          color: DynamicTheme.of(context).data.primaryColorLight,
                                        ),
                                      ),
                                      backgroundColor: DynamicTheme.of(context).data.errorColor,
                                    )
                                  );
                                }
                                if (messageSnack != "") {
                                  showDurationDialog(context, messageSnack, 1);
                                  messageSnack = "";
                                }
                              } 
                            }
                            setState(() {
                                activityFormOpenerState.isLoading = false;
                                _msgError = false;
                                _msgErrorText = "";
                                if (serverDown) {
                                  serverDown = false;
                                } else {
                                  _formValue.step += 1;
                                  oldActivityMessage = _formValue.message;
                                  // _inputMsg.text = "";
                                }
                                messageSnack = "";
                            });
                          } else {
                            setState(() {
                              _msgErrorText = AppLocalizations.of(context).translate('msg-alert-error');
                              _msgError = true;
                            });
                          }
                        }),
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    GestureDetector(
                  onTap: widget.isPost
                      ? null
                      : () {
                          setState(() {
                            _formValue.step = 3;
                          });
                        },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                    width: double.infinity,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                      Text(
                        "4.",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                      ),
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context).translate('activity-select-setup-contraints'),
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                        ),
                      ),
                    ]),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                  ),
                ),
                  ],
                )
              ],
            ),
          ),
        );
      });
    }

    int keyI = 0;
    // Step 4
    /// This widget is used to set or update the list of constraints that is associated with the activity or model
    Widget _setUpConstraints(BuildContext context) {
      return LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return SingleChildScrollView(
            child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: viewportConstraints.maxHeight,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: widget.isPost
                        ? null
                        : () {
                      setState(() {
                        _formValue.step = 0;
                      });
                    },
                    child: Container(
                        key: Key('ActivityFrom2to0'),
                        padding: EdgeInsets.symmetric(vertical: 3),
                        width: double.infinity,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "1.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activityform-confirm-activity-title'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ])),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                    ),
                  ),
                  GestureDetector(
                    onTap: widget.isPost
                        ? null
                        : () {
                      setState(() {
                        _formValue.step = 1;
                      });
                    },
                    child: Container(
                        key: Key('ActivityFrom2to1'),
                        padding: EdgeInsets.symmetric(vertical: 3),
                        width: double.infinity,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "2.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activityform-confirm-recipient'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ])),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                    ),
                  ),
                  GestureDetector(
                    onTap: widget.isPost
                        ? null
                        : () {
                      setState(() {
                        _formValue.step = 2;
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(vertical: 3),
                        width: double.infinity,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                          Text(
                            "3.",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColor),
                          ),
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context).translate('activity-select-setup-messages'),
                              textAlign: TextAlign.right,
                              style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColor),
                            ),
                          ),
                        ])),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColor)),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 20),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                      Text(
                        "4.",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColorLight),
                      ),
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context).translate('activity-select-setup-contraints'),
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 16.0, color: DynamicTheme.of(context).data.primaryColorLight),
                        ),
                      ),
                    ]),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    padding: EdgeInsets.only(bottom: 0.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight)),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                    padding: EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        border: Border(
                          top: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight),
                          right: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight),
                          left: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight),
                          bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight),
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(25.0))),
                    width: 400,
                    child: DropdownButton<String>(
                      key: Key('ActivityAddConstraintsButton'),
                      value: _formValue.dropdownValue,
                      icon: Icon(IconData(60240, fontFamily: 'MaterialIcons'), color: DynamicTheme.of(context).data.accentColor),
                      iconSize: 18,
                      dropdownColor: DynamicTheme.of(context).data.primaryColor,
                      focusColor: DynamicTheme.of(context).data.primaryColor,
                      style: TextStyle(color: DynamicTheme.of(context).data.accentColor, fontSize: 18),
                      onChanged: (String newValue) async {
                        setState(() {
                          _formValue.dropdownValue = newValue;
                        });
                        List<Constraint> tmpCl = new List<Constraint>();
                        if (newValue == "timer") {
                          if (widget.isModel) {
                            addSelectedConstraintsWithData(tmpCl, newValue, {
                              'start_offset': 0,
                              'notify_offset': 3300,
                              'end_offset': 3600,
                              'alert_message': _formValue.message == ""
                                              ? AppLocalizations.of(context).translate('activity-default-message') 
                                              : _formValue.message
                            });
                          } else {
                            DateTime sd =  DateTime.now();
                            DateTime ed =  sd.add(new Duration(seconds: 7200));
                            DateTime nd =  ed.add(Duration(seconds: -300));
                            addSelectedConstraintsWithData(tmpCl, newValue, {
                              'end_date': ed.toUtc().toIso8601String(),
                              'start_date': sd.toUtc().toIso8601String(),
                              'notify_date': nd.toUtc().toIso8601String(),
                              'alert_message': _formValue.message == ""
                                              ? AppLocalizations.of(context).translate('activity-default-message') 
                                              : _formValue.message
                            });
                          }
                        }
                        Constraint newC;
                        var response;
                        if (widget.isPost) {
                          newC = tmpCl[0];
                        } else {
                          response = await addConstraint(tmpCl[0]);
                          if (response.statusCode == 201) {
                            tmpCl[0].id = json.decode(response.body)['id'];
                            newC = tmpCl[0];
                          }
                        }
                        if (newC != null) {
                          _formValue.constraintsEdit.add(newC);
                          if (response != null) {
                            if (!widget.isPost && response.statusCode == 201) {
                              _formValue.constraints = _formValue.constraintsEdit
                                                        .map((item) => new Constraint.clone(item))
                                                        .toList();
                              // change constraint in activity in provider with its id
                              if (widget.isModel) {
                                Provider.of<ModelProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                              } else {
                                Provider.of<ActivityProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                              }
                            }
                          }
                        }
                        if (serverDown) {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(content: Text(
                              AppLocalizations.of(context).translate('server-error'),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                ),
                              ),
                              backgroundColor: DynamicTheme.of(context).data.errorColor,
                            )
                          );
                        }
                        if (messageSnack != "") {
                          showDurationDialog(context, messageSnack, 1);
                          messageSnack = "";
                        }
                        setState(() {
                          _formValue.constraintError = false;
                          if (serverDown) {
                            serverDown = false;
                          }
                        });
                        if (_formValue.constraintsEdit.length >= 0) {
                        } else {
                          setState(() {
                            _formValue.constraintError = true;
                          });
                        }
                      },
                      items: listOfConstraints(context).map<DropdownMenuItem<String>>((element) {
                        return DropdownMenuItem<String>(
                          value: element.type,
                          child: Text(
                            element.type,
                            key: new Key('ActivityAddConstraintsButton' + element.type),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                  info
                  ? Container(
                      key: new Key('ActivityConstraintsInfo'),
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child: Text(
                        description,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontStyle: FontStyle.italic,
                          color: DynamicTheme.of(context).data.primaryColorLight,
                        ),
                        softWrap: true,
                      ),
                    )
                  : Container(),
                  Container(
                    child: _formValue.constraintError
                        ? Padding(
                          key: new Key('ActivityConstraintsError'),
                          padding: EdgeInsets.symmetric(vertical: 5.0),
                          child: Text(
                            AppLocalizations.of(context).translate('activityform-set-all-constaint'),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 12.0,
                              color: DynamicTheme.of(context).data.accentColor,
                            ),
                          ),
                        )
                      : Container(),
                  ),
                  Column(
                      mainAxisSize: MainAxisSize.min,
                      children: _formValue.constraintsEdit.asMap().entries.map((e) {
                        keyI++;
                        return Center(
                          child: Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.symmetric(vertical: 5.0),
                                decoration: BoxDecoration(
                                  color: DynamicTheme.of(context).data.primaryColor,
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                                ),
                                child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                                  Container(
                                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                        Icon(
                                            IconData(
                                                e.value.icon,
                                                fontFamily: 'MaterialIcons'
                                            ),
                                            key:  (keyI == _formValue.constraintsEdit.length) ? Key("FinalActivityConstraintsListTileLeading") : Key("ActivityConstraintsListTileLeading" + keyI.toString()),
                                            color: DynamicTheme.of(context).data.primaryColorLight,
                                            size: 20
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 5.0),
                                          child: Text(
                                            e.value.type,
                                            key:  (keyI == _formValue.constraintsEdit.length) ? Key("FinalActivityConstraintsListTileTitle") : Key("ActivityConstraintsListTileTitle" + keyI.toString()),
                                            style: TextStyle(fontSize: 18.0, color: DynamicTheme.of(context).data.primaryColorLight),
                                          ),
                                        ),
                                        IconButton(
                                          key: (keyI == _formValue.constraintsEdit.length) ? Key("FinalActivityConstraintsInfoTip") : Key("ActivityConstraintsInfoTip" + keyI.toString()),
                                          icon: Icon(
                                            IconData(59353, fontFamily: 'MaterialIcons'),
                                            color: DynamicTheme.of(context).data.primaryColorDark, size: 20),
                                          tooltip: AppLocalizations.of(context).translate('activityform-info-tip'),
                                          hoverColor: Color.fromRGBO(0, 0, 0, 0),
                                          onPressed: () async {
                                            if (description.toString() == e.value.description.toString() || description == "") {
                                              setState(() {
                                                info = !info;
                                                if (info) {
                                                  description = e.value.type == "timer" ? AppLocalizations.of(context).translate('timer-description') : "";
                                                } else {
                                                  description = "";
                                                }
                                              });
                                            }
                                          }),
                                        ]),
                                      ),
                                      IconButton(
                                        key: (keyI == _formValue.constraintsEdit.length) ? Key("FinalActivityConstraintsListTileTrailing") : Key("ActivityConstraintsListTileTrailing" + keyI.toString()),
                                        icon: Icon(
                                          IconData(59041, fontFamily: 'MaterialIcons'),
                                          color: DynamicTheme.of(context).data.primaryColorDark
                                        ),
                                        tooltip: AppLocalizations.of(context).translate('activityform-delete-constaint-tip'),
                                        hoverColor: Color.fromRGBO(0, 0, 0, 0),
                                        onPressed: () async {
                                          if (_formValue.constraintsEdit.length > 1) {
                                            if (widget.isPost) {
                                              setState(() {
                                                _formValue.constraintsEdit = removeSelectedConstraints(_formValue.constraintsEdit, e.key);
                                                _formValue.constraintError = false;
                                              });
                                            } else {
                                              var response;
                                              response = await deleteConstraint(e.value, e.key);
                                              if (response.statusCode == 200) {
                                                _formValue.constraints = _formValue.constraintsEdit
                                                        .map((item) => new Constraint.clone(item))
                                                        .toList();
                                                // remove constraint from activity and change it in provider with its id
                                                if (widget.isModel) {
                                                  Provider.of<ModelProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                                } else {
                                                  Provider.of<ActivityProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                                }
                                              }
                                              if (serverDown) {
                                                Scaffold.of(context).showSnackBar(
                                                  SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                                                    style: TextStyle(
                                                        fontSize: 18.0,
                                                        color: DynamicTheme.of(context).data.primaryColorLight,
                                                      ),
                                                    ),
                                                    backgroundColor: DynamicTheme.of(context).data.errorColor,
                                                  )
                                                );
                                              }
                                              if (messageSnack != "") {
                                                showDurationDialog(context, messageSnack, 1);
                                              }
                                              setState(() {
                                                serverDown = false;
                                                messageSnack = "";
                                              });
                                            }
                                          } else {
                                            setState(() {
                                              _formValue.constraintError = true;
                                            });
                                          }
                                        },
                                      ),
                                    ]),
                                  ),
                                  settingConstraint(context, _formValue.constraintsEdit[keyI - 1], keyI, (keyI == _formValue.constraintsEdit.length))
                                ]),
                              ),
                        );
                      }).toList())
                ],
              ),
              ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 100,
                  minHeight: 50,
                  maxWidth: 400,
                  maxHeight: 150,
                ),
                child: Center(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: 320,
                      minHeight: 70,
                      maxWidth: 400,
                      maxHeight: 85,
                    ),
                    child: activityFormOpenerState.isLoading
                    ? Loading()
                    : RaisedButton(
                        key: Key('ValidActivityConstraintsForm'),
                        child: Text(
                          AppLocalizations.of(context).translate('save'),
                          style: TextStyle(
                            color: ms.theme.appThemeName == "Standard" 
                            ? DynamicTheme.of(context).data.primaryColor 
                            : DynamicTheme.of(context).data.accentColor,
                          ),
                        ),
                        padding: EdgeInsets.only(top: 1),
                        color: DynamicTheme.of(context).data.primaryColorLight,
                        onPressed: () async {
                          if (selectedConstraintsSetUp(_formValue.constraintsEdit) && _formValue.constraintsEdit.length > 0) {
                            setState(() {
                              _formValue.constraintError = false;
                            });
                            if (widget.isPost) {
                              var response = await postActivity();
                              if (response.statusCode == 201) {
                                // add activity in providers lists with its id
                                _formValue.constraints = _formValue.constraintsEdit
                                                        .map((item) => new Constraint.clone(item))
                                                        .toList();
                                if (widget.isModel) {
                                  Provider.of<ModelProvider>(context, listen: false).add(_formValue.getNewActivity((json.decode(response.body))['id'], widget.isModel));
                                } else {
                                  Provider.of<ActivityProvider>(context, listen: false).add(_formValue.getNewActivity((json.decode(response.body))['id'], widget.isModel));
                                }
                                Navigator.pop(context, messageSnack);
                              }
                            } else {
                              // compare widget.initValues.constraints && _formValue.constraints
                              List<Constraint> toUpdate = [];
                              _formValue.constraints.forEach((element) {
                                _formValue.constraintsEdit.forEach((e) {
                                  if (!widget.isModel && e.id == element.id
                                    && (e.startDate != element.startDate
                                      || e.endDate != element.endDate
                                      || e.message != element.message
                                      || e.dateOffset != element.dateOffset)) {
                                        
                                    toUpdate.add(e);
                                  } else if (widget.isModel && e.id == element.id
                                    && (e.startOffset != element.startOffset
                                      || e.endOffset != element.endOffset
                                      || e.message != element.message
                                      || e.timerOffsetCheck != element.timerOffsetCheck)) {
                                    toUpdate.add(e);
                                  }
                                });
                              });
                              if (toUpdate.length > 0) {
                                bool hasError = false;
                                var response;
                                for (int i = 0; i < toUpdate.length; i++) {
                                  response = await updateConstraint(toUpdate[i]);
                                  if (response.statusCode != 200) {
                                    hasError = true;
                                    break ;
                                  }
                                }
                                if (!hasError) {
                                  _formValue.constraints = _formValue.constraintsEdit
                                                        .map((item) => new Constraint.clone(item))
                                                        .toList();
                                  // update constraint from activity in providers list with its id
                                  if (widget.isModel) {
                                    Provider.of<ModelProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                  } else {
                                    Provider.of<ActivityProvider>(context, listen: false).update(_formValue.getNewActivity(_formValue.id, widget.isModel));
                                  }
                                  Navigator.pop(context, messageSnack);
                                }
                              } else {
                                Navigator.pop(context, messageSnack);
                              }
                            }
                            if (serverDown) {
                              Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      color: DynamicTheme.of(context).data.primaryColorLight,
                                    ),
                                  ),
                                  backgroundColor: DynamicTheme.of(context).data.errorColor,
                                )
                              );
                            }
                            if (messageSnack != "") {
                              showDurationDialog(context, messageSnack, 1);
                            }
                            setState(() {
                              activityFormOpenerState.isLoading = false;
                              messageSnack = "";
                              serverDown = false;
                            });
                          } else {
                            setState(() {
                              _formValue.constraintError = true;
                            });
                          }
                        }),
                  ),
                ),
              ),
            ],
          ),
        ));
      });
    }

    return Expanded(
      key: Key('ActivityForm'),
      child:
        _formValue.step == 0 ? _nameForm(context) :
          (_formValue.step == 1 ? _setUpContacts(context) :
            (_formValue.step == 2 ? _setUpMessage(context) :
              _setUpConstraints(context)
            )
          ),
    );
  }
}
