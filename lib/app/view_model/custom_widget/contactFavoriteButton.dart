import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:provider/provider.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';

class Favorite extends StatefulWidget {
  final Contact contact;
  final String fKey;
  Favorite({this.contact, this.fKey}) : super(key: new Key('FavoriteWidget'));
  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {

  /// This widget is the button that is used to set or unset the favorite status for a contact
  /// It will display a blank star if not in favorite
  /// It will display a yellow star if in favorite
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);

    return IconButton(
      key: Key(widget.fKey),
      hoverColor: Color.fromRGBO(0, 0, 0, 0),
      onPressed: () async {
        String messageSnack = "";
        ms.navIsLoading = true;
        var response;
        response = await widget.contact.changeFavouriteStatus(ms.api);
        if (response.statusCode == 200) {
          Provider.of<ContactProvider>(context, listen: false).update(widget.contact);
          setState(() {
            widget.contact.setFavorite();
          });
          if (widget.contact.favorite) {
            messageSnack = widget.contact.nickname + ' ' + AppLocalizations.of(context).translate('contact-put-favorite');
          } else {
            messageSnack = widget.contact.nickname + ' ' + AppLocalizations.of(context).translate('contact-notput-favorite');
          }
        } else {   
          if (response.statusCode == 400) {
            messageSnack = AppLocalizations.of(context).translate('error-occured');
          } else if (response.statusCode == 500) {
            Scaffold.of(context).showSnackBar(
              SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                style: TextStyle(
                    fontSize: 18.0,
                    color: DynamicTheme.of(context).data.primaryColorLight,
                  ),
                ),
                backgroundColor: DynamicTheme.of(context).data.errorColor,
              )
            );
          }
        }
        ms.navIsLoading = false;
        if (messageSnack != "" && messageSnack != null) {
          showDurationDialog(context, messageSnack, 1);
          messageSnack = "";
        }
      },
      icon: widget.contact.favorite
          ? Icon(
              IconData(59938, fontFamily: 'MaterialIcons'),
              color: DynamicTheme.of(context).data.accentColor,
            )
          : Icon(
              IconData(59939, fontFamily: 'MaterialIcons'),
              color: DynamicTheme.of(context).data.primaryColorLight,
            ),
    );
  }
}