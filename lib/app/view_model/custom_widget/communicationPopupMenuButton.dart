// import 'package:dynamic_theme/dynamic_theme.dart';
// import 'package:flutter/material.dart';
// import 'package:iqiperclient/app/model/AppLocalizations.dart';
// import 'package:iqiperclient/app/model/Communication.dart';
// import 'package:iqiperclient/app/model/MainState.dart';
// import 'package:iqiperclient/app/view/custom_widget/activityAlertDialog.dart';
// import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
// import 'package:iqiperclient/app/view/page/reports.dart';
// import 'package:provider/provider.dart';

// class ReportMenuChoices {
//   final String text;
//   final String key;
//   ReportMenuChoices({this.text, this.key});
// }

// class CommunicationPopupMenuButton extends StatefulWidget {
//   final bool index;
//   final Communication communication;
//   final ReportState parentWidget;
  
//   CommunicationPopupMenuButton({this.communication, this.index, this.parentWidget});

//   @override
//   CommunicationPopupMenuButtonState createState() => CommunicationPopupMenuButtonState();
// }

// class CommunicationPopupMenuButtonState extends State<CommunicationPopupMenuButton> {

//   /// This widget is the popup menu button that is displayed when selecting the option button of a report
//   @override
//   Widget build(BuildContext context) {
//     return PopupMenuButton<String>(
//       key: widget.index ? new Key('FinalActivityReportPopUpMenuButton') : Key("ReportPopUpMenuButton"),
//       icon: Icon(
//         IconData(0xe867, fontFamily: 'MaterialIcons'),
//         color: DynamicTheme.of(context).data.primaryColorLight,
//       ),
//       onSelected: (choice) async {
//         String messageSnack = '';
//         if (choice == AppLocalizations.of(context).translate('report-fix-activity')) {
//           MainState ms = Provider.of<MainState>(context, listen: false);
//           if (await widget.communication.linkedActivity.canBeFixed(ms.api, widget.parentWidget.context)) {
//             final result = await showDialog(
//               context: context,
//               builder: (BuildContext context) {
//                 return ActivityAlertDialog(
//                   question: AppLocalizations.of(context).translate('report-fix-alert-question'),
//                   confirmation: AppLocalizations.of(context).translate('report-fix-activity'),
//                 );
//             });
//             if (result) {
//               var response = await widget.communication.linkedActivity.alertActionActivity('alert/correct', ms.api);
//               if (response.statusCode == 200) {
//                 messageSnack = AppLocalizations.of(context).translate('report-alert-fixed');
//                 widget.parentWidget.executeFetch(ms);
//               } else {   
//                 if (response.statusCode == 400) {
//                   messageSnack = AppLocalizations.of(context).translate('error-occured');
//                 } else if (response.statusCode == 500) {
//                   Scaffold.of(context).showSnackBar(
//                     SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
//                       style: TextStyle(
//                           fontSize: 18.0,
//                           color: DynamicTheme.of(context).data.primaryColorLight,
//                         ),
//                       ),
//                       backgroundColor: DynamicTheme.of(context).data.errorColor,
//                     )
//                   );
//                 }
//               }
//             }
//           } else {
//             messageSnack = AppLocalizations.of(context).translate('report-no-more-correction');
//           }
//         } else if (choice == AppLocalizations.of(context).translate('delete')) {
//           final result = await showDialog(
//             context: context,
//             builder: (BuildContext context) {
//               return ActivityAlertDialog(
//                 question: AppLocalizations.of(context).translate('report-delete-question'),
//                 confirmation: AppLocalizations.of(context).translate('report-choice-delete'),
//               );
//             }
//           );
//           if (result) {
//             MainState ms = Provider.of<MainState>(context, listen: false);
//             var response = await widget.communication.linkedActivity.deleteActivity(ms.api);
//             if (response.statusCode == 200) {
//               messageSnack = AppLocalizations.of(context).translate('report-confirm-delete');
//               Provider.of<ActivityProvider>(context, listen: false).remove(widget.communication.linkedActivity.id);
//               widget.parentWidget.executeFetch(ms);
//             } else {   
//               if (response.statusCode == 400) {
//                 messageSnack = AppLocalizations.of(context).translate('error-occured');
//               } else if (response.statusCode == 500) {
//                 Scaffold.of(context).showSnackBar(
//                   SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
//                     style: TextStyle(
//                         fontSize: 18.0,
//                         color: DynamicTheme.of(context).data.primaryColorLight,
//                       ),
//                     ),
//                     backgroundColor: DynamicTheme.of(context).data.errorColor,
//                   )
//                 );
//               }
//             }
//           }
//         }
//         if (messageSnack != null && messageSnack != "") {
//           showDurationDialog(context, messageSnack, 3);
//         }
//       },
//       itemBuilder: (BuildContext context) {
//         List<ReportMenuChoices> choices = [
//           new ReportMenuChoices(text: AppLocalizations.of(context).translate('report-fix-activity'), key: "Correction"),
//           new ReportMenuChoices(text: AppLocalizations.of(context).translate('delete'), key: "Delete"),
//         ];

//         return choices.map((choice) {
//           return PopupMenuItem<String>(
//               key: widget.index ? new Key('FinalActivityReportPopUpMenuButton' + choice.key) : Key("ReportPopUpMenuButton" + choice.key),
//               value: choice.text,
//               child: Text(choice.text,
//                   textAlign: TextAlign.center,
//                   style: TextStyle(
//                     color: DynamicTheme.of(context).data.primaryColorLight,
//                   )));
//         }).toList();
//       },
//     );
//   }
// }
