import 'package:flutter/material.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/FormValues.dart';
import 'package:iqiperclient/app/view/custom_widget/activityAlertDialog.dart';
import 'package:iqiperclient/app/view/custom_widget/activityFormOpener.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';

class DashboardMenuChoices {
  final String text;
  final String key;
  DashboardMenuChoices({this.text, this.key});
}

/// This widget is the popup menu button that is displayed when selecting the option button of an activity
class DashboardPopupMenuButton extends StatefulWidget {
  final bool index;
  final Activity activity;
  DashboardPopupMenuButton({this.activity, this.index}) : super(key: Key('DashboardPopupMenuButtonMenuWidget'));

  @override
  DashboardPopupMenuButtonState createState() => DashboardPopupMenuButtonState();
}

class DashboardPopupMenuButtonState extends State<DashboardPopupMenuButton> {
  String messageSnack = "";
  bool serverDown = false;

  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);
    
    return PopupMenuButton<String>(
      key: widget.index ? new Key('FinalDashboardPopUpMenuButton') : Key("DashboardPopUpMenuButton"),
      icon: Icon(
        IconData(0xe867, fontFamily: 'MaterialIcons'),
        color: DynamicTheme.of(context).data.primaryColorLight,
      ),
      onSelected: (choice) async {
        if (choice == AppLocalizations.of(context).translate('dashboard-send-notif-activity')) {
          final result = await showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return ActivityAlertDialog(
                question: AppLocalizations.of(context).translate('dashboard-send-notif-question'),
                confirmation: AppLocalizations.of(context).translate('dashboard-send-notif-confirmation')
              );
            }
          );
          if (result) {
            final response = await widget.activity.alertActionActivity("alert", ms.api);
            if (response.statusCode == 200) {
              messageSnack = AppLocalizations.of(context).translate('dashboard-send-notif-confirmed');
              widget.activity.endDate = DateTime.now().toUtc().add(DateTime.now().timeZoneOffset).toIso8601String();
              Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
            } else {
              if (response.statusCode == 400) {
                messageSnack = AppLocalizations.of(context).translate('error-occured');
              } else if (response.statusCode == 500) {
                serverDown = true;
              }
            }
          }
        } else if (choice == AppLocalizations.of(context).translate('dashboard-terminate-activity')) {
          final result = await showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return ActivityAlertDialog(
                question: AppLocalizations.of(context).translate('dashboard-end-alert-question'),
                confirmation: AppLocalizations.of(context).translate('dashboard-terminate-activity'),
              );
            });
          if (result) {
            final response = await widget.activity.alertActionActivity("finish", ms.api);
            if (response.statusCode == 200) {
              await ms.cancelNotification();
              widget.activity.endDate = DateTime.now().toUtc().add(DateTime.now().timeZoneOffset).toIso8601String();
              Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
              messageSnack = AppLocalizations.of(context).translate('dashboard-alert-ended');
            } else {
              if (response.statusCode == 400) {
                messageSnack = AppLocalizations.of(context).translate('error-occured');
              } else if (response.statusCode == 500) {
                serverDown = true;
              }
            }
          }
        } else if (choice == AppLocalizations.of(context).translate('delete')) {
          final result = await showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return ActivityAlertDialog(
                question: AppLocalizations.of(context).translate('dashboard-delete-activity-question'),
                confirmation: AppLocalizations.of(context).translate('delete'),
              );
            }
          );
          if (result) {
            var response = await widget.activity.deleteActivity(ms.api);
            if (response.statusCode == 200) {
              await ms.cancelNotification();
              Provider.of<ActivityProvider>(context, listen: false).remove(widget.activity.id);
              messageSnack = AppLocalizations.of(context).translate('deleted');
            } else {
              if (response.statusCode == 400) {
                messageSnack = AppLocalizations.of(context).translate('delete-fail');
              } else if (response.statusCode == 500) {
                serverDown = true;
              }
            }
          }
        } else if (choice == AppLocalizations.of(context).translate('dashboard-modify-alert')) {
          //UpdateActivity
          final result = await pushOrShow(
            context,
            ActivityFormOpener(
              isPost: false,
              isModel: false,
              action: AppLocalizations.of(context).translate('dashboard-update-current-activity'),
              formValues: FormValues(
                step: 0,
                title: widget.activity.title,
                message: widget.activity.message,
                icon: widget.activity.icon,
                id: widget.activity.id,
                constraintError: false,
                dropdownValue: 'timer',
                constraints: widget.activity.constraints,
                contactError: false,
                contactSelectionState: contactForContactSelection(Provider.of<ContactProvider>(context, listen: false).list, widget.activity.contacts),
                contacts: contactForContactSelection(widget.activity.contacts, widget.activity.contacts),
              )
            )
          );
          if (result != null && result != "") {
            if (result == AppLocalizations.of(context).translate('server-error')) {
              serverDown = true;
            } else {
              messageSnack = "$result";
            }
          }
        }
        if (messageSnack != null && messageSnack != "") {
          showDurationDialog(context, messageSnack, 1);
        }
        if (serverDown) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(
              AppLocalizations.of(context).translate('server-error'),
              style: TextStyle(
                  fontSize: 18.0,
                  color: DynamicTheme.of(context).data.primaryColorLight,
                ),
              ),
              backgroundColor: DynamicTheme.of(context).data.errorColor,
            )
          );
          serverDown = false;
        }
      },
      itemBuilder: (BuildContext context) {
        List<DashboardMenuChoices> choices = [
          new DashboardMenuChoices(text: AppLocalizations.of(context).translate('dashboard-modify-alert'), key: "Modify"),
          new DashboardMenuChoices(text: AppLocalizations.of(context).translate('dashboard-terminate-activity'), key: "Done"),
          new DashboardMenuChoices(text: AppLocalizations.of(context).translate('dashboard-send-notif-activity'), key: "Notify"),
          new DashboardMenuChoices(text: AppLocalizations.of(context).translate('delete'), key: "Delete"),
        ];

        return choices.map((choice) {
          return PopupMenuItem<String>(
              key: widget.index ? new Key('FinalDashboardPopUpMenuButton' + choice.key) : Key("DashboardPopUpMenuButton" + choice.key),
              value: choice.text,
              child: Text(choice.text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: DynamicTheme.of(context).data.accentColor,
                  )));
        }).toList();
      },
    );
  }
}
