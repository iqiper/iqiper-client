import 'package:flutter/material.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/view/custom_widget/activityAlertDialog.dart';

class ReportMenuChoices {
  final String text;
  final String key;
  ReportMenuChoices({this.text, this.key});
}

/// This widget is the popup menu button that is displayed when selecting the option button of an activity
class ReportPopupMenuButton extends StatefulWidget {
  final bool index;
  final Activity activity;
  ReportPopupMenuButton({this.activity, this.index}) : super(key: Key('ReportPopupMenuButtonMenuWidget'));

  @override
  ReportPopupMenuButtonState createState() => ReportPopupMenuButtonState();
}

class ReportPopupMenuButtonState extends State<ReportPopupMenuButton> {
  String messageSnack = "";
  bool serverDown = false;

  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);
    
    return PopupMenuButton<String>(
      key: widget.index ? new Key('FinalReportPopUpMenuButton') : Key("ReportPopUpMenuButton"),
      icon: Icon(
        IconData(0xe867, fontFamily: 'MaterialIcons'),
        color: DynamicTheme.of(context).data.primaryColorLight,
      ),
      onSelected: (choice) async {
        if (choice == AppLocalizations.of(context).translate('report-fix-activity')) {
          if (await widget.activity.canBeFixed(ms.api, context)) {
            final result = await showDialog(
              context: context,
              builder: (BuildContext context) {
                return ActivityAlertDialog(
                  question: AppLocalizations.of(context).translate('report-fix-alert-question'),
                  confirmation: AppLocalizations.of(context).translate('report-fix-activity'),
                );
            });
            if (result) {
              var response = await widget.activity.alertActionActivity('alert/correct', ms.api);
              if (response.statusCode == 200) {
                messageSnack = AppLocalizations.of(context).translate('report-alert-fixed');
              } else {   
                if (response.statusCode == 400) {
                  messageSnack = AppLocalizations.of(context).translate('error-occured');
                } else if (response.statusCode == 500) {
                  Scaffold.of(context).showSnackBar(
                    SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                      style: TextStyle(
                          fontSize: 18.0,
                          color: DynamicTheme.of(context).data.primaryColorLight,
                        ),
                      ),
                      backgroundColor: DynamicTheme.of(context).data.errorColor,
                    )
                  );
                }
              }
            }
          } else {
            messageSnack = AppLocalizations.of(context).translate('report-no-more-correction');
          }
        }  else if (choice == AppLocalizations.of(context).translate('dashboard-send-notif-activity')) {
          final result = await showDialog(
            context: context,
            builder: (BuildContext context) {
              return ActivityAlertDialog(
                question: AppLocalizations.of(context).translate('dashboard-send-notif-question'),
                confirmation: AppLocalizations.of(context).translate('dashboard-send-notif-confirmation'),
              );
          });
          if (result) {
            final response = await widget.activity.alertActionActivity("alert", ms.api);
            if (response.statusCode == 200) {
              messageSnack = AppLocalizations.of(context).translate('dashboard-send-notif-confirmed');
              Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
            } else {
              if (response.statusCode == 400) {
                messageSnack = AppLocalizations.of(context).translate('error-occured');
              } else if (response.statusCode == 500) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(content: Text(
                    AppLocalizations.of(context).translate('server-error'),
                    style: TextStyle(
                        fontSize: 18.0,
                        color: DynamicTheme.of(context).data.primaryColorLight,
                      ),
                    ),
                    backgroundColor: DynamicTheme.of(context).data.errorColor,
                  )
                );
              }
            }
          }
        } else if (choice == AppLocalizations.of(context).translate('delete')) {
          final result = await showDialog(
            context: context,
            builder: (BuildContext context) {
              return ActivityAlertDialog(
                question: AppLocalizations.of(context).translate('report-delete-question'),
                confirmation: AppLocalizations.of(context).translate('delete'),
              );
            }
          );
          if (result) {
            var response = await widget.activity.deleteActivity(ms.api);
            if (response.statusCode == 200) {
              await ms.cancelNotification();
              Provider.of<ActivityProvider>(context, listen: false).remove(widget.activity.id);
              messageSnack = AppLocalizations.of(context).translate('deleted');
            } else {
              if (response.statusCode == 400) {
                messageSnack = AppLocalizations.of(context).translate('delete-fail');
              } else if (response.statusCode == 500) {
                serverDown = true;
              }
            }
          }
        }
        if (messageSnack != null && messageSnack != "") {
          showDurationDialog(context, messageSnack, 1);
        }
        if (serverDown) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(
              AppLocalizations.of(context).translate('server-error'),
              style: TextStyle(
                  fontSize: 18.0,
                  color: DynamicTheme.of(context).data.primaryColorLight,
                ),
              ),
              backgroundColor: DynamicTheme.of(context).data.errorColor,
            )
          );
          serverDown = false;
        }
      },
      itemBuilder: (BuildContext context) {
        List<ReportMenuChoices> choices = [
          new ReportMenuChoices(text: AppLocalizations.of(context).translate('report-fix-activity'), key: "Fix"),
          new ReportMenuChoices(text: AppLocalizations.of(context).translate('dashboard-send-notif-activity'), key: "Launch"),
          new ReportMenuChoices(text: AppLocalizations.of(context).translate('delete'), key: "Delete"),
        ];

        return choices.map((choice) {
          return PopupMenuItem<String>(
              key: widget.index ? new Key('FinalReportPopUpMenuButton' + choice.key) : Key("ReportPopUpMenuButton" + choice.key),
              value: choice.text,
              child: Text(choice.text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: DynamicTheme.of(context).data.accentColor,
                  )));
        }).toList();
      },
    );
  }
}
