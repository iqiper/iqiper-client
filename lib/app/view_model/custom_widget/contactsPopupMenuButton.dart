import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:iqiperclient/app/view/custom_widget/activityAlertDialog.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:iqiperclient/app/view/custom_widget/contactFormOpener.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';

class ContactMenuChoices {
  final String text;
  final String key;
  ContactMenuChoices({this.text, this.key});
}

class ContactPopupMenuButton extends StatefulWidget {
  final bool index;
  final Contact contact;
  ContactPopupMenuButton({this.contact, this.index}) : super(key: new Key('ContactPopupMenuButtonWidget'));

  @override
  _ContactPopupMenuButtonState createState() => _ContactPopupMenuButtonState();
}

class _ContactPopupMenuButtonState extends State<ContactPopupMenuButton> {
  String messageSnack = "";
  bool serverDown = false;

  /// This widget is the popup menu button that is displayed when selecting the option button of a contact
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);

    return PopupMenuButton<String>(
      key: widget.index ? new Key('FinalContactsPopUpMenuButton') : Key("ContactsPopUpMenuButton"),
      icon: Container(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Icon(
          IconData(0xe867, fontFamily: 'MaterialIcons'), 
          color:  DynamicTheme.of(context).data.primaryColorLight,
        ),
      ),
      onSelected: (String choice) async {
        if (choice == AppLocalizations.of(context).translate('contact-modify')) {
          final result = await pushOrShow(
              context,
              ContactFormOpener(
                isPost: false,
                action: AppLocalizations.of(context).translate('contact-modify'),
                contact: widget.contact
              )
          );
          if (result != null && result != "") {
            if (result == AppLocalizations.of(context).translate('server-error')) {
              serverDown = true;
            } else {
              messageSnack = "$result";
            }
          }
        } else if (choice == AppLocalizations.of(context).translate('contact-delete')) {
          // delete a contact
          final result = await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return ActivityAlertDialog(
                question: AppLocalizations.of(context).translate('wish-to-delete') + ' ' + widget.contact.nickname + ' ?',
                confirmation: AppLocalizations.of(context).translate('contact-deletee'),
              );
            }
          );
          if (result) {
            final response = await widget.contact.deleteContact(ms.api);
            if (response.statusCode == 200) {
              Provider.of<ContactProvider>(context, listen: false).remove((json.decode(response.body))['id']);
              messageSnack = widget.contact.nickname + ' ' + AppLocalizations.of(context).translate('deleted');
            } else {
              if (response.statusCode == 400) {
                messageSnack = AppLocalizations.of(context).translate('delete-fail');
              } else if (response.statusCode == 500) {
                serverDown = true;
              }
            }
          }
        }
        if (messageSnack != null && messageSnack != "") {
          showDurationDialog(context, messageSnack, 1);
        }
        if (serverDown) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(
              AppLocalizations.of(context).translate('server-error'),
              style: TextStyle(
                  fontSize: 18.0,
                  color: DynamicTheme.of(context).data.primaryColorLight,
                ),
              ),
              backgroundColor: DynamicTheme.of(context).data.errorColor,
            )
          );
          serverDown = false;
        }
      },
      itemBuilder: (BuildContext context) {
        List<ContactMenuChoices> choices = [
          new ContactMenuChoices(text: AppLocalizations.of(context).translate('contact-modify'), key: "Modify"),
          new ContactMenuChoices(text: AppLocalizations.of(context).translate('contact-delete'), key: "Delete"),
        ];

        return choices.map((choice) {
          return PopupMenuItem<String>(
              key: widget.index ? new Key('FinalContactsPopUpMenuButton' + choice.key) : Key("ContactsPopUpMenuButton" + choice.key),
              value: choice.text,
            child: Text(choice.text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: DynamicTheme.of(context).data.accentColor,
                )
              )
          );
        }).toList();
      },
    );
  }
}
