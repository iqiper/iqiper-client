import 'package:flutter/material.dart';
import 'package:iqiperclient/app/view/custom_widget/activityAlertDialog.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/FormValues.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/Model.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:iqiperclient/app/view/custom_widget/activityFormOpener.dart';

class ActivityMenuChoices {
  final String text;
  final String key;
  ActivityMenuChoices({this.text, this.key});
}

class ActivitiesPopupMenuButton extends StatefulWidget {
  final bool index;
  final Model activity;
  ActivitiesPopupMenuButton({this.activity, this.index});

  @override
  _ActivitiesPopupMenuButtonState createState() => _ActivitiesPopupMenuButtonState();
}

class _ActivitiesPopupMenuButtonState extends State<ActivitiesPopupMenuButton> {
  String messageSnack = "";
  bool serverDown = false;

  /// This widget is the popup menu button that is displayed when selecting the option button of a model
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);

    return PopupMenuButton<String>(
      key: widget.index ? new Key('FinalActivitiesPopUpMenuButton') : Key("ActivitiesPopUpMenuButton"),
      icon: Container(
        child: Icon(
          IconData(0xe867, fontFamily: 'MaterialIcons'),
          color: DynamicTheme.of(context).data.primaryColorLight,
        ),
      ),
      onSelected: (String choice) async {
        if (choice == AppLocalizations.of(context).translate('activity-menu-choice-activate')) {
          // add an activity
          final result = await pushOrShow(
            context,
            ActivityFormOpener(
              isPost: true,
              isModel: false,
              action: AppLocalizations.of(context).translate('activity-model-start'),
              formValues: FormValues(
                step: 0,
                title: widget.activity.title,
                message: widget.activity.message,
                icon: widget.activity.icon,
                id: widget.activity.id,
                constraintError: false,
                dropdownValue: 'timer',
                constraints: widget.activity.constraints,
                contactError: false,
                contactSelectionState: contactForContactSelection(Provider.of<ContactProvider>(context, listen: false).list, widget.activity.contacts),
                contacts: contactForContactSelection(widget.activity.contacts, widget.activity.contacts)
              )
            )
          );
          if (result != null && result != "") {
            if (result == AppLocalizations.of(context).translate('server-error')) {
              serverDown = true;
            } else {
              messageSnack = "$result";
            }
          }
        } else if (choice == AppLocalizations.of(context).translate('activity-menu-choice-modify')) {
          // modify a model
          final result = await pushOrShow(
            context,
              ActivityFormOpener(
                isPost: false,
                isModel: true,
                action: AppLocalizations.of(context).translate('activity-model-update'),
                formValues: FormValues(
                  step: 0,
                  title: widget.activity.title,
                  message: widget.activity.message,
                  icon: widget.activity.icon,
                  id: widget.activity.id,
                  constraintError: false,
                  dropdownValue: 'timer',
                  constraints: widget.activity.constraints,
                  contactError: false,
                  contactSelectionState: contactForContactSelection(Provider.of<ContactProvider>(context, listen: false).list, widget.activity.contacts),
                  contacts: contactForContactSelection(widget.activity.contacts, widget.activity.contacts),
                )
              )
          );
          if (result != null && result != "") {
            if (result == AppLocalizations.of(context).translate('server-error')) {
              serverDown = true;
            } else {
              messageSnack = "$result";
            }
          }
        } else if (choice == AppLocalizations.of(context).translate('activity-menu-choice-duplicate')) {
          // duplicate a model
          final response = await widget.activity.duplicateActivityModel(ms.api);
          if (response.statusCode == 201) {
            messageSnack = widget.activity.title + ' ' + AppLocalizations.of(context).translate('duplicated');
            Model model = Model.clone(widget.activity);
            model.id = json.decode(response.body)['id'];
            Provider.of<ModelProvider>(context, listen: false).add(model);
          } else {
            if (response.statusCode == 400) {
              messageSnack = widget.activity.title + ' ' + AppLocalizations.of(context).translate('duplicated-fail');
            } else if (response.statusCode == 500) {
              serverDown = true;
            }
          }
        } else if (choice == AppLocalizations.of(context).translate('activity-menu-choice-delete')) {
          // delete a model
          final result = await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return ActivityAlertDialog(
                question: AppLocalizations.of(context).translate('wish-to-delete') + ' ' + widget.activity.title + ' ?',
                confirmation: AppLocalizations.of(context).translate('activity-confirm-delete-model'),
              );
            }
          );
          if (result) {
            var response = await widget.activity.deleteActivityModel(ms.api);
            if (response != null) {
              if (response.statusCode == 200) {
                messageSnack = widget.activity.title + ' ' + AppLocalizations.of(context).translate('deleted');
                Provider.of<ModelProvider>(context, listen: false).remove(widget.activity.id);
              } else {
                if (response.statusCode == 400) {
                  messageSnack = AppLocalizations.of(context).translate('delete-fail');
                } else if (response.statusCode == 500) {
                  serverDown = true;
                }
              }
            }
          }
        }
        if (messageSnack != null && messageSnack != "") {
          showDurationDialog(context, messageSnack, 1);
        }
        if (serverDown) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(
              AppLocalizations.of(context).translate('server-error'),
              style: TextStyle(
                  fontSize: 18.0,
                  color: DynamicTheme.of(context).data.primaryColorLight,
                ),
              ),
              backgroundColor: DynamicTheme.of(context).data.errorColor,
            )
          );
          serverDown = false;
        }
      },
      itemBuilder: (BuildContext context) {
        List<ActivityMenuChoices> choices = [
          new ActivityMenuChoices(text: AppLocalizations.of(context).translate('activity-menu-choice-activate'), key: "Activate"),
          new ActivityMenuChoices(text: AppLocalizations.of(context).translate('activity-menu-choice-modify'), key: "Modify"),
          new ActivityMenuChoices(text: AppLocalizations.of(context).translate('activity-menu-choice-duplicate'), key: "Duplicate"),
          new ActivityMenuChoices(text: AppLocalizations.of(context).translate('activity-menu-choice-delete'), key: "Delete"),
        ];

        return choices.map((choice) {
          return PopupMenuItem<String>(
              key: widget.index ? new Key('FinalActivitiesPopUpMenuButton' + choice.key) : Key("ActivitiesPopUpMenuButton" + choice.key),
              value: choice.text,
              child: Text(choice.text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: DynamicTheme.of(context).data.accentColor,
                  )));
        }).toList();
      },
    );
  }
}
