import 'package:flutter/material.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/FormValues.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:iqiperclient/app/view/custom_widget/contactFavoriteSelectionDialog.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';

class AlertTile extends StatefulWidget {
  AlertTile();

  @override
  AlertTileState createState() => AlertTileState();
}

class AlertTileState extends State<AlertTile> {
  List<Contact> _contacts;
  String messageSnack;
  bool serverDown;

  @override
  void initState() {
    super.initState();
    _contacts = new List<Contact>();
    messageSnack = "";
    serverDown = false;
  }

  /// This widget is the alert slider and button use to send a manual alert
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);
    // DashboardState dashboardState = context.findAncestorStateOfType<DashboardState>();

    /// Post an activity with the template [newAlert]
    postAlert(FormValues newAlert) async {
      // MainState ms = Provider.of<MainState>(context, listen: false);
      ms.navIsLoading = true;
      final response = await newAlert.postNewActivity(ms.api);
      if (response.statusCode == 201) {
        messageSnack = "";
      } else {
        messageSnack = AppLocalizations.of(context).translate('error-occured');
      }
      ms.navIsLoading = false;
      return response;
    }

    /// function that create and post an alert and end it immediatly - emergency alert
    createManualAlert(List<Contact> contacts) async {
      List<Constraint> tmp = new List<Constraint>();
      Constraint now = new Constraint();
      // post new activity on the go
      DateTime startDate =  DateTime.now();
      DateTime notifyDate =  startDate.add(Duration(seconds: 8));
      DateTime enDate =  startDate.add(Duration(seconds: 10));
      now.setData({
        'type': 'timer',
        'icon': 60031,
        'description': 'timer description',
        'start_date': startDate.toUtc().toIso8601String(),
        'notify_date': notifyDate.toUtc().toIso8601String(),
        'end_date': enDate.toUtc().toIso8601String(),
        'alert_message': "Manual alert",
      });
      tmp.add(now);
      FormValues newAlert = FormValues(
        step: 2,
        title: "Manual alert",
        message: AppLocalizations.of(context).translate('activity-default-message'),
        icon: 62287,
        id: "",
        constraintError: false,
        dropdownValue: 'timer',
        constraints: tmp,
        constraintsEdit: tmp,
        contactError: false,
        contactSelectionState: contacts,
        contacts: contacts,
      );
      final response = await postAlert(newAlert);
      if (response.statusCode == 201) {
        Activity mn = newAlert.getNewActivity(json.decode(response.body)['id'], false);
        final responseAlert = await mn.alertActionActivity('alert', ms.api);
        if (responseAlert.statusCode == 200) {
          setState(() {
            mn.endDate = DateTime.now().toUtc().toIso8601String();
            Provider.of<ActivityProvider>(context, listen: false).addReport(mn);
          });
        } else {
          if (responseAlert.statusCode == 400) {
            messageSnack = messageSnack = AppLocalizations.of(context).translate('error-occured');
          } else if (responseAlert.statusCode == 500) {
            serverDown = true;
            messageSnack = "";
          }
        }
      } else {
        if (response.statusCode == 400) {
          messageSnack = messageSnack = AppLocalizations.of(context).translate('error-occured');
        } else if (response.statusCode == 500) {
          serverDown = true;
          messageSnack = "";
        }
      }
      _contacts = new List<Contact>();
    }

    /// Method called when pushing the alert button
    /// If there is at least one favorite contact then send an alert with this contact
    /// If there are no contacts then ask the user to add at least one contact to favorite
    triggerAlert(BuildContext context) async {
      // check favorites
      bool isThereFavourites = false;
      Set<Contact> set = Set.from(Provider.of<ContactProvider>(context, listen: false).list);
      set.forEach((e) {
        if (e != null) {
          if (e.favorite) {
            e.isSelected = true;
            _contacts.add(e);
            isThereFavourites = true;
          }
        }
      });
      // AT LEAST ONE FAVORITE CONTACT
      if (isThereFavourites) {
        ms.navIsLoading = true;
        await createManualAlert(_contacts);
        ms.navIsLoading = false;
      } else {
        // IF NO FAVORITE CONTACTS
        final tmpContact = await pushOrShow (context, FavoriteContactSelectionDialog());
        if (tmpContact != null) {
          ms.navIsLoading = true;
          final response = await tmpContact.changeFavouriteStatus(ms.api);
          if (response.statusCode == 200) {
            tmpContact.favorite = true;
            Provider.of<ContactProvider>(context, listen: false).update(tmpContact);
            tmpContact.isSelected = true;
            _contacts.add(tmpContact);
            await createManualAlert(_contacts);
          }
          ms.navIsLoading = false;
        }
      }
      if (serverDown) {
        Scaffold.of(context).showSnackBar(
          SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
            style: TextStyle(
                fontSize: 18.0,
                color: DynamicTheme.of(context).data.primaryColorLight,
              ),
            ),
            backgroundColor: DynamicTheme.of(context).data.errorColor,
          )
        );
      }
      if (messageSnack != "") {
        showDurationDialog(context, messageSnack, 1);
      }
      setState(() {
        serverDown = false;
        messageSnack = "";
      });
    }

    /// Return a widget that is the sliding alert button
    Container sliderContainer() {
      String favouriteString = '';
      String favouriteMessage = '';

      Set<Contact> set = Set.from(Provider.of<ContactProvider>(context, listen: false).list);
      set.forEach((e) => {
        if (e != null)
          {
            if (e.favorite) {favouriteString += e.nickname + ' ' + AppLocalizations.of(context).translate('and') + ' '}
          }
      });

      if (favouriteString == '') {
        favouriteMessage = AppLocalizations.of(context).translate('dashboard-alert-no-favorites');
      } else {
        favouriteString = favouriteString.substring(0, favouriteString.length - 4);
        favouriteMessage = favouriteString;
      }

        return Container(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
          height: 100,
          color: DynamicTheme.of(context).data.primaryColorDark,
          child: ListTile(
            leading: Container(
              width: 45,
              height: 45,
              child: CircleAvatar(
              backgroundColor: DynamicTheme.of(context).data.accentColor,
              child: Icon(
                IconData(62287, fontFamily: 'MaterialIcons'),
                size: 35,
                color: DynamicTheme.of(context).data.backgroundColor,
              ),
            ),
            ),
            title: Text(
                AppLocalizations.of(context).translate('dashboard-swipe-to-alert'),
                style: TextStyle(
                  color: DynamicTheme.of(context).data.primaryColorLight,
                  fontSize: 20)
            ),
            subtitle: Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Wrap(
                direction: Axis.horizontal,
                children: [
                  Icon(
                      IconData(62341, fontFamily: 'MaterialIcons'),
                      size: 18,
                      color: DynamicTheme.of(context).data.accentColor
                  ),
                  Text(" " + favouriteMessage,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: DynamicTheme.of(context).data.primaryColorLight,
                          fontSize: 18
                      ),
                  ),
              ]),
            ),
            key: Key('DashboardAlertSlider'),
          ),
        );
    }

    IconSlideAction alertSlide = IconSlideAction(
      caption: AppLocalizations.of(context).translate('dashboard-launch-alert'),
      color: DynamicTheme.of(context).data.accentColor,
      icon: IconData(58140, fontFamily: 'MaterialIcons'),
      onTap: () async { await triggerAlert(context); },
      key: Key('DashboardAlertButton'),
    );

    SlideAction alertSlideNoText = SlideAction(
      color: DynamicTheme.of(context).data.accentColor,
      child: Container(),
      onTap: () async { await triggerAlert(context); },
    );

    List<Widget> sliderWidgets() {
        return <Widget>[
          alertSlideNoText,
          alertSlide,
          alertSlideNoText,
        ];
    }

    return ms == null || ms.navIsLoading
      ? Loading()
      : Slidable(
        actionPane: SlidableScrollActionPane(),
        actionExtentRatio: 0.25,
        child: sliderContainer(),
        actions: sliderWidgets(),
        secondaryActions: sliderWidgets(),
      );
  }
}