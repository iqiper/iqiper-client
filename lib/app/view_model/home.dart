import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/view/navigation.dart';
import 'package:iqiperclient/app/view/page/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:iqiperclient/app/model/MainState.dart';

class App extends StatefulWidget {
  App() : super(key: new Key('MainappWidget'));
  @override
  MainApp createState() => MainApp();
}

class MainApp extends State<App> {
  changeState() {
    setState(() {});
  }
  
  /// This widget is used to configure the locale and theme
  /// If the user has a valid authorization token he will be redirected to the navigation widget
  /// Otherwise he will be redirected to the login page
  @override
  Widget build(BuildContext context) {
  // precacheImage(AssetImage("assets/img/logo.png"), context);
  return Consumer<MainState>(
    builder: (context, ms, child) {
     return DynamicTheme(
         data: (brightness) => ms.theme.appTheme,
         themedWidgetBuilder: (context, theme) {
        ms.api.setMainAppSetState(this);
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          locale: ms.language.appLocal,
          theme: ThemeData(
            brightness: Brightness.dark,
          ),
          supportedLocales: [
            Locale('en', 'US'),
            Locale('fr', 'FR'),
          ],
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          home: ms.api.tokenValid || ms.api.config.skipLogin ? Navigation() : Login()
        );
      });
    });
  }
}