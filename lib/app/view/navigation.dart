import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/Notification.dart';
import 'package:provider/provider.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:iqiperclient/app/view/page/dashboard.dart';
import 'package:iqiperclient/app/view/page/contacts.dart';
import 'package:iqiperclient/app/view/page/activities.dart';
import 'package:iqiperclient/app/view/page/options.dart';
import 'package:iqiperclient/app/view/page/reports.dart';

class NavigationItem {
  String title;
  IconData icon;

  NavigationItem(this.title, this.icon);

  String getKey() {
    switch (title) {
      case "dashboard":
        return "DashboardTab";
      case "contacts":
        return "ContactsTab";
      case "activities":
        return "ActivitiesTab";
      case "reports":
        return "ReportsTab";
      case "options":
        return "OptionsTab";
      default:
        throw new ArgumentError("Invalid title");
    }
  }
}

class Navigation extends StatefulWidget {
  Navigation() : super(key: new Key('NavigationWidget'));

  @override
  NavigationState createState() => NavigationState();
}

class NavigationState extends State<Navigation> {
  MainState ms;
  int _selectedDrawerIndex = 0;
  int _tabControllerIndex = 0;
  String title = "";
  ImageProvider logo = AssetImage("assets/img/logo.png");
  TabController tabController;
  @override
  void initState() {
    super.initState();
    ms = Provider.of<MainState>(context, listen: false);
    Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
    Provider.of<ContactProvider>(context, listen: false).fetchAllContacts();
    Provider.of<ModelProvider>(context, listen: false).fetchAllModels();
    _configureDidReceiveLocalNotificationSubject();
    _configureSelectNotificationSubject();
  }


  /// notification handlers
  /// iOS notification stream handler
  void _configureDidReceiveLocalNotificationSubject() {
    ms.didReceiveLocalNotificationSubject.stream.listen(
      (ReceivedNotification receivedNotification) async {
        setState(() {
          if (ms.whichScale(context)) {
            if (ms.navIsLoading) {
              tabController.index = _tabControllerIndex;
            } else {
              tabController.index = 0;
              _tabControllerIndex = 0;
            }
          } else {
            if (!ms.navIsLoading) {
              _selectedDrawerIndex = 0;
            }
          }
        });
      });
  }

  /// Android notification stream handler
  void _configureSelectNotificationSubject() {
    ms.selectNotificationSubject.stream.listen(
      (String payload) async {
        setState(() {
          if (ms.whichScale(context)) {
            if (ms.navIsLoading) {
              tabController.index = _tabControllerIndex;
            } else {
              tabController.index = 0;
              _tabControllerIndex = 0;
            }
          } else {
            if (!ms.navIsLoading) {
              _selectedDrawerIndex = 0;
            }
          }
        });
      });
  }

  /// navigation handlers part
  final List<NavigationItem> navigationItem = [
    new NavigationItem("dashboard", IconData(59035, fontFamily: 'MaterialIcons')),
    new NavigationItem("contacts", IconData(61749, fontFamily: 'MaterialIcons')),
    new NavigationItem("activities", IconData(58947, fontFamily: 'MaterialIcons')),
    new NavigationItem("reports", IconData(61585, fontFamily: 'MaterialIcons')),
    new NavigationItem("options", IconData(61662, fontFamily: 'MaterialIcons')),
  ];

  /// This widget is used for small screens (mobile, ...)
  Widget _smallScreen(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Builder(builder: (BuildContext context) {
    
        tabController = DefaultTabController.of(context);
        tabController.addListener(() {
            if (ms.navIsLoading) {
              tabController.index = _tabControllerIndex;
            } else {
              if (_tabControllerIndex != tabController.index) {
                _tabControllerIndex = tabController.index;
                _selectedDrawerIndex = tabController.index;
              }
            }
        });
    
        return Scaffold(
            appBar: AppBar(
              key: new Key('NavigationApbBar'),
              leading: IconButton(
                key: Key(navigationItem[4].getKey()),
                icon: Icon(
                  navigationItem[4].icon,
                  size: 18,
                  color: Colors.grey[500],
                ),
                onPressed: () {
                  if (!ms.navIsLoading) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            Scaffold(
                                appBar: AppBar(
                                  key: new Key('NavigationApbBar'),
                                  leading: IconButton(
                                    key: Key(navigationItem[4].getKey()),
                                    icon: Icon(
                                      IconData(0xe867, fontFamily: 'MaterialIcons'),
                                      size: 18,
                                      color: Colors.grey[500],
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                  title: Text(
                                    AppLocalizations.of(context).translate('options'),
                                    key: new Key('NavigationApbBarTitle'),
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: DynamicTheme
                                          .of(context)
                                          .data
                                          .accentColor,
                                    ),
                                  ),
                                ),
                                body: Options()
                            ),
                      ),
                    );
                  }
                },
              ),
              title: Align(
                alignment: Alignment.centerRight,
                child: Image(image: logo, width: 55.0, height: 55.0,)
              ),
            ),
            body: TabBarView(
            controller: tabController,
            children:
              [
                Dashboard(),
                Contacts(),
                Activities(),
                Reports(),
              ],
            ),
            bottomNavigationBar: TabBar(
              key: new Key('NavigationTabBar'),
              indicatorWeight: 0.9,
              isScrollable: false,
              tabs: [
                Tab(
                    key: new Key(navigationItem[0].getKey()),
                    icon: Icon(
                      navigationItem[0].icon,
                      size: 30,
                    ),
                    child: Text(AppLocalizations.of(context).translate(navigationItem[0].title), style: TextStyle(fontSize: 10))),
                Tab(
                    key: new Key(navigationItem[1].getKey()),
                    icon: Icon(
                      navigationItem[1].icon,
                      size: 30,
                    ),
                    child: Text(AppLocalizations.of(context).translate(navigationItem[1].title), style: TextStyle(fontSize: 10))),
                Tab(
                    key: new Key(navigationItem[2].getKey()),
                    icon: Icon(
                      navigationItem[2].icon,
                      size: 30,
                    ),
                    child: Text(AppLocalizations.of(context).translate(navigationItem[2].title), style: TextStyle(fontSize: 10))),
                Tab(
                    key: new Key(navigationItem[3].getKey()),
                    icon: Icon(
                      navigationItem[3].icon,
                      size: 30,
                    ),
                    child: Text(AppLocalizations.of(context).translate(navigationItem[3].title), style: TextStyle(fontSize: 10))),
              ],
              labelColor: DynamicTheme
                  .of(context)
                  .data
                  .accentColor,
              indicatorColor: DynamicTheme
                  .of(context)
                  .data
                  .accentColor,
              unselectedLabelColor: Colors.grey[600],
              indicatorSize: TabBarIndicatorSize.label,
              indicatorPadding: EdgeInsets.all(5.0),
            ),
          );
      })
    );
  }

  /// This widget is used for large screens (web, tablet, ...)
  Widget _largeScreen(BuildContext context) {
    /// Get corresponding navigation item from index [pos]
    _getDrawerItemWidget(int pos) {
      switch (pos) {
        case 0:
          return Dashboard();
        case 1:
          return Contacts();
        case 2:
          return Activities();
        case 3:
          return Reports();
        case 4:
          return Options();
        default:
          return FullScreenLoading();
      }
    }

    /// Set selected view to number [index] and close drawer
    _onSelectItem(int index) {
      if (!ms.navIsLoading) {
        setState(() {
          // tabController.index = index;
          _selectedDrawerIndex = index;
        });
      }
      Navigator.of(context).pop(); // close the drawer
    }

    // Add each navigation items to the drawer
    var drawerOptions = <Widget>[];
    for (var i = 0; i < navigationItem.length; i++) {
      var d = navigationItem[i];
      drawerOptions.add(
        Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(width: 2, color: Colors.grey[600])),
            ),
            child: ListTileTheme(
                selectedColor: DynamicTheme.of(context).data.accentColor,
                iconColor: Colors.grey[600],
                textColor: Colors.grey[600],
                child: ListTile(
                  key: new Key(d.getKey()),
                  leading: Icon(d.icon),
                  title: Text(
                    AppLocalizations.of(context).translate(d.title),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  contentPadding: EdgeInsets.all(10.0),
                  selected: i == _selectedDrawerIndex,
                  onTap: () => _onSelectItem(i),
                ))),
      );
    }
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        key: new Key('NavigationApbBar'),
        leading: IconButton(
          tooltip: "Open navigation menu",
          icon: Icon(IconData(0xe867, fontFamily: 'MaterialIcons')),
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();
          },
          key: new Key('OpenDrawer'),
        ),
        iconTheme: IconThemeData(
          color: DynamicTheme.of(context).data.accentColor,
        ),
        title: Text(navigationItem[_selectedDrawerIndex].title,
            key: new Key('NavigationApbBarTitle'),
            textAlign: TextAlign.center,
            style: TextStyle(
              color: DynamicTheme.of(context).data.accentColor,
            )),
        actions: [
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.all(15.0),
                child: Text(
                'iqiper',
                textAlign: TextAlign.right,
                style: TextStyle(
                  color: DynamicTheme.of(context).data.accentColor,
                  fontSize: 24,
                ),
              ),
            ),
          ),
        ],
      ),
      drawer: Drawer(
        key: new Key('NavigationDrawer'),
        child: ListView(
          key: new Key('NavigationTabBar'),
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Image(image: logo, width: 55.0, height: 55.0,),
              key: new Key('DrawerHeader'),
            ),
            Column(
                key: new Key('NavigationTab'),
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: drawerOptions)
          ],
        ),
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Provider.of<MainState>(context).whichScale(context) ? _smallScreen(context) : _largeScreen(context);
  }
}
