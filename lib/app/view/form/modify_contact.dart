import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/view_model/form/contactForm.dart';

class UpdateContact extends StatefulWidget {
  final Contact contact;
  const UpdateContact({Key key, this.contact}) : super(key: key);

  @override
  UpdateContactState createState() {
    return UpdateContactState();
  }
}

class UpdateContactState extends State<UpdateContact> {

  /// This widget is the form used to udpate the nickname and favorite status of a contact
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
    child:  Container(
            margin: EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: DynamicTheme.of(context).data.primaryColor,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(25.0))),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: 320,
                      maxWidth: 380,
                    ),
                    child: ContactForm(
                      form: "nickname",
                      isPost: false,
                      intro: AppLocalizations.of(context).translate('contact-form-update-intro'),
                      label: AppLocalizations.of(context).translate('contact-form-update-label'),
                      hint: AppLocalizations.of(context).translate('contact-form-nickname-hint'),
                      button: AppLocalizations.of(context).translate('save'), 
                      contact: new Contact.clone(widget.contact)
                    ),
          ),
        ),
      ]),
      )
    );
  }
}