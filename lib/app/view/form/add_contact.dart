import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/view_model/form/contactForm.dart';
import 'package:iqiperclient/app/view/custom_widget/searchByUsername.dart';

class AddContact extends StatefulWidget {
  @override
  AddContactState createState() => AddContactState();
}

class AddContactState extends State<AddContact> {
  int addType = 0;
  int steps = 0;
  Contact newContact = new Contact(favorite: false);
  List<Contact> potentialUsers = new List();

  /// This widget is the form used to setup the nickname and favorite status of a contact
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child:
          SingleChildScrollView(
            child: 
              /// form content
              addType == 1 && steps == 1
              // check username
                ? SearchUsername(username: newContact.username, users: potentialUsers)
                : (addType == 1 && steps == 2) || (addType == 2 && steps == 1)
              // once the email or username is checked by the back then user set nickname
                ? Container(
                margin: EdgeInsets.all(10.0),
                child: Container(
                    decoration: BoxDecoration(
                    color: DynamicTheme.of(context).data.primaryColor,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(25.0))),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 320,
                        maxWidth: 380,
                      ),
                      child: ContactForm(
                        form: "nickname",
                        isPost: true,
                        intro: AppLocalizations.of(context).translate('contact-form-nickname-intro'),
                        label: AppLocalizations.of(context).translate('contact-form-nickname-label'),
                        hint: AppLocalizations.of(context).translate('contact-form-nickname-hint'),
                        button: AppLocalizations.of(context).translate('save'),
                      ),
                    ),
                  ),
                )
              // else it asks for email or username
                : Container(
                    margin: EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: DynamicTheme.of(context).data.primaryColor,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.all(Radius.circular(25.0))),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              minWidth: 320,
                              maxWidth: 380,
                            ),
                            child: addType == 1 && steps == 0
                              ? ContactForm(
                                  form: "username",
                                  isPost: true,
                                  intro: AppLocalizations.of(context).translate('contact-search-on-iqiper'),
                                  label: AppLocalizations.of(context).translate('contact-form-username-label'),
                                  hint: AppLocalizations.of(context).translate('contact-form-username-hint'),
                                  button: AppLocalizations.of(context).translate('search')
                                )
                              : Padding(
                              padding: EdgeInsets.fromLTRB(25, 25, 25, 25),
                              child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  addType = addType == 0  || addType == 2 ? 1 : 0;
                                });
                              },
                            child: Container(
                              key: Key("openUsernameContactsForm"),
                              child: Text(
                                AppLocalizations.of(context).translate('contact-search-on-iqiper'),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: addType == 0 ? DynamicTheme.of(context).data.accentColor : DynamicTheme.of(context).data.primaryColorDark,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 0, top: 5, right: 0, bottom: 0),
                      decoration: BoxDecoration(
                        color: DynamicTheme.of(context).data.primaryColor,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(25.0))),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              minWidth: 320,
                              maxWidth: 380,
                            ),
                            child: addType == 2
                            ? ContactForm(
                                form: "email",
                                isPost: true,
                                intro: AppLocalizations.of(context).translate('contact-save-mail'),
                                label: AppLocalizations.of(context).translate('contact-form-email-label'),
                                hint: AppLocalizations.of(context).translate('contact-form-email-hint'),
                                button: AppLocalizations.of(context).translate('save'),
                              )
                            : Padding(
                            padding: EdgeInsets.fromLTRB(25, 25, 25, 25),
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  addType = addType == 0 || addType == 1 ? 2 : 0;
                                });
                              },
                              child: Container(
                                key: Key("openEmailContactsForm"),
                                child: Text(
                                  AppLocalizations.of(context).translate('contact-save-mail'),
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: addType == 0 ? DynamicTheme.of(context).data.accentColor : DynamicTheme.of(context).data.primaryColorDark,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}