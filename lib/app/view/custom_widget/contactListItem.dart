import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view_model/custom_widget/contactFavoriteButton.dart';
import 'package:iqiperclient/app/view_model/custom_widget/contactsPopupMenuButton.dart';
import 'loading.dart';

class ContactListItem extends StatefulWidget {
  final int index;
  ContactListItem({this.index});
  @override
  ContactListItemState createState() => ContactListItemState();
}

class ContactListItemState extends State<ContactListItem> {
  bool selected = false;

  /// This is a contact item that is displayed in the contacts tab
  @override
  Widget build(BuildContext context) {
    List<Contact> _contacts = Provider.of<ContactProvider>(context, listen: false).list;

    return _contacts == null
        ? Loading()
        : Center(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 320,
                  maxWidth: 600,
                ),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      selected = !selected;
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                    key: (widget.index == _contacts.length - 1) ? new Key('FinalContactsListTile') : Key("ContactsListTile" + widget.index.toString()),
                    decoration: BoxDecoration(
                      color: DynamicTheme.of(context).data.primaryColorDark,
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                    child: Row(
                      children: [
                      // leading:
                      Container(
                        key: (widget.index == _contacts.length - 1)
                            ? new Key('FinalContactsListTileLeading')
                            : Key("ContactsListTileLeading" + widget.index.toString()),
                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Container(
                          width: 65,
                          height: 65,
                          child: Center(
                            child: Icon(
                              IconData(62399, fontFamily: 'MaterialIcons'),
                              size: 45,
                              color: DynamicTheme.of(context).data.primaryColorLight,
                            ),
                          ),
                        ),
                      ),
                      // title:
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                          child: Wrap(direction: Axis.vertical, children: [
                            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, mainAxisSize: MainAxisSize.max, children: <Widget>[
                              Text(
                                _contacts[widget.index].nickname,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                ),
                                key: (widget.index == _contacts.length - 1)
                                    ? Key('FinalContactsTileNickname')
                                    : Key("ContactsTileNickname" + widget.index.toString()),
                              ),
                              Favorite(contact: _contacts[widget.index], fKey: (widget.index == _contacts.length - 1)
                                  ? "FinalContactFavorite"
                                  : "ContactFavorite" + widget.index.toString()),
                            ]),
                            selected
                                ? Container(
                                    padding: EdgeInsets.symmetric(vertical: 3),
                                    child: Text(
                                      _contacts[widget.index].subtitle,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 15.0,
                                        color: DynamicTheme.of(context).data.primaryColorLight,
                                      ),
                                      key: (widget.index == _contacts.length - 1)
                                          ? new Key('FinalContactsTileSub')
                                          : Key("ContactsTileSub" + widget.index.toString()),
                                    ),
                                  )
                                : Container()
                          ]),
                        ),
                      ),
                      // trailing:
                      ContactPopupMenuButton(contact: _contacts[widget.index], index: widget.index == _contacts.length - 1)
                      // ),
                    ]),
                  ),
                ),
              ),
            ),
          );
  }
}
