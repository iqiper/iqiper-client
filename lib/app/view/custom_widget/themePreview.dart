import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:iqiperclient/app/model/AppTheme.dart';
import 'package:iqiperclient/app/model/MainState.dart';

class ThemePreview extends StatelessWidget {
  final IqiperTheme theme;
  ThemePreview({this.theme});

  /// This widget is a preview of the color pattern from the theme [theme]
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);
    return GestureDetector(
        onTap: () => ms.changeTheme(theme),
        child: Stack(
          children: [
            Container(
              height: 300,
              width: 150,
              key: Key(ms.theme.appThemeName == theme.name ? "selected " + theme.name : ""),
              decoration: new BoxDecoration(
                  color: ms.theme.appThemeName == theme.name ? Color(0x55FFFFFF) : Colors.transparent,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(25.0))
              ),
            ),
            Container(
                padding: EdgeInsets.only(left: 15, top: 30),
                child: Stack(
                  children: [
                    Container(
                      height: 240,
                      width: 120,
                      decoration: new BoxDecoration(
                          color: theme.theme.backgroundColor,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(12))
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 120,
                      decoration: new BoxDecoration(
                          color: Colors.grey[800],
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.vertical(top: Radius.circular(12))
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 210),
                      height: 30,
                      width: 120,
                      decoration: new BoxDecoration(
                          color: Colors.grey[800],
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.vertical(bottom: Radius.circular(12))
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, top: 60),
                      height: 40,
                      width: 80,
                      decoration: new BoxDecoration(
                        color: theme.theme.primaryColorDark,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 32, top: 72),
                      height: 16,
                      width: 55,
                      decoration: new BoxDecoration(
                        color: theme.theme.primaryColorLight,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, top: 120),
                      height: 70,
                      width: 80,
                      decoration: new BoxDecoration(
                        color: theme.theme.primaryColorDark,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 32, top: 132),
                      height: 16,
                      width: 55,
                      decoration: new BoxDecoration(
                        color: theme.theme.primaryColor,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 32, top: 162),
                      height: 16,
                      width: 55,
                      decoration: new BoxDecoration(
                        color: theme.theme.canvasColor,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 22, top: 218.5),
                      height: 12,
                      width: 12,
                      decoration: new BoxDecoration(
                        color: theme.theme.primaryColorLight,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 54, top: 218.5),
                      height: 12,
                      width: 12,
                      decoration: new BoxDecoration(
                        color: theme.theme.backgroundColor,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 86, top: 218.5),
                      height: 12,
                      width: 12,
                      decoration: new BoxDecoration(
                        color: theme.theme.backgroundColor,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 240),
                      height: 300,
                      width: 120,
                      child: Center(child: Text(theme.name, style: TextStyle(fontSize: 15, color: Color(0xFFBDD9BF)))),
                    )
                  ],
                )
            )
          ],
        )
    );
  }
}