import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Communication.dart';
import 'package:iqiperclient/app/view/custom_widget/ReportResume.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:iqiperclient/app/view_model/custom_widget/reportPopupMenuButton.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
// import 'package:iqiperclient/app/model/CommunicationLog.dart';
// import 'package:iqiperclient/app/model/Constraint.dart';
// import 'package:iqiperclient/app/model/Contact.dart';

class ReportListItem extends StatefulWidget {
  final int index;
  ReportListItem({this.index});
  @override
  ReportListItemState createState() => ReportListItemState();
}

class ReportListItemState extends State<ReportListItem> {
  double itemNb = 0;
  Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (Timer t) => setState(() {}));
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  /// This is an activity item that is displayed in the dashboard tab
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);
    List<Activity> _reports = Provider.of<ActivityProvider>(context, listen: false).reportList.reversed.toList();

    /// Get activity [activity]'s data to be displayed and format them in a list
    List<Widget> getReportDisplayData(Activity activity) {
      String dataText;

      if (activity.endDate == null) {
        dataText = AppLocalizations.of(context).translate('running') + '...';
      } else {
        dataText = 'Date: ' +
          DateFormat('dd/MM/yyyy HH:mm').format(DateTime.parse(activity.endDate).toUtc().add(DateTime.now().timeZoneOffset));
      }
      List<Widget> widgets = [
        Text(
          activity.title,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontSize: 18.0,
            color: DynamicTheme.of(context).data.primaryColorLight,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 3),
          child: Row(children: [
            Text(
              dataText,
              style: TextStyle(
                fontSize: 15.0,
                color: DynamicTheme.of(context).data.primaryColorLight,
              ),
            ),
          ]),
        ),
      ];
      return widgets;
    }

    return _reports != null
        ? _reports[widget.index] != null ?
        Center(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 320,
                  maxWidth: 600,
                ),
                child: GestureDetector(
                    onTap: () async {
                      List<Communication> comList = new List<Communication>();
                      var responseActivityCommunications = await ms.api.get(
                        '/activity/' + _reports[widget.index].id + '/communication',
                        false, ms.api.config.maxNumberOfRequestAttempt);
              
                      if (responseActivityCommunications.statusCode == 200) {
                        for (var e in json.decode(responseActivityCommunications.body)) {
                          Communication tmpCom = communicationFromJson(e, _reports[widget.index], null, ms.api);
                          await tmpCom.fetchLogs(ms.api, context);
                          comList.add(tmpCom);
                        // comList.add(new Communication.full(
                        //   logs: [
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //   ],
                        //   type: 'email',
                        //   reason: 'alert',
                        //   createdAt: '1970-01-01 00:00:00.000Z',
                        //   linkedConstraint: new Constraint.fromForm(
                        //     type: 'timer',
                        //     message: 'sjbfjasgjhsagjhakghdaklghkladhgklahdkghkladhgkldhgklhadklghkadlhgkdhglkhadlkghadklghkhadklgh',
                        //   ),
                        //   linkedActivity: new Activity(
                        //     constraints: [new Constraint.fromForm(
                        //       type: 'timer',
                        //       message: 'the messagethe messagethe messagethe messagethe messagethe messagethe messagethe message',
                        //     )],
                        //     message: 'the messagethe messagethe messagethe messagethe messagethe messagethe messagethe message',
                        //   ),
                        // ));
                        // comList.add(new Communication.full(
                        //   logs: [
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                            // new CommunicationLog(
                            //   states: 'alert',
                            //   createdAt: '1970-01-01 00:00:00.000Z',
                            //   linkedContact: new Contact(
                            //     nickname: 'alex',
                            //   ),
                            // ),
                            // new CommunicationLog(
                            //   states: 'alert',
                            //   createdAt: '1970-01-01 00:00:00.000Z',
                            //   linkedContact: new Contact(
                            //     nickname: 'alex',
                            //   ),
                            // ),
                        //   ],
                        //   type: 'email',
                        //   reason: 'alert',
                        //   createdAt: '1970-01-01 00:00:00.000Z',
                        //   linkedConstraint: new Constraint.fromForm(
                        //     type: 'timer',
                        //     message: 'sjbfjasgjhsagjhakghdaklghkladhgklahdkghkladhgkldhgklhadklghkadlhgkdhglkhadlkghadklghkhadklgh',
                        //   ),
                        //   linkedActivity: new Activity(
                        //     constraints: [new Constraint.fromForm(
                        //       type: 'timer',
                        //       message: 'the messagethe messagethe messagethe messagethe messagethe messagethe messagethe message',
                        //     )],
                        //     message: 'the messagethe messagethe messagethe messagethe messagethe messagethe messagethe message',
                        //   ),
                        // ));
                        }
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ReportResume(report: _reports[widget.index], activityCommunications: comList)
                          )
                        );
                      } else {
                        if (responseActivityCommunications.statusCode == 400) {
                          String messageError = AppLocalizations.of(context).translate('error-occured');
                          showDurationDialog(context, messageError, 1);
                        } else if (responseActivityCommunications.statusCode == 500) {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                ),
                              ),
                              backgroundColor: DynamicTheme.of(context).data.errorColor,
                            )
                          );
                        }
                      }
                    },
                    child: Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                        decoration: BoxDecoration(
                          color: DynamicTheme.of(context).data.primaryColorDark,
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        key: (widget.index == _reports.length - 1)
                            ? Key("FinalReportListTile")
                            : Key("ReportListTile" + widget.index.toString()),
                        child: Row(
                          children: [
                            // leading:
                            Container(
                              key: (widget.index == _reports.length - 1)
                                  ? new Key('FinalReportListTileLeading')
                                  : Key("ReportListTileLeading" + widget.index.toString()),
                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Stack(children: <Widget>[
                                Container(
                                  width: 45,
                                  height: 45,
                                  child: Center(
                                    child: Icon(IconData(_reports[widget.index].icon, fontFamily: 'MaterialIcons'),
                                      color: DynamicTheme.of(context).data.accentColor, size: 45),
                                  ),
                                ),
                              ]),
                            ),
                            // title:
                            Expanded(
                              child: Container(
                              key: (widget.index == _reports.length - 1)
                                  ? new Key('FinalReportListTileTitle')
                                  : Key("ReportListTileTitle" + widget.index.toString()),
                              padding: EdgeInsets.fromLTRB(20, 10, 0, 10),
                              child: Wrap(
                                children: getReportDisplayData(_reports[widget.index]),
                              ),
                            )),
                            // trailing:
                            ReportPopupMenuButton(activity: _reports[widget.index], index: (widget.index == _reports.length - 1)),
                          ],
                        )),
                  )
              ),
            ),
          ) : Loading() : Loading();
  }
}
