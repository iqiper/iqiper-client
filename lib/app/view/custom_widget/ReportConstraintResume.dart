import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Communication.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/view/custom_widget/communicationListItem.dart';

class ReportConstraintResume extends StatelessWidget {
  final Constraint constraint;
  final List<Communication> constraintCommunications;

  ReportConstraintResume({this.constraint, this.constraintCommunications}) : super(key: new Key('ReportConstraintResumeWidget'));

  @override
  Widget build(BuildContext context) {
    Widget _communicationListItem(BuildContext context, int index) {
      return CommunicationListItem(communication: constraintCommunications[index]);
    }

    return Scaffold(
        key: Key('CommunicationPreviewWindow'),
        backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
        appBar: AppBar(
            leading: IconButton(
              key: Key('CommunicationResumeBack'),
              icon: Icon(IconData(58791, fontFamily: 'MaterialIcons'),
                  size: 35,
                  color: DynamicTheme.of(context).data.backgroundColor),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              AppLocalizations.of(context).translate('reports'),
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: DynamicTheme.of(context).data.primaryColor,
              ),
            ),
        ),
        body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width * .9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: SingleChildScrollView(
                    child: Container(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Wrap(direction: Axis.vertical, children: [
                          ConstrainedBox(
                            constraints: BoxConstraints(
                              minWidth: 320,
                              maxWidth: 600,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                color: DynamicTheme.of(context)
                                    .data
                                    .backgroundColor,
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                              child: Wrap(
                                direction: Axis.vertical,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      alignment: WrapAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: 200,
                                          padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                                          child:
                                            Text(
                                              constraint.type,
                                              textAlign: TextAlign.left,
                                              maxLines: 30,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 18.0,
                                                color: DynamicTheme.of(context).data.primaryColorLight,
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      alignment: WrapAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)
                                                  .translate('start-date') +
                                              " : " + DateFormat('dd/MM/yyyy HH:mm').format(DateTime.parse(constraint.startDate).toUtc().add(DateTime.now().timeZoneOffset)),
                                          key: Key('ReportResumeActivityContacts'),
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            color: DynamicTheme.of(context)
                                                .data
                                                .primaryColorLight,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 30),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      alignment: WrapAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)
                                                  .translate('end-date') +
                                              " : " + DateFormat('dd/MM/yyyy HH:mm').format(DateTime.parse(constraint.endDate).toUtc().add(DateTime.now().timeZoneOffset)),
                                          key: Key('ReportResumeActivityContacts'),
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            color: DynamicTheme.of(context)
                                                .data
                                                .primaryColorLight,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Text('Communications:',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: DynamicTheme.of(context).data.accentColor
                                ),
                              ),
                            ),
                            Container (
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              height: MediaQuery.of(context).size.height * .7,
                              width: MediaQuery.of(context).size.width * 0.9,
                                child: ListView.builder(
                                  key: Key('CommunicationListView'),
                                  padding: EdgeInsets.symmetric(vertical: 5.0),
                                  itemCount: constraintCommunications.length,
                                  itemBuilder: _communicationListItem
                                ),
                            ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }
}
