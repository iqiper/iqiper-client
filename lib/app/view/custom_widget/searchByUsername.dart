import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/form/add_contact.dart';
import 'package:provider/provider.dart';

class SearchUsername extends StatefulWidget {
  final String username;
  final List<Contact> users;
  SearchUsername({Key key, this.username, this.users}) : super(key: key);

  @override
  SearchUsernameState createState() => SearchUsernameState();
}

class SearchUsernameState extends State<SearchUsername> {
  @override
  Widget build(BuildContext context) {
    AddContactState addContactState = context.findAncestorStateOfType<AddContactState>();
    return Center(
      child: Container(
        key: Key('SearchUsernameEmpty'),
        color: DynamicTheme.of(context).data.primaryColorDark,
        width: Provider.of<MainState>(context, listen: false).whichScale(context) 
          ? MediaQuery.of(context).size.width
          : MediaQuery.of(context).size.width * .5,
        height: MediaQuery.of(context).size.height * .8,
          child: widget.users.length == null || widget.users.length == 0
            ? Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: Text(
                      AppLocalizations.of(context).translate('contact-form-user-not-found'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 23.0,
                        color: DynamicTheme.of(context).data.canvasColor,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 25),
                  child: Container(
                    height: 50,
                    width: 200,
                    child: RaisedButton(
                        elevation: 5,
                        padding: EdgeInsets.only(top: 1),
                        child: Text(
                          AppLocalizations.of(context).translate('return'),
                          style: TextStyle(
                            color: DynamicTheme.of(context).data.primaryColor,
                          ),
                        ),
                        color: DynamicTheme.of(context).data.canvasColor,
                        onPressed: () {
                          addContactState.setState(() {
                            // return to username form
                            addContactState.steps = 0;
                          });
                      }),
                  ),
                ),
              ])
            : ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                key: Key('SearchUsernameListView'),
                padding: EdgeInsets.symmetric(vertical: 8.0),
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                    child: Center(
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minWidth: 320,
                          minHeight: 80,
                          maxWidth: 400,
                          maxHeight: 100,
                        ),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          margin: EdgeInsets.symmetric(vertical: 5),
                          decoration: BoxDecoration(
                              color: DynamicTheme.of(context).data.primaryColor,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.all(Radius.circular(25.0))),
                          child: ListTile(
                            key: (index == widget.users.length - 1) ? new Key('FinalUsersListTile') : Key("UsersListTile" + index.toString()),
                            leading: Icon(
                              IconData(62399, fontFamily: 'MaterialIcons'),
                              key: (index == widget.users.length - 1) ? new Key('FinalUsersListTileLeading') : Key("UsersListTileLeading" + index.toString()),
                              size: 45,
                              color: DynamicTheme.of(context).data.accentColor
                            ),
                            hoverColor: Color.fromRGBO(0, 0, 0, 0),
                            contentPadding: EdgeInsets.all(10),
                            title: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(children: [
                                  Text(
                                    widget.users[index].username,
                                    key: (index == widget.users.length - 1) ? new Key('FinalUsersListTileUsername') : Key("UsersListTileUsername" + index.toString()),
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: DynamicTheme.of(context).data.primaryColorLight,
                                    ),
                                  )
                                ]),
                                widget.users[index].firstName == null
                                    ? Text("")
                                    : Row(children: [
                                        (widget.users[index].firstName + ' ' + widget.users[index].lastName).length <= 24
                                            ? Text(
                                                widget.users[index].firstName + ' ' + widget.users[index].lastName,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                                ),
                                              )
                                            : Text(
                                                (widget.users[index].firstName + ' ' + widget.users[index].lastName).substring(0, 24),
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                                ),
                                              ),
                                      ])
                              ],
                            ),
                            onTap: () async {
                              addContactState.setState(() {
                                addContactState.newContact.setUid(widget.users[index].uid);
                                if (widget.users[index].firstName != null) {
                                  addContactState.newContact.setNames(widget.users[index].firstName, widget.users[index].lastName);
                                }
                                addContactState.steps += 1;
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                    
                  );
                },
                itemCount: widget.users.length
              )
            )
    );
  }
}
