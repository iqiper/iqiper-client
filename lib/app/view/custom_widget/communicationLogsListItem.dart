import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:intl/intl.dart';
import 'package:iqiperclient/app/model/CommunicationLog.dart';

class CommunicationLogsListItem extends StatefulWidget {
  final CommunicationLog log;
  CommunicationLogsListItem({this.log});

  @override
  CommunicationLogsListItemState createState() => CommunicationLogsListItemState();
}

class CommunicationLogsListItemState extends State<CommunicationLogsListItem> {
  /// This is a communication log item that is displayed in the communication resume
  @override
  Widget build(BuildContext context) {
    return Center(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 0),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 320,
                  maxWidth: 600,
                ),
                child: Container(
                    decoration: BoxDecoration(
                      color: DynamicTheme.of(context).data.primaryColor,
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                    child: Row(
                      children: [
                        // title:
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 0, 10),
                            child: Wrap(
                              direction: Axis.vertical,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Status: ' + widget.log.states.toString(),
                                    style: TextStyle(
                                      color: DynamicTheme.of(context).data.accentColor
                                    )),
                                    Text('Linked contact: ' + widget.log.linkedContact.nickname.toString(),
                                    style: TextStyle(
                                      color: DynamicTheme.of(context).data.accentColor
                                    )),
                                    Text('Created_at: ' + DateFormat('dd/MM/yyyy HH:mm').format(DateTime.parse(widget.log.createdAt).toUtc().add(DateTime.now().timeZoneOffset)),
                                    style: TextStyle(
                                      color: DynamicTheme.of(context).data.accentColor
                                    )),
                                  ]
                                )
                              ],
                            ),
                          ),
                        ),
                      ]
                    ),
                  ),
              ),
            ),
          );
  }
}
