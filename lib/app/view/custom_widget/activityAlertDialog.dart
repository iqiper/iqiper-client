import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';

class ActivityAlertDialog extends StatelessWidget {
  final String question;
  final String confirmation;

  ActivityAlertDialog({this.question, this.confirmation});

  /// This widget is the dialog that is displayed when an action notify or delete is chosen for activities
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      key: Key('ActivityAlertDialog'),
      backgroundColor: DynamicTheme.of(context).data.accentColor,
      title: Text(
        question,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 15.0,
          fontWeight: FontWeight.bold,
          color: DynamicTheme.of(context).data.backgroundColor,
        ),
      ),
      actions: <Widget>[
        FlatButton(
          key: Key('ValidateActivityAlertDialog'),
          onPressed: () async {
            Navigator.pop(context, true);
          },
          child: Text(
            confirmation,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              color: DynamicTheme.of(context).data.primaryColorDark,
            ),
          ),
        ),
        FlatButton(
          key: Key('ActivityAlertDialogBack'),
          onPressed: () {
            Navigator.pop(context, false);
          },
          child: Text(
            AppLocalizations.of(context).translate('cancel'),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 15.0,
              color: DynamicTheme.of(context).data.primaryColor,
            ),
          ),
        ),
      ],
    );
  }
}