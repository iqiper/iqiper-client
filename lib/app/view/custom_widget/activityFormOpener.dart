import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/FormValues.dart';
import 'package:iqiperclient/app/view_model/form/activityForm.dart';

class ActivityFormOpener extends StatefulWidget {
  final bool isPost;
  final bool isModel;
  final String action;
  final FormValues formValues;

  ActivityFormOpener({this.formValues, this.action, this.isPost, this.isModel});
  ActivityFormOpenerState createState() => ActivityFormOpenerState();
}

class ActivityFormOpenerState extends State<ActivityFormOpener> {
  bool isLoading = false;

  Widget _smallScreen(BuildContext context) {
    return Scaffold(
        backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
        appBar: AppBar(
          leading: IconButton(
            key: Key('ActivityFormBack'),
            icon: Icon(IconData(58791, fontFamily: 'MaterialIcons'),
                size: 35, color: DynamicTheme.of(context).data.backgroundColor),
            onPressed: () {
              if (!isLoading) {
                Navigator.pop(context, "");
              }
            },
          ),
          title: Text(
            widget.action,
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              color: DynamicTheme.of(context).data.primaryColor,
            ),
          ),
          backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
        ),
        body: Center(
          child: Container(
              color: DynamicTheme.of(context).data.primaryColorDark,
              width: MediaQuery.of(context).size.width * .9,
              child: Column(children: <Widget>[
                widget.isModel
                    ? ActivityForm(
                        initValues: widget.formValues,
                        isPost: widget.isPost,
                        isModel: widget.isModel)
                    : ActivityForm(
                        initValues: widget.formValues,
                        isPost: widget.isPost,
                        isModel: widget.isModel)
              ])),
        ));
  }

  Widget _largeScreen(BuildContext context) {
    return SimpleDialog(
      backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
      contentPadding: EdgeInsets.all(20),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      children: <Widget>[
        Container(
          width: 400,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
          child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: Column(
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                              key: Key('ActivityFormBack'),
                              icon: Icon(
                                  IconData(58791, fontFamily: 'MaterialIcons'),
                                  size: 35,
                                  color: DynamicTheme.of(context)
                                      .data
                                      .backgroundColor),
                              onPressed: () {
                                if (!isLoading) {
                                  Navigator.pop(context, "");
                                }
                              },
                            ),
                            Container(
                              child: Text(
                                widget.action,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  color: DynamicTheme.of(context)
                                      .data
                                      .primaryColor,
                                ),
                              ),
                            ),
                          ]),
                        Center(
                          child: Container(
                            color: DynamicTheme.of(context).data.primaryColorDark,
                            width: MediaQuery.of(context).size.width * .9,
                            height: MediaQuery.of(context).size.height * .8,
                            child: Column(
                              children: <Widget>[
                                widget.isModel
                                ? ActivityForm(
                                    initValues: widget.formValues,
                                    isPost: widget.isPost,
                                    isModel: widget.isModel)
                                : ActivityForm(
                                    initValues: widget.formValues,
                                    isPost: widget.isPost,
                                    isModel: widget.isModel)
                              ],
                            )),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _smallScreen(context);
  }
}
