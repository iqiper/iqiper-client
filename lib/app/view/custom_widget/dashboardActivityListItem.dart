import 'dart:async';
import 'package:flutter/material.dart';
import 'package:iqiperclient/app/view/custom_widget/activityAlertDialog.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:iqiperclient/app/view/custom_widget/messagePreview.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:iqiperclient/app/view_model/custom_widget/dashboardPopupMenuButton.dart';

class DashboardActivityListItem extends StatefulWidget {
  final int index;
  DashboardActivityListItem({this.index});
  @override
  DashboardActivityListItemState createState() => DashboardActivityListItemState();
}

class DashboardActivityListItemState extends State<DashboardActivityListItem> {
  bool selected = false;
  double itemNb = 0;
  Timer _timer;
  String messageSnack = "";
  
  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (Timer t) => setState(() {}));
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  /// This is an activity item that is displayed in the dashboard tab
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);
    List<Activity> _activity = Provider.of<ActivityProvider>(context, listen: false).list;

    /// Get activity [activity]'s icon animation progress value
    double getActivityIconValue(Activity activity) {
      double shortestOffset = 0;
      int nearest = 0;

        calculateOffset() {
          for (int i = 0; i < activity.constraints.length; i++) {
            if (activity.constraints[i].type == "timer") {
              int now = DateTime.now().toUtc().add(DateTime.now().timeZoneOffset).millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond;
              int start = DateTime.parse(activity.constraints[i].startDate).toUtc().add(DateTime.now().timeZoneOffset).millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond;
              int end = DateTime.parse(activity.constraints[i].endDate).toUtc().add(DateTime.now().timeZoneOffset).millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond;
              int passed = now - start;
              int endOffset = end - start;
              double tmp = 0;

              // le temps ecoule depuis start
              if (endOffset != 0) {
                tmp = (passed / endOffset);
              }
              // le plus proche de la fin
              if ((nearest == 0 || (end - now) < nearest) && (end - now) > 0) {
                nearest = end - now;
                shortestOffset = tmp;
              } else if ((end - now) < 0) {
                WidgetsBinding.instance.addPostFrameCallback((_) => setState(() {
                  Provider.of<ActivityProvider>(context, listen: false).remove(activity.id);
                }));
              }       
            }
          }
        }

        if (activity != null) {
          calculateOffset();
        }

        return (shortestOffset < 1 ? shortestOffset : 1).toDouble();
    }

    /// Get activity [activity]'s data to be displayed and format them in a list
    List<Widget> getActivityDisplayData(Activity activity) {
      List<Container> constraints = [];
      activity.constraints.forEach((e) {
        if (e.type == "timer") {
          constraints.add(Container(
            height: 70,
            child: Column(children: [
              Wrap(
                alignment: WrapAlignment.start,
                direction: Axis.horizontal,
                children: [
                  Container(
                    child: IconButton(
                        icon: Icon(IconData(e.icon, fontFamily: 'MaterialIcons'), size: 20, color: DynamicTheme.of(context).data.accentColor),
                        onPressed: () {}),
                  ),
                  Container(
                    child: IconButton(
                      icon: Icon(IconData(59499, fontFamily: 'MaterialIcons'), size: 20, color: DynamicTheme.of(context).data.primaryColorLight),
                      onPressed: () {
                        pushOrShow(
                            context,
                            MessagePreview(
                                isModel: false,
                                action: AppLocalizations.of(context).translate('dashboard-update-current-activity'),
                                activity: activity,
                                model: null,
                                constraint: e));
                      },
                      key: (widget.index == _activity.length - 1) ? new Key('FinalDashboardConstraintMessagePreview') : Key("DashboardConstraintMessagePreview" + widget.index.toString()),
                    ),
                  ),
                ],
              ),
              Wrap(
                alignment: WrapAlignment.start,
                direction: Axis.horizontal,
                children: [
                  Text(
                    " " + getDateDifference(context, e.startDate, e.endDate),
                    key: (widget.index == _activity.length - 1) ? new Key('FinalDashboardConstraintTime') : Key("DashboardConstraintTime" + widget.index.toString()),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 15.0,
                      color: DynamicTheme.of(context).data.accentColor,
                    ),
                  ),
                ],
              )
            ]),
          ));
        }
      });

      setState(() {
        itemNb = constraints.length.toDouble();
      });

      List<Widget> widgets = [
        Text(
          activity.title,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontSize: 18.0,
            color: DynamicTheme.of(context).data.primaryColorLight,
          ),
        ),
        Padding(
          key: (widget.index == _activity.length - 1)
              ? new Key('FinalDashboardListTileContacts')
              : Key("DashboardListTileContacts" + widget.index.toString()),
          padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(children: [
            Icon(IconData(62399, fontFamily: 'MaterialIcons'), size: 18, color: DynamicTheme.of(context).data.accentColor),
            Text(
              " " + getActivityContacts(activity.contacts),
              style: TextStyle(
                fontSize: 15.0,
                color: DynamicTheme.of(context).data.primaryColorLight,
              ),
            ),
          ]),
        ),
        selected ? Wrap(runSpacing: 5.0, direction: Axis.vertical, children: constraints) : Container()
      ];
      return widgets;
    }

    return _activity == null
        ? Loading()
        : Center(
            child: widget.index >= _activity.length ? Container() : Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 320,
                  maxWidth: 600,
                ),
                child: Slidable(
                  key: (widget.index == _activity.length - 1)
                      ? new Key('FinalDashboardSlidableTile')
                      : Key("DashboardSlidableTile" + widget.index.toString()),
                  actionPane: SlidableScrollActionPane(),
                  actionExtentRatio: 0.25,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        selected = !selected;
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 0, 7),
                        decoration: BoxDecoration(
                          color: DynamicTheme.of(context).data.primaryColorDark,
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        // ListTile(
                        key: (widget.index == _activity.length - 1)
                            ? Key("FinalDashboardListTile")
                            : Key("DashboardListTile" + widget.index.toString()),
                        child: Row(
                          children: [
                            // leading:
                            Container(
                              key: (widget.index == _activity.length - 1)
                                  ? new Key('FinalDashboardListTileLeading')
                                  : Key("DashboardListTileLeading" + widget.index.toString()),
                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Stack(children: <Widget>[
                                Container(
                                  width: 45,
                                  height: 45,
                                  child: CircularProgressIndicator(
                                      backgroundColor: DynamicTheme.of(context).data.backgroundColor,
                                      valueColor: AlwaysStoppedAnimation<Color>(DynamicTheme.of(context).data.accentColor),
                                      strokeWidth: 9.0,
                                      value: getActivityIconValue(_activity[widget.index])
                                    ),
                                ),
                                Container(
                                  width: 45,
                                  height: 45,
                                  child: Center(
                                    child: CircleAvatar(
                                      backgroundColor: DynamicTheme.of(context).data.backgroundColor,
                                      child: Icon(IconData(_activity[widget.index].icon, fontFamily: 'MaterialIcons'),
                                          color: DynamicTheme.of(context).data.accentColor, size: 30),
                                    ),
                                  ),
                                ),
                              ]),
                            ),
                            // title:
                            Expanded(
                              child: Container(
                              key: (widget.index == _activity.length - 1)
                                  ? new Key('FinalDashboardListTileTitle')
                                  : Key("DashboardListTileTitle" + widget.index.toString()),
                              padding: EdgeInsets.fromLTRB(20, 10, 0, 10),
                              child: Wrap(
                                children: getActivityDisplayData(_activity[widget.index]),
                              ),
                            )),
                            // trailing:
                            DashboardPopupMenuButton(activity: _activity[widget.index], index: (widget.index == _activity.length - 1)),
                          ],
                        )),
                  ),
                  actions: [
                    IconSlideAction(
                      key: (widget.index == _activity.length - 1)
                          ? new Key('FinalDashboardTileNotify')
                          : Key("DashboardTileNotify" + widget.index.toString()),
                      caption: AppLocalizations.of(context).translate('dashboard-send-notif-activity'),
                      color: DynamicTheme.of(context).data.accentColor,
                      iconWidget: Icon(IconData(62287, fontFamily: 'MaterialIcons'), color: DynamicTheme.of(context).data.primaryColorDark, size: 25),
                      onTap: () async {
                        final result = await showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return ActivityAlertDialog(
                              question: AppLocalizations.of(context).translate('dashboard-send-notif-question'),
                              confirmation: AppLocalizations.of(context).translate('dashboard-send-notif-confirmation')
                            );
                          }
                        );
                        if (result) {
                          final response = await _activity[widget.index].alertActionActivity("alert", ms.api);
                          if (response.statusCode == 200) {
                            messageSnack = AppLocalizations.of(context).translate('dashboard-send-notif-confirmed');
                            _activity[widget.index].endDate = DateTime.now().toUtc().add(DateTime.now().timeZoneOffset).toIso8601String();
                            Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
                          } else {
                            if (response.statusCode == 400) {
                              messageSnack = AppLocalizations.of(context).translate('error-occured');
                            } else if (response.statusCode == 500) {
                              Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text(
                                  AppLocalizations.of(context).translate('server-error'),
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      color: DynamicTheme.of(context).data.primaryColorLight,
                                    ),
                                  ),
                                  backgroundColor: DynamicTheme.of(context).data.errorColor,
                                )
                              );
                            }
                          }
                        }
                        if (messageSnack != null && messageSnack != "") {
                          showDurationDialog(context, messageSnack, 1);
                        }
                      },
                    )
                  ],
                  secondaryActions: [
                    IconSlideAction(
                      key: (widget.index == _activity.length - 1)
                          ? new Key('FinalDashboardTileEnd')
                          : Key("DashboardTileEnd" + widget.index.toString()),
                      caption: AppLocalizations.of(context).translate('dashboard-terminate-activity'),
                      color: DynamicTheme.of(context).data.accentColor,
                      iconWidget: Icon(IconData(59594, fontFamily: 'MaterialIcons'), color: DynamicTheme.of(context).data.primaryColorDark, size: 25),
                      onTap: () async {
                        final result = await showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return ActivityAlertDialog(
                              question: AppLocalizations.of(context).translate('dashboard-end-alert-question'),
                              confirmation: AppLocalizations.of(context).translate('dashboard-terminate-activity'),
                            );
                          });
                        if (result) {
                          final response = await _activity[widget.index].alertActionActivity("finish", ms.api);
                          if (response.statusCode == 200) {
                            await ms.cancelNotification();
                            _activity[widget.index].endDate = DateTime.now().toUtc().add(DateTime.now().timeZoneOffset).toIso8601String();
                            Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
                            messageSnack = AppLocalizations.of(context).translate('dashboard-alert-ended');
                          } else {
                            if (response.statusCode == 400) {
                              messageSnack = AppLocalizations.of(context).translate('error-occured');
                            } else if (response.statusCode == 500) {
                              Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text(
                                  AppLocalizations.of(context).translate('server-error'),
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      color: DynamicTheme.of(context).data.primaryColorLight,
                                    ),
                                  ),
                                  backgroundColor: DynamicTheme.of(context).data.errorColor,
                                )
                              );
                            }
                          }
                          if (messageSnack != null && messageSnack != "") {
                            showDurationDialog(context, messageSnack, 1);
                          }
                        }
                      },
                    )
                  ],
                ),
              ),
            ),
          );
  }
}
