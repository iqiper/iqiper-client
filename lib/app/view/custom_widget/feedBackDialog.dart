import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';

/// Show a dialog that will last [duration] seconds and display [text]
void showDurationDialog(BuildContext context, String text, int duration) {
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (context) {
      Future.delayed(Duration(seconds: duration), () {
        Navigator.of(context).pop(true);
      });
      return AlertDialog(
        key: Key('DurationDialog'),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(32.0))),
        backgroundColor: DynamicTheme.of(context).data.backgroundColor,
        title: Icon(IconData(62793, fontFamily: 'MaterialIcons'), size: 30),
        content: Wrap(
          children: [Center(child: Text(
            text,
            style: TextStyle(color: Colors.white, fontSize: 16),
            textAlign: TextAlign.center
          ))],
        ),
      );
    }
  );
}
