import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';

class FullScreenLoading extends StatelessWidget {
  final ImageProvider logo = AssetImage("assets/img/logo.png");

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        // adapt Text widget if parent widget size is <= 300
        if (constraints.maxHeight <= 300)
          return Container(
              color: DynamicTheme.of(context).data.primaryColorDark,
              child: Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.spaceEvenly, 
                runSpacing: 1.0, 
                children: [
                  Column(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Center(
                        child: Image(image: logo, width: 50.0, height: 50.0,),
                              ),
                            ),
                          ]),
                  Column(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                      child: Text(
                        "iqiper",
                        style: TextStyle(
                          fontSize: 50,
                          fontWeight: FontWeight.bold,
                          color: DynamicTheme.of(context).data.accentColor,
                        ),
                      ),
                    ),
                    ),
                  ]),
                  Column(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: CircularProgressIndicator(
                            backgroundColor: DynamicTheme.of(context).data.backgroundColor,
                            valueColor: AlwaysStoppedAnimation<Color>(DynamicTheme.of(context).data.accentColor),
                          ),
                        ),
                      )
                    ]),
                ]),
              );
        else
          return Container(
          color: DynamicTheme.of(context).data.primaryColorDark,
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Center(
                  child: Image(image: logo, width: 100.0, height: 100.0,),
                  ),
              ),
              Container(
                height: 75,
                child: Center(
                    child: Text(
                  "iqiper",
                  style: TextStyle(
                    fontSize: 50,
                    fontWeight: FontWeight.bold,
                    color: DynamicTheme.of(context).data.accentColor,
                  ),
                )),
              ),
              Expanded(
                flex: 1,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: DynamicTheme.of(context).data.backgroundColor,
                    valueColor: AlwaysStoppedAnimation<Color>(DynamicTheme.of(context).data.accentColor),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
        child: Center(
          child: CircularProgressIndicator(
            backgroundColor: DynamicTheme.of(context).data.backgroundColor,
            valueColor: AlwaysStoppedAnimation<Color>(DynamicTheme.of(context).data.accentColor),
          ),
        ),
    )]);
  }
}

//class LoadingForm extends StatelessWidget {
//  final String text;
//  LoadingForm({this.text});
//
//  @override
//  Widget build(BuildContext context) {
//    return Expanded(
//      child: Center(
//          child: Column(
//        children: [
//          Text(
//              text,
//              style: TextStyle(color: Colors.white, fontSize: 16),
//              textAlign: TextAlign.center
//          ),
//          CircularProgressIndicator(
//            backgroundColor: DynamicTheme.of(context).data.backgroundColor,
//            valueColor: AlwaysStoppedAnimation<Color>(DynamicTheme.of(context).data.accentColor),
////          value
//          ),
//        ],
//      )),
//    );
//  }
//}
