import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Model.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view_model/custom_widget/activitiesPopupMenuButton.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:iqiperclient/app/view/custom_widget/messagePreview.dart';
import 'loading.dart';

class ActivityListItem extends StatefulWidget {
  final int index;
  ActivityListItem({this.index});

  @override
  ActivityListItemState createState() => ActivityListItemState();
}

class ActivityListItemState extends State<ActivityListItem> {
  bool selected = false;
  double itemNb = 0;

  /// This is a model item that is displayed in the activities tab
  @override
  Widget build(BuildContext context) {
    List<Model> _models = Provider.of<ModelProvider>(context).list;

    /// Get model [activity]'s data to be displayed and format them in a list
    List<Widget> getModelActivityDisplayData(Model activity) {
      List<Widget> constraints = [];

      int count = 0;
      activity.constraints.forEach((e) => {
            if (e.type == "timer")
              {
                constraints.add(
                  ConstrainedBox(
                    key: (widget.index == _models.length - 1)
                      ? Key(count.toString() + 'FinalActivityModelConstraintTime')
                      : Key(count.toString() + "ActivityModelListTileConstraint" + widget.index.toString()),
                    constraints: BoxConstraints(
                      minHeight: 50,
                      maxHeight: 100,
                      maxWidth: 70,
                    ),
                    child: Row(
                      children: [
                        Container(
                          child: Icon(IconData(e.icon, fontFamily: 'MaterialIcons'), size: 15, color: DynamicTheme.of(context).data.accentColor),
                        ),
                        Container(
                          child: IconButton(
                            icon:
                                Icon(IconData(59499, fontFamily: 'MaterialIcons'), size: 20, color: DynamicTheme.of(context).data.primaryColorLight),
                            onPressed: () {
                              pushOrShow(
                                  context,
                                  MessagePreview(
                                      action: AppLocalizations.of(context).translate('activity-model-update'),
                                      isModel: true,
                                      model: activity,
                                      activity: null,
                                      constraint: e));
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              },
              count++
          });

      setState(() {
        itemNb = constraints.length.toDouble();
      });

      List<Widget> widgets = [
        Text(
          activity.title,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontSize: 18.0,
            color: DynamicTheme.of(context).data.primaryColorLight,
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 3),
          child: Wrap(children: [
            Row(children: [
              Icon(IconData(62399, fontFamily: 'MaterialIcons'), size: 18, color: DynamicTheme.of(context).data.accentColor),
              Text(
                " " + getActivityContacts(activity.contacts),
                key: (widget.index == _models.length - 1)
                  ? Key('FinalActivityModelListTileContacts')
                  : Key("ActivityModelListTileContacts" + widget.index.toString()),
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 15.0,
                  color: DynamicTheme.of(context).data.primaryColorLight,
                ),
              ),
            ]),
          ]),
        ),
        selected
        ? Wrap(
          spacing: 5.0,
          direction: Axis.horizontal,
          children: constraints)
        : Container()
      ];
      return widgets;
    }

    return _models == null
        ? Loading()
        : widget.index >= _models.length ? Container() : Center(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 320,
                  maxWidth: 600,
                ),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      selected = !selected;
                    });
                  },
                  child: Container(
                    key: (widget.index == _models.length - 1)
                      ? Key("FinalActivityModelListTile")
                      : Key("ActivityModelListTile" + widget.index.toString()),
                    padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                    decoration: BoxDecoration(
                      color: DynamicTheme.of(context).data.primaryColorDark,
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                    child: Row(
                    children: [
                      // leading:
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Container(
                          width: 65,
                          height: 65,
                          child: Center(
                            child: Icon(IconData(_models[widget.index].icon, fontFamily: 'MaterialIcons'),
                                color: DynamicTheme.of(context).data.accentColor, size: 45),
                          ),
                        ),
                      ),
                      // title:
                      Expanded(
                        child: Container(
                          key: (widget.index == _models.length - 1)
                            ? Key("FinalActivityModelListTileTitle")
                            : Key("ActivityModelListTileTitle" + widget.index.toString()),
                          padding: EdgeInsets.fromLTRB(20, 10, 0, 10),
                          child: Wrap(
                            children: getModelActivityDisplayData(_models[widget.index]),
                          ),
                        ),
                      ),
                      // trailing:
                      ActivitiesPopupMenuButton(activity: _models[widget.index], index: (widget.index == _models.length - 1)),
                    ]),
                  ),
                ),
              ),
            ),
          );
  }
}
