import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/form/add_contact.dart';
import 'package:iqiperclient/app/view/form/modify_contact.dart';
import 'package:provider/provider.dart';

class ContactFormOpener extends StatefulWidget {
  final bool isPost;
  final String action;
  final Contact contact;
  ContactFormOpener({this.contact, this.action, this.isPost}): super(key: new Key("ContactsForm"));

  @override
  ContactFormOpenerState createState() => ContactFormOpenerState();
}

class ContactFormOpenerState extends State<ContactFormOpener> {
  bool isLoading = false;

  Widget _smallScreen (BuildContext context) {
    return Scaffold(
        backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
        appBar: AppBar(
            leading: IconButton(
              key: Key('ContactsFormBack'),
              icon: Icon(IconData(58791, fontFamily: 'MaterialIcons'), size: 35, color: DynamicTheme.of(context).data.backgroundColor),
              onPressed: () {
                if (!isLoading) {
                  Navigator.pop(context, "");
                }
              },
            ),
            title: Text(
              widget.action,
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: DynamicTheme.of(context).data.primaryColor,
              ),
            ),
            backgroundColor: DynamicTheme.of(context).data.primaryColorDark
        ),
        body: Center(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: 320,
              maxWidth: 380,
            ),
            child: Wrap(
              direction: Axis.vertical,
              alignment: WrapAlignment.start,
              crossAxisAlignment: WrapCrossAlignment.start,
                    children: <Widget>[
                      widget.isPost
                      ? AddContact()
                      : UpdateContact(contact: widget.contact)
                    ],
                  ),
                ),
            ),
    );
  }

  Widget _largeScreen (BuildContext context) {
    return SimpleDialog(
      backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
      contentPadding: EdgeInsets.all(20),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      children: <Widget>[
        Container(
          width: 400,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 25),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        key: Key('ContactsFormBack'),
                        icon: Icon(IconData(58791, fontFamily: 'MaterialIcons'), size: 35, color: DynamicTheme.of(context).data.backgroundColor),
                        onPressed: () {
                          if (!isLoading) {
                            Navigator.pop(context, "");
                          }
                        },
                      ),
                      Container(
                        child: Text(
                          widget.action,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: DynamicTheme.of(context).data.primaryColor,
                          ),
                        ),
                      ),
                    ]),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: EdgeInsets.only(bottom: 0.0),
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  border: Border(
                    bottom: BorderSide(
                      width: 2, 
                      color: DynamicTheme.of(context).data.primaryColorLight)
                      ),
                ),
              ),
              Center(
                child: Container(
                  color: DynamicTheme.of(context).data.primaryColorDark,
                  width: MediaQuery.of(context).size.width * .9,
                  height: MediaQuery.of(context).size.height * .8,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child:
                      widget.isPost
                      ? AddContact()
                      : UpdateContact(contact: widget.contact)
                      )
                  ])
                )
              )
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _smallScreen(context);
  }
}
