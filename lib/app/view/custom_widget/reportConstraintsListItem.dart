import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Communication.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/view/custom_widget/ReportConstraintResume.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/MainState.dart';
// import 'package:iqiperclient/app/model/CommunicationLog.dart';
// import 'package:iqiperclient/app/model/Contact.dart';

class ReportConstraintsListItem extends StatefulWidget {
  final Activity activity;
  final Constraint constraint;
  ReportConstraintsListItem({this.activity, this.constraint});
  @override
  ReportConstraintsListItemState createState() => ReportConstraintsListItemState();
}

class ReportConstraintsListItemState extends State<ReportConstraintsListItem> {

  /// This is an constraint button item that is displayed in the report resume
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);

    return Center(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 320,
                  maxWidth: 600,
                ),
                child: GestureDetector(
                    onTap: () async {
                      List<Communication> comList = new List<Communication>();
                      var responseConstraintCommunications = await ms.api.get(
                        '/activity/' + widget.activity.id + '/constraint/' + widget.constraint.id + '/communication',
                        false, ms.api.config.maxNumberOfRequestAttempt);
                        //   comList.add(new Communication.full(
                        //   logs: [
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //   ],
                        //   type: 'email',
                        //   reason: 'alert',
                        //   createdAt: '1970-01-01 00:00:00.000Z',
                        //   linkedConstraint: new Constraint.fromForm(
                        //     type: 'timer',
                        //     message: 'sjbfjasgjhsagjhakghdaklghkladhgklahdkghkladhgkldhgklhadklghkadlhgkdhglkhadlkghadklghkhadklgh',
                        //   ),
                        //   linkedActivity: new Activity(
                        //     constraints: [new Constraint.fromForm(
                        //       type: 'timer',
                        //       message: 'the messagethe messagethe messagethe messagethe messagethe messagethe messagethe message',
                        //     )],
                        //     message: 'the messagethe messagethe messagethe messagethe messagethe messagethe messagethe message',
                        //   ),
                        // ));
                        // comList.add(new Communication.full(
                        //   logs: [
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //     new CommunicationLog(
                        //       states: 'alert',
                        //       createdAt: '1970-01-01 00:00:00.000Z',
                        //       linkedContact: new Contact(
                        //         nickname: 'alex',
                        //       ),
                        //     ),
                        //   ],
                        //   type: 'email',
                        //   reason: 'alert',
                        //   createdAt: '1970-01-01 00:00:00.000Z',
                        //   linkedConstraint: new Constraint.fromForm(
                        //     type: 'timer',
                        //     message: 'sjbfjasgjhsagjhakghdaklghkladhgklahdkghkladhgkldhgklhadklghkadlhgkdhglkhadlkghadklghkhadklgh',
                        //   ),
                        //   linkedActivity: new Activity(
                        //     constraints: [new Constraint.fromForm(
                        //       type: 'timer',
                        //       message: 'the messagethe messagethe messagethe messagethe messagethe messagethe messagethe message',
                        //     )],
                        //     message: 'the messagethe messagethe messagethe messagethe messagethe messagethe messagethe message',
                        //   ),
                        // ));
                      if (responseConstraintCommunications.statusCode == 200) {
                        for (var e in json.decode(responseConstraintCommunications.body)) {
                          Communication tmpCom = communicationFromJson(e, widget.activity, widget.constraint, ms.api);
                          await tmpCom.fetchLogs(ms.api, context);
                          comList.add(tmpCom);
                        }
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ReportConstraintResume(constraint: widget.constraint, constraintCommunications: comList)
                          )
                        );
                      } else {
                        if (responseConstraintCommunications.statusCode == 400) {
                          String messageError = AppLocalizations.of(context).translate('error-occured');
                          showDurationDialog(context, messageError, 1);
                        } else if (responseConstraintCommunications.statusCode == 500) {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                              style: TextStyle(
                                  fontSize: 18.0,
                                  color: DynamicTheme.of(context).data.primaryColorLight,
                                ),
                              ),
                              backgroundColor: DynamicTheme.of(context).data.errorColor,
                            )
                          );
                        }
                      }
                    },
                    child: Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                        decoration: BoxDecoration(
                          color: DynamicTheme.of(context).data.primaryColorDark,
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        child: Row(
                          children: [
                            // title:
                            Expanded(
                              child: Container(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Wrap(
                                children: [
                                  Center(
                                    child:Text(widget.constraint.type.toString(),
                                  style: TextStyle(
                                    color: DynamicTheme.of(context).data.primaryColorLight,
                              ),
                                  ))
                                ],
                              ),
                            )),
                          ],
                        )),
                  )
              ),
            ),
          );
  }
}
