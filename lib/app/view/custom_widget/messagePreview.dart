import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/Model.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/model/FormValues.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:iqiperclient/app/view/custom_widget/activityFormOpener.dart';

class MessagePreview extends StatelessWidget {
  final bool isModel;
  final String action;
  final Constraint constraint;
  final Activity activity;
  final Model model;

  MessagePreview({this.isModel, this.action, this.activity, this.constraint, this.model});

  Widget _smallScreen(BuildContext context) {
    return Scaffold(
        key: Key('MessagePreviewWindow'),
        appBar: AppBar(
            leading: IconButton(
              key: Key('MessagePreviewBack'),
              icon: Icon(IconData(58791, fontFamily: 'MaterialIcons'), size: 25, color: DynamicTheme.of(context).data.primaryColorLight),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              AppLocalizations.of(context).translate('message-preview'),
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: DynamicTheme.of(context).data.primaryColor,
              ),
            ),
            backgroundColor: DynamicTheme.of(context).data.primaryColorDark),
        body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width * .9,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      key: Key('MessagePreviewEdit'),
                      icon: Icon(IconData(58878, fontFamily: 'MaterialIcons'), size: 25, color: DynamicTheme.of(context).data.accentColor),
                      onPressed: () {
                        Navigator.pop(context);
                        pushOrShow(
                          context,
                          ActivityFormOpener(
                            isPost: false,
                            isModel: isModel,
                            action: action,
                            formValues: FormValues(
                              step: 2,
                              title: isModel ? model.title : activity.title,
                              message: isModel ? model.message : activity.message,
                              icon: isModel ? model.icon : activity.icon,
                              id: isModel ? model.id : activity.id,
                              constraintError: false,
                              dropdownValue: 'timer',
                              constraints: isModel ? model.constraints : activity.constraints,
                              contactError: false,
                              contactSelectionState: contactForContactSelection(Provider.of<ContactProvider>(context, listen: false).list, isModel ? model.contacts : activity.contacts),
                              contacts: contactForContactSelection(isModel ? model.contacts : activity.contacts, isModel ? model.contacts : activity.contacts),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                  padding: EdgeInsets.only(bottom: 0.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight)),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Text(
                        constraint.message,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          fontSize: 20.0,
                          color: DynamicTheme.of(context).data.accentColor,
                        ),
                      )),
                ),
              ],
            ),
          ),
        ));
  }

  Widget _largeScreen(BuildContext context) {
    return SimpleDialog(
      key: Key('MessagePreviewWindow'),
      backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
      contentPadding: EdgeInsets.all(20),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      children: <Widget>[
        Container(
          width: 400,
          height: MediaQuery.of(context).size.height * .7,
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
          child: Column(
            children: <Widget>[
              Container(
                height: 20,
                margin: EdgeInsets.fromLTRB(0, 0, 0, 25),
                child: Text(
                  AppLocalizations.of(context).translate('message-preview'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: DynamicTheme.of(context).data.primaryColor,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    key: Key('MessagePreviewBack'),
                    icon: Icon(IconData(58791, fontFamily: 'MaterialIcons'), size: 25, color: DynamicTheme.of(context).data.primaryColorLight),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  IconButton(
                    key: Key('MessagePreviewEdit'),
                    icon: Icon(IconData(58878, fontFamily: 'MaterialIcons'), size: 25, color: DynamicTheme.of(context).data.accentColor),
                    onPressed: () {
                      Navigator.pop(context);
                      pushOrShow(
                        context,
                        ActivityFormOpener(
                        isPost: false,
                          isModel: isModel,
                          action: action,
                          formValues: FormValues(
                            step: 2,
                            title: isModel ? model.title : activity.title,
                            message: isModel ? model.message : activity.message,
                            icon: isModel ? model.icon : activity.icon,
                            id: isModel ? model.id : activity.id,
                            constraintError: false,
                            dropdownValue: 'timer',
                            constraints: isModel ? model.constraints : activity.constraints,
                            contactError: false,
                            contactSelectionState: contactForContactSelection(Provider.of<ContactProvider>(context, listen: false).list, isModel ? model.contacts : activity.contacts),
                            contacts: contactForContactSelection(isModel ? model.contacts : activity.contacts, isModel ? model.contacts : activity.contacts),
                          ),
                        )
                      );
                    },
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: EdgeInsets.only(bottom: 0.0),
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  border: Border(bottom: BorderSide(width: 2, color: DynamicTheme.of(context).data.primaryColorLight)),
                ),
              ),
              Expanded(
                flex: 1,
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Text(
                      constraint.message,
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                        fontSize: 15.0,
                        color: DynamicTheme.of(context).data.accentColor,
                      ),
                    )),
              ),
            ],
          ),
        ),
      ],
    );
  }

  /// This widget displays a preview of the message from constraint [constraint]
  /// A button is used to quickly edit this activity or model
  @override
  Widget build(BuildContext context) {
    return Provider.of<MainState>(context, listen: false).whichScale(context)
      ? _smallScreen(context)
      : _largeScreen(context);
  }
}
