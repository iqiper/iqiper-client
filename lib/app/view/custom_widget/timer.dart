import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:iqiperclient/app/view_model/utils/validator.dart';

class Timer extends StatefulWidget {
  final Constraint constraint;
  final bool isModel;
  final bool isPost;
  final int index;
  final bool test;
  Timer({this.constraint, this.isModel, this.index, this.test, this.isPost});

  @override
  _TimerState createState() => _TimerState();
}

class _TimerState extends State<Timer> {
  // timer
  bool timerError = false;
  String timerErrorTxt = "";
  // alert msg
  bool _msgError = false;
  String _msgErrorText;
  
  Duration zoneOffset = DateTime.now().timeZoneOffset;

  DateTime startDate;
  DateTime endDate;

  Duration startOffset;
  Duration endOffset;

  Duration modelOffset;

  String tmpMessage;
  TextEditingController _input = TextEditingController();

  Duration endOffsetToServer(){
    return endOffset + startOffset;
  }
  Duration modelOffsetToServer() {
    return endOffset - modelOffset + startOffset;
  }

  @override
  void dispose() {
    _input.dispose();
    super.dispose();
  }

  updateChildWithParent() { 
    // model - data {"start_offset", "end_offset", "notify_offset"} /
    // activity - data {"start_date", "end_date", "notify_date"} /
    if (widget.isModel) {
      startOffset = new Duration(seconds: widget.constraint.startOffset == null ? 0 : widget.constraint.startOffset);
      endOffset = new Duration(seconds: widget.constraint.endOffset == null ? 3600 : widget.constraint.endOffset);
      if (!widget.isPost) {
        endOffset = endOffset - startOffset; // aka end Offset To Client
      }
    } else {
      startDate = widget.constraint.startOffset == null
        ? DateTime.parse(widget.constraint.startDate)
        : DateTime.now().add(new Duration(seconds: widget.constraint.startOffset == null
                                          ? 0
                                          : widget.constraint.startOffset.toInt()));
      endDate = widget.constraint.endOffset == null
        ? DateTime.parse(widget.constraint.endDate)
        : DateTime.now().add(new Duration(seconds: widget.constraint.endOffset == null
                                          ? 0
                                          : widget.constraint.endOffset.toInt()));
    }

    if (widget.constraint.timerOffsetCheck == null && widget.constraint.dateOffset == null) {
      modelOffset = new Duration(seconds: 3300);
      // modelOffset = new Duration(seconds: -300);
    } else {
      modelOffset = widget.constraint.dateOffset == null 
                      ? new Duration(seconds: widget.constraint.endOffset - widget.constraint.timerOffsetCheck) // aka model Offset To Client
                      : new Duration(seconds: DateTime.parse(widget.constraint.endDate).difference(DateTime.parse(widget.constraint.dateOffset)).inSeconds);
    }

    if (widget.isModel) {
      setOffsetData(widget.constraint);
    } else {
      setTimerData(widget.constraint);
    }

  }

  @override
  void initState() {
    super.initState();

    updateChildWithParent();
  }

  @override
  void didUpdateWidget(Timer oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isModel) {
      if (oldWidget.constraint.startOffset != widget.constraint.startOffset || 
        oldWidget.constraint.endOffset != widget.constraint.endOffset ||
        oldWidget.constraint.dateOffset != widget.constraint.dateOffset) {
        updateChildWithParent();
      }
    } else {
      if (oldWidget.constraint.startDate != widget.constraint.startDate || 
        oldWidget.constraint.endDate != widget.constraint.endDate ||
        oldWidget.constraint.dateOffset != widget.constraint.dateOffset) {
        updateChildWithParent();
      }
    }
  }

  /// This widget is the form use to setup a timer constraint
  /// With a start, an end, a notify offset and an alert_message
  @override
  Widget build(BuildContext context) {
    tmpMessage = widget.constraint.message == "" 
              ? AppLocalizations.of(context).translate('activity-default-message') 
              : widget.constraint.message;
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(0, 0.0, 0, 5.0),
      padding: EdgeInsets.fromLTRB(0, 0.0, 0, 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          widget.isModel
            ? Center(
              child: Text(
                AppLocalizations.of(context).translate('activityform-model-setup-timer'),
                style: TextStyle(fontSize: 15.0, color: DynamicTheme.of(context).data.primaryColorLight),
                textAlign: TextAlign.center,
              ))
            :  Container(),
          Container(
            width: double.infinity,
            padding: EdgeInsets.fromLTRB(0, 10.0, 0, 10.0),
            child: timerError
              ? Text(
                  timerError ? timerErrorTxt : "",
                  style: TextStyle(fontSize: 10.0, color: DynamicTheme.of(context).data.primaryColorLight),
                  textAlign: TextAlign.center,
                )
              : Container(),
          ),
          widget.isModel
              ? Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                      Container(
                        height: 35,
                        width: 90,
                        child: RaisedButton(
                          key: widget.test ? Key("FinalTimerStartButton") : Key("TimerStartButton" + widget.index.toString()),
                          elevation: 5,
                          padding: EdgeInsets.only(top: 1),
                          color: DynamicTheme.of(context).data.primaryColorDark,
                          onPressed: () {
                            DatePicker.showPicker(context,
                                pickerModel: CustomPicker(),
                                onConfirm: (date) {
                                  Duration diff = secondRounder(DateTime.now().difference(date).abs());
                                  startOffset = diff;
                                  setOffsetData(widget.constraint);
                                },
                              locale: AppLocalizations.of(context).locale.toString() == 'en_US' ? LocaleType.en : LocaleType.fr,
                            );
                          },
                          child: Text(
                            AppLocalizations.of(context).translate('start-offset'),
                            textAlign: TextAlign.center,
                            style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Center(
                            child: Text(
                              displayCountdown(context, startOffset, false),
                              key: widget.test ? Key("FinalTimerStartText") : Key("TimerStartText" + widget.index.toString()),
                              textAlign: TextAlign.center,
                              style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                            )
                        ),
                      ),
                    ]),
                  )
              : Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                    Container(
                      height: 35,
                      width: 90,
                      child: RaisedButton(
                        key: widget.test ? Key("FinalTimerStartButton") : Key("TimerStartButto + widget.index.toString()n" + widget.index.toString()),
                        elevation: 5,
                        padding: EdgeInsets.only(top: 1),
                        color: DynamicTheme.of(context).data.primaryColorDark,
                        onPressed: () {
                          return DatePicker.showDateTimePicker(
                            context,
                            showTitleActions: true,
                            minTime: DateTime.now(),
                            maxTime: DateTime(2222, 1, 1, 00, 00),
                            currentTime: startDate.toUtc().add(zoneOffset),
                            onConfirm: (date) {
                              startDate = date.toUtc().subtract(zoneOffset);
                              setTimerData(widget.constraint);
                            },
                            locale: AppLocalizations.of(context).locale.toString() == 'en_US' ? LocaleType.en : LocaleType.fr,
                          );
                        },
                        child: Text(
                          AppLocalizations.of(context).translate('start'),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Center(
                          child: Text(
                            (startDate == null) ? "" : startDate.toUtc().add(zoneOffset).toString().substring(0, 16),
                            key: widget.test ? Key("FinalTimerStartText") : Key("TimerStartText" + widget.index.toString()),
                            textAlign: TextAlign.center,
                            style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                          )
                      ),
                    ),
                  ]),
                ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
              Container(
                height: 35,
                width: 90,
                child: RaisedButton(
                  key: widget.test ? Key("FinalTimerNotifButton") : Key("TimerNotifButton" + widget.index.toString()),
                  elevation: 5,
                  padding: EdgeInsets.only(top: 1),
                  color: DynamicTheme.of(context).data.primaryColorDark,
                  onPressed: () {
                    DatePicker.showPicker(context,
                        pickerModel: CustomPicker(),
                        onConfirm: (date) {
                          Duration diff = secondRounder(DateTime.now().difference(date).abs());
                          modelOffset = diff;
                          if (widget.isModel) {
                            setOffsetData(widget.constraint);
                          } else {
                            setTimerData(widget.constraint);
                          }
                        },
                      locale: AppLocalizations.of(context).locale.toString() == 'en_US' ? LocaleType.en : LocaleType.fr,
                    );
                  },
                  child: Text(
                    AppLocalizations.of(context).translate('recall'),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                  ),
                ),
              ),
              Expanded(
                child: Center(
                    child: Text(
                      displayCountdown(context, modelOffset, false),
                      key: widget.test ? Key("FinalTimerNotifText") : Key("TimerNotifText" + widget.index.toString()),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                    )
                ),
              )
            ]),
          ),
          widget.isModel
              ? Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                      Container(
                        height: 35,
                        width: 90,
                        child: RaisedButton(
                          key: widget.test ? Key("FinalTimerEndButton") : Key("TimerEndButton" + widget.index.toString()),
                          elevation: 5,
                          padding: EdgeInsets.only(top: 1),
                          color: DynamicTheme.of(context).data.primaryColorDark,
                          onPressed: () {
                            DatePicker.showPicker(context,
                                pickerModel: CustomPicker(),
                                onConfirm: (date) {
                                  Duration diff = secondRounder(DateTime.now().difference(date).abs());
                                  endOffset = diff;
                                  setOffsetData(widget.constraint);
                                },
                              locale: AppLocalizations.of(context).locale.toString() == 'en_US' ? LocaleType.en : LocaleType.fr,
                            );
                          },
                          child: Text(
                            AppLocalizations.of(context).translate('end-offset'),
                            textAlign: TextAlign.center,
                            style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                          ),
                        ),
                      ),
                      Expanded(
                          child: Center(
                              child: Text(
                                displayCountdown(context, endOffset, false),
                                key: widget.test ? Key("FinalTimerEndText") : Key("TimerEndText" + widget.index.toString()),
                                textAlign: TextAlign.center,
                                style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                              )
                          ),
                      ),
                    ]),
                  )
              : Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                    Container(
                      height: 35,
                      width: 90,
                      child: RaisedButton(
                        key: widget.test ? Key("FinalTimerEndButton") : Key("TimerEndButton" + widget.index.toString()),
                        elevation: 5,
                        padding: EdgeInsets.only(top: 1),
                        color: DynamicTheme.of(context).data.primaryColorDark,
                        onPressed: () {
                          return DatePicker.showDateTimePicker(
                            context,
                            showTitleActions: true,
                            minTime: DateTime.now(),
                            maxTime: DateTime(2222, 1, 1, 00, 00),
                            currentTime: endDate.toUtc().add(zoneOffset),
                            onConfirm: (date) {
                              endDate = date.toUtc().subtract(zoneOffset);
                              setTimerData(widget.constraint);
                            },
                            locale: AppLocalizations.of(context).locale.toString() == 'en_US' ? LocaleType.en : LocaleType.fr,
                          );
                        },
                        child: Text(
                          AppLocalizations.of(context).translate('end'),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                        ),
                      ),
                    ),
                    Expanded(
                        child: Center(
                            child: Text(
                              (endDate == null) ? "" : endDate.toUtc().add(zoneOffset).toString().substring(0, 16),
                              key: widget.test ? Key("FinalTimerEndText") : Key("TimerEndText" + widget.index.toString()),
                              textAlign: TextAlign.center,
                              style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                            )
                        ),
                    ),
                  ]),
                ),
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
              Container(
                height: 35,
                width: 90,
                child: RaisedButton(
                  elevation: 5,
                  padding: EdgeInsets.only(top: 1),
                  color: DynamicTheme.of(context).data.primaryColorDark,
                  onPressed: () {
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) {
                        _input.text = tmpMessage;
                        return StatefulBuilder(
                          builder: (context, setState) {
                        return SimpleDialog(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30.0))),
                          contentPadding: EdgeInsets.all(10.0),
                          backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
                          children: <Widget>[
                            Container(
                              key: Key('TimerMsgDialog'),
                              width: MediaQuery.of(context).size.height * .8,
                              height: MediaQuery.of(context).size.height * .8,
                              decoration:
                                  BoxDecoration(
                                    shape: BoxShape.rectangle, 
                                    borderRadius: BorderRadius.all(Radius.circular(25.0))
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[                                                      
                                      Container(
                                        width: MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.fromLTRB(0, 0, 0, 25),
                                        child: 
                                          Wrap(
                                          direction: Axis.horizontal,
                                          alignment: WrapAlignment.spaceBetween,
                                            children: [
                                              IconButton(
                                                icon: Icon(IconData(58791, fontFamily: 'MaterialIcons'), size: 35, color: DynamicTheme.of(context).data.backgroundColor),
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                              ),
                                              Container(
                                                height: 35,
                                                margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                                child: Text(
                                                  AppLocalizations.of(context).translate('edit-message-dialog'),
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 20.0,
                                                    color: DynamicTheme.of(context).data.primaryColor,
                                                  ),
                                                ),
                                              ),
                                            ]),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: SingleChildScrollView(
                                              child: Wrap(
                                              children: [
                                                Center(
                                                  child: TextField(
                                                    keyboardType: TextInputType.multiline,
                                                    maxLines: null,
                                                    controller: _input,                                                    
                                                    onChanged: (value) {
                                                      tmpMessage = removeDiacritics(value);
                                                    },
                                                    cursorColor: DynamicTheme.of(context).data.primaryColorLight,
                                                    style: TextStyle(
                                                      color: DynamicTheme.of(context).data.accentColor, fontSize: 18
                                                    ),
                                                    decoration: InputDecoration(
                                                      isDense: true,
                                                      contentPadding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
                                                      filled: true,
                                                      fillColor: DynamicTheme.of(context).data.primaryColorDark,
                                                      labelStyle: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                                                      enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                                      ),
                                                      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight)),
                                                      focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                                      ),
                                                      border: OutlineInputBorder(
                                                        borderSide: BorderSide(color: DynamicTheme.of(context).data.primaryColorLight),
                                                      ),
                                                      hintStyle: TextStyle(color: DynamicTheme.of(context).data.primaryColorLight),
                                                      errorText: _msgError ? _msgErrorText : null,
                                                      errorStyle: TextStyle(color: DynamicTheme.of(context).data.primaryColorLight),
                                                      errorBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: DynamicTheme.of(context).data.accentColor),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                                  child: Center(
                                                  child: RaisedButton(
                                                    key: Key('ValidActivityMessageForm'),
                                                    padding: EdgeInsets.only(top: 1),
                                                    child: Text(
                                                      AppLocalizations.of(context).translate('save'),
                                                      style: TextStyle(
                                                        color: DynamicTheme.of(context).data.primaryColor,
                                                      ),
                                                    ),
                                                    color: DynamicTheme.of(context).data.primaryColorLight,
                                                    onPressed: () async {
                                                      tmpMessage = removeDiacritics(_input.text == "" 
                                                                    ? AppLocalizations.of(context).translate('activity-default-message') 
                                                                    : _input.text);
                                                      if (tmpMessage.isAlertMessageValid() || tmpMessage == "") {
                                                        setState(() {
                                                          widget.constraint.message = tmpMessage;
                                                          _msgError = false;
                                                          _msgErrorText = "";
                                                        });
                                                        _input.text = "";
                                                        Navigator.pop(context);
                                                      } else {
                                                        setState(() {
                                                          _msgErrorText = AppLocalizations.of(context).translate('msg-alert-error');
                                                          _msgError = true;
                                                        });
                                                      }
                                                    },
                                                  ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                        );
                        });
                      }
                    );
                  },
                  child: Text(
                    AppLocalizations.of(context).translate('edit-message'),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                  ),
                ),
              ),
              Expanded(
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                    child: Text(
                      widget.constraint.message.toString(),
                      textAlign: TextAlign.left,
                      style: TextStyle(color: DynamicTheme.of(context).data.accentColor),
                    )
                  ),
                ),
              )
            ]),
          ),
        ],
      ),
    );
  }

  bool checkOffsetRecall() {
    if (startOffset != null && endOffset != null && modelOffset != null) {
      // offset is equal to zero
      if (modelOffset.inSeconds == 0) {
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-is-zero');
        });
        return false;
        // offset is before startoffset
      } else if (startOffset != null && (modelOffset.inSeconds + startOffset.inSeconds) <= startOffset.inSeconds) {
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-recall-to-high');
        });
        return false;
        // offset is after or equal to endOffset
      } else if (modelOffset.inSeconds >= endOffset.inSeconds) {
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-recall-to-high');
        });
        return false;
      } 
    }
    setState(() {
      timerError = false;
      timerErrorTxt = "";
    });
    return true;
  }


  /// Check if offset values are correct
  /// [end] true if needs to check the end offset
  /// [recall] true if needs to check the recall offset
  bool checkOffset() {
    // if its a startoffset check it shouldnt be null || if its a endoffset check neither || nor recall
    if (startOffset == null || endOffset == null || modelOffset == null) {
      setState(() {
        timerError = true;
        timerErrorTxt = AppLocalizations.of(context).translate('offset-incorrect-format');
      });
    } else {
      // then it s ok
      setState(() {
        timerError = false;
        timerErrorTxt = "";
      });
      // but not if it s equal zero
      if (endOffset.inSeconds == 0) {
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-is-zero');
        });
      } else if ((startOffset == null && endOffset.inSeconds > 0)) {
        // startoffset should be set
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-incorrect-format');
        });
      } else if (startOffset.isNegative || endOffset.isNegative || modelOffset.isNegative) {
        // values must be positives
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-incorrect-value');
        });
      }
    }
    return !timerError;
  }
  
  bool checkTimerRecall() {
    if (startDate != null && endDate != null && modelOffset != null) {
      if (modelOffset.inMinutes == 0) {
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-is-zero');
        });
      } else if (DateTime.parse(startDate.toString()).add(modelOffset).isAfter(DateTime.parse(endDate.toString()))) {
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-recall-to-high');
        });
        return false;
      } else if (!DateTime.parse(endDate.toString()).subtract(modelOffset).isAfter(DateTime.parse(startDate.toString()))) {
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('offset-recall-to-high');
        });
        return false;
      } else {
        setState(() {
          timerError = false;
          timerErrorTxt = "";
        });
        return true;
      }
    }
    return false;
  }

  /// check if [startDate] and [endDate] are valid
  bool checkTimer(DateTime startDate, DateTime endDate) {
    if (startDate != null && endDate != null) {
      // to check startdate and enddate, values can t be null
      if (DateTime.parse(startDate.toString()) == null && DateTime.parse(endDate.toString()) == null) {
        setState(() {
          timerError = true;
          timerErrorTxt = AppLocalizations.of(context).translate('timer-incorrect-format');
        });
      } else {
        // calculate offset
        Duration offset = endDate.difference(startDate);
        // startdate can t be after enddate
        if (offset.isNegative) {
          setState(() {
            timerError = true;
            timerErrorTxt = AppLocalizations.of(context).translate('timer-start-after-end');
          });
        } else {
          // startdate and enddate must be differents
          if (offset.inSeconds == 0) {
            setState(() {
              timerError = true;
              timerErrorTxt = AppLocalizations.of(context).translate('timer-start-different-end');
            });
          } else {
            // it s ok
            setState(() {
              timerError = false;
              timerErrorTxt = "";
            });
            return true;
          }
        }
      }
    } else {
      // si start date et pas end date on laisse une chance
      setState(() {
        timerError = true;
        timerErrorTxt = "";
      });
    }
    return false;
  }

  setOffsetData(Constraint c) {
    setState(() {
      if (checkOffset() && checkOffsetRecall()) { 
          c.setOffset(startOffset.inSeconds, endOffsetToServer().inSeconds, modelOffsetToServer().inSeconds);
          widget.constraint.isValid = true;
      } else {
        widget.constraint.isValid = false;
      }
    });
  }

  setTimerData(Constraint c) {
    setState(() {
      if (checkTimer(startDate, endDate) && checkTimerRecall()) {
        widget.constraint.isValid = true;
        c.setDate(startDate.toUtc().toIso8601String(), endDate.toUtc().toIso8601String(), endDate.toUtc().subtract(modelOffset).toIso8601String());
      } else {
        widget.constraint.isValid = false;
      }
    });
  }
}

class CustomPicker extends CommonPickerModel {

  /// Return [value] digit of length [length] into a String
  String digits(int value, int length) {
    return '$value'.padLeft(length, "0");
  }

  CustomPicker({DateTime currentTime, LocaleType locale}) : super(locale: locale) {
    this.currentTime = currentTime ?? DateTime(0);
    this.setLeftIndex(this.currentTime.hour);
    this.setMiddleIndex(this.currentTime.minute);
    this.setRightIndex(this.currentTime.second);
  }

  /// Check if day [index] is correct
  /// Return digit value of length 2
  @override
  String leftStringAtIndex(int index) {
    if (index >= 0 && index < 365) {
      return this.digits(index, 2);
    } else {
      return null;
    }
  }

  /// Check if hour [index] is correct
  /// Return digit value of length 2
  @override
  String middleStringAtIndex(int index) {
    if (index >= 0 && index < 24) {
      return this.digits(index, 2);
    } else {
      return null;
    }
  }

  /// Check if minute [index] is correct
  /// Return digit value of length 2
  @override
  String rightStringAtIndex(int index) {
    if (index >= 0 && index < 60) {
      return this.digits(index, 2);
    } else {
      return null;
    }
  }

  @override
  String leftDivider() {
    return 'd';
  }

  @override
  String rightDivider() {
    return "h";
  }

  @override
  List<int> layoutProportions() {
    return [1, 1, 1];
  }

  /// Take this timer's days, hours and minutes
  /// And return a Datetime with these values
  @override
  DateTime finalTime() {
    Duration days = new Duration(days: this.currentLeftIndex().toInt());
    Duration hours = new Duration(hours: this.currentMiddleIndex().toInt());
    Duration minutes = new Duration(minutes: this.currentRightIndex().toInt());
    return DateTime.now().add(Duration(seconds: days.inSeconds + hours.inSeconds + minutes.inSeconds));
  }
}

