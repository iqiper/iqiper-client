import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Communication.dart';
import 'package:iqiperclient/app/view/custom_widget/communicationListItem.dart';
import 'package:iqiperclient/app/view/custom_widget/reportConstraintsListItem.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';

class ReportResume extends StatelessWidget {
  final Activity report;
  final List<Communication> activityCommunications;

  ReportResume({this.report, this.activityCommunications}) : super(key: new Key('ReportResumeWidget'));

  @override
  Widget build(BuildContext context) {
    Widget _communicationListItem(BuildContext context, int index) {
      return CommunicationListItem(communication: activityCommunications[index]);
    }

    Widget _reportConstraintsListItem(BuildContext context, int index) {
      return ReportConstraintsListItem(activity: report, constraint: report.constraints[index]);
    }

    return Scaffold(
        key: Key('CommunicationPreviewWindow'),
        backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
        appBar: AppBar(
            leading: IconButton(
              key: Key('CommunicationResumeBack'),
              icon: Icon(IconData(58791, fontFamily: 'MaterialIcons'),
                  size: 35,
                  color: DynamicTheme.of(context).data.backgroundColor),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              AppLocalizations.of(context).translate('reports'),
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: DynamicTheme.of(context).data.primaryColor,
              ),
            ),
        ),
        body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width * .9,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: SingleChildScrollView(
                    child: Container(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Wrap(direction: Axis.vertical, children: [
                          ConstrainedBox(
                            constraints: BoxConstraints(
                              minWidth: 320,
                              maxWidth: 600,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                color: DynamicTheme.of(context)
                                    .data
                                    .backgroundColor,
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                              child: Wrap(
                                direction: Axis.vertical,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      alignment: WrapAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: 65,
                                          height: 65,
                                          child: Center(
                                            key: Key('ReportResumeActivityIcon'),
                                            child: Icon(
                                              IconData(
                                                report.icon == null ? 58947 :
                                                report.icon, fontFamily: 'MaterialIcons'
                                              ),
                                              color: DynamicTheme.of(context).data.accentColor, size: 45
                                            ),
                                          ),
                                        ),
                                        Container(
                                          key: Key('ReportResumeActivityTitle'),
                                          padding: EdgeInsets.fromLTRB(10, 20, 0, 0),
                                          child:
                                            Text(
                                              report.title,
                                              textAlign: TextAlign.left,
                                              maxLines: 30,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 18.0,
                                                color: DynamicTheme.of(context).data.primaryColorLight,
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      alignment: WrapAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)
                                                  .translate('contacts') +
                                              " : ",
                                          key: Key('ReportResumeActivityContacts'),
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            color: DynamicTheme.of(context)
                                                .data
                                                .primaryColorLight,
                                          ),
                                        ),
                                        Text(
                                          getActivityContacts(report.contacts),
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            color: DynamicTheme.of(context)
                                                .data
                                                .accentColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      alignment: WrapAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)
                                                  .translate('constraints') +
                                              " : ",
                                          key: Key('ReportResumeActivityContacts'),
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            color: DynamicTheme.of(context)
                                                .data
                                                .primaryColorLight,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                      height: MediaQuery.of(context).size.height * .3,
                                      width: MediaQuery.of(context).size.width * 0.9,
                                      child: Center(
                                      child: ListView.builder(
                                        key: Key('CommunicationLogsListView'),
                                        padding: EdgeInsets.symmetric(vertical: 5.0),
                                        itemCount: report.constraints.length,
                                        itemBuilder: _reportConstraintsListItem
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Text('Communications:',
                            style: TextStyle(
                                fontSize: 18.0,
                                color: DynamicTheme.of(context).data.accentColor
                              ),
                            ),
                          ),
                          Container (
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            height: MediaQuery.of(context).size.height * .7,
                            width: MediaQuery.of(context).size.width * 0.9,
                              child: ListView.builder(
                                key: Key('CommunicationListView'),
                                padding: EdgeInsets.symmetric(vertical: 5.0),
                                itemCount: activityCommunications.length,
                                itemBuilder: _communicationListItem
                              ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }
}
