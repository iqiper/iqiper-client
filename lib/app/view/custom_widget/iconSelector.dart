import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/ActivityIcon.dart';

class IconSelector extends StatelessWidget {
  final Function changeIcon;
  IconSelector(this.changeIcon);

  /// This widget displays a list of icons that can be selected for activities or models
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(32.0))),
        backgroundColor: DynamicTheme.of(context).data.backgroundColor,
        title: Text(AppLocalizations.of(context).translate('activity-choose-icon'), style: TextStyle(color: Colors.white)),
        content: Container(
          width: 320.0,
          child: GridView.count(
            crossAxisCount: 4,
            children: List.generate(listOfIcons.length, (index) {
              return Center(
                child: IconButton(
                  icon: Icon(IconData(listOfIcons[index].iconId, fontFamily: 'MaterialIcons'), color: Colors.white),
                  onPressed: () {
                    Navigator.of(context).pop();
                    changeIcon(listOfIcons[index].iconId);
                  },
                ),
              );
            }),
          ),
        )
    );
  }
}