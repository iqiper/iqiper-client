import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/Contact.dart';

class FavoriteContactSelectionDialog extends StatefulWidget {
  FavoriteContactSelectionDialog();
  @override
  FavoriteContactSelectionDialogState createState() => FavoriteContactSelectionDialogState();
}

class FavoriteContactSelectionDialogState extends State<FavoriteContactSelectionDialog> {
  List<Contact> _contacts;

  @override
  void initState() {
    super.initState();
  }

  /// This widget displays a list of contact to be selected if user try to send an alert with no favorite contacts
  @override
  Widget build(BuildContext context) {
    // MainState ms = Provider.of<MainState>(context, listen: false);
    _contacts = Provider.of<ContactProvider>(context, listen: false).list;
    return SimpleDialog(
        key: Key('AddContactFavoriteDialog'),
        backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
        contentPadding: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        children: <Widget>[
          Wrap(children: [
            Container(
              key: Key('ActivityAddContactDialog'),
              width: MediaQuery.of(context).size.height * .8,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle, 
                  borderRadius: BorderRadius.all(Radius.circular(25.0))
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 25),
                      child: 
                        Wrap(
                        direction: Axis.horizontal,
                        alignment: WrapAlignment.spaceBetween,
                          children: [
                            IconButton(
                              icon:
                                  Icon(IconData(58791, fontFamily: 'MaterialIcons'), size: 35, color: DynamicTheme.of(context).data.backgroundColor),
                              onPressed: () {
                                  Navigator.pop(context);
                              },
                            ),
                            Container(
                              height: 35,
                              margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                              child: Text(
                                AppLocalizations.of(context).translate('dashboard-choose-contact-alert'),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: DynamicTheme.of(context).data.primaryColor,
                                ),
                              ),
                            ),
                          ]),
                        ),
                        Expanded(
                          flex: 1,
                            child: Wrap(children: [
                              Column(
                              children: <Widget>[
                                ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  key: Key('AddContactFavoriteDialogListView'),
                                  padding: EdgeInsets.all(5.0),
                                  shrinkWrap: true,
                                  itemCount: _contacts.length,
                                  itemBuilder: (context, index) {
                                    return Center(
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                          minWidth: 320,
                                          minHeight: 80,
                                          maxWidth: 400,
                                          maxHeight: 100,
                                        ),
                                        child: Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: MediaQuery.of(context).size.height,
                                          margin: EdgeInsets.symmetric(vertical: 5),
                                          decoration: BoxDecoration(
                                              color: DynamicTheme.of(context).data.primaryColorLight,
                                              shape: BoxShape.rectangle,
                                              borderRadius: BorderRadius.all(Radius.circular(25.0))),
                                          child: 
                                            ListTile(
                                            key: (index == _contacts.length)
                                                ? Key("FinalFavoriteContactSelectionListTile")
                                                : Key("FavoriteContactSelectionListTile" + index.toString()),
                                            leading: Icon(
                                              IconData(62399, fontFamily: 'MaterialIcons'),
                                              size: 45,
                                              color: DynamicTheme.of(context).data.primaryColor,
                                            ),
                                            title: Text(
                                              _contacts[index].nickname,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 18.0,
                                                color: DynamicTheme.of(context).data.primaryColor,
                                              ),
                                            ),
                                            subtitle: Text(
                                              _contacts[index].subtitle,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: 12.0,
                                                color: DynamicTheme.of(context).data.primaryColor,
                                              ),
                                            ),
                                            onTap: () async {
                                              Navigator.pop(context, _contacts[index]);
                                            },
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ]
                        )
                      ),
                    ],
                  )
                )
              ]
            )
          ]
        );
  }
}
