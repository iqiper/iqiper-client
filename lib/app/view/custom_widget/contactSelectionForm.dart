import 'package:flutter/material.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Contact.dart';

class ContactSelection extends StatefulWidget {
  final List<Contact> contactSelectionState;

  ContactSelection({this.contactSelectionState});

  @override
  _ContactSelectionState createState() => _ContactSelectionState();
}

class _ContactSelectionState extends State<ContactSelection> {
  
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      backgroundColor: DynamicTheme.of(context).data.primaryColorDark,
      contentPadding: EdgeInsets.all(10.0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      children: <Widget>[
            Container(
              key: Key('ActivityAddContactDialog'),
              width: MediaQuery.of(context).size.height * .8,
              height: MediaQuery.of(context).size.height,
              decoration:
                  BoxDecoration(
                    shape: BoxShape.rectangle, 
                    borderRadius: BorderRadius.all(Radius.circular(25.0))
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 25),
                        child: 
                          Wrap(
                          direction: Axis.horizontal,
                          alignment: WrapAlignment.spaceBetween,
                            children: [
                              IconButton(
                                icon: Icon(
                                  IconData(58791, fontFamily: 'MaterialIcons'), 
                                  size: 35, 
                                  color: DynamicTheme.of(context).data.backgroundColor),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                Container(
                                  height: 35,
                                  margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                  child: Text(
                                    AppLocalizations.of(context).translate('activity-select-more-contacts'),
                                    key: Key('ActivityAddContactDialogListView'),
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: DynamicTheme.of(context).data.primaryColor,
                                    ),
                                  )
                                ),
                              ]),
                            ),
                            Expanded( 
                              flex: 1,
                              child: Wrap(
                                children: [
                                  Column(
                                  children: <Widget>[
                                    ListView.builder(
                                      physics: NeverScrollableScrollPhysics(),
                                      key: Key('AddContactDialogListView'),
                                      scrollDirection: Axis.vertical,
                                      padding: EdgeInsets.all(5.0),
                                      shrinkWrap: true,
                                      itemCount: widget.contactSelectionState.length,
                                      itemBuilder: (context, index) {
                                      return Center(
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                            minWidth: 320,
                                            minHeight: 80,
                                            maxWidth: 400,
                                            maxHeight: 100,
                                          ),
                                          child: Container(
                                            width: MediaQuery.of(context).size.width,
                                            height: MediaQuery.of(context).size.height,
                                            margin: EdgeInsets.symmetric(vertical: 5),
                                            decoration: BoxDecoration(
                                              color: widget.contactSelectionState[index].isSelected
                                                ? DynamicTheme.of(context).data.primaryColor
                                                : DynamicTheme.of(context).data.primaryColorLight,
                                            shape: BoxShape.rectangle,
                                            borderRadius: BorderRadius.all(Radius.circular(25.0))),
                                            child: ListTile(
                                              key: (index + 1 == widget.contactSelectionState.length) ? Key("FinalActivityContactSelectionListTile") : Key("ActivityContactSelectionListTile" + index.toString()),
                                              leading: Padding(
                                                padding: EdgeInsets.fromLTRB(0, 7, 0, 0),
                                                  child: Icon(
                                                  IconData(62399, fontFamily: 'MaterialIcons'),
                                                  size: 45,
                                                  color: widget.contactSelectionState[index].isSelected
                                                    ? DynamicTheme.of(context).data.primaryColorLight
                                                    : DynamicTheme.of(context).data.primaryColor,
                                              ),
                                            ),
                                            title: Padding(
                                              padding: widget.contactSelectionState[index].subtitle == "" 
                                              ? EdgeInsets.fromLTRB(0, 30, 0, 0)
                                              :  EdgeInsets.fromLTRB(0, 20, 0, 0),
                                              child: Text(
                                                widget.contactSelectionState[index].nickname,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontSize: 18.0,
                                                  color: widget.contactSelectionState[index].isSelected
                                                      ? DynamicTheme.of(context).data.primaryColorLight
                                                      : DynamicTheme.of(context).data.primaryColor,
                                                ),
                                              ),
                                            ),
                                            subtitle: Padding(
                                              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                              child: Text(
                                                widget.contactSelectionState[index].subtitle,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  fontSize: 12.0,
                                                  color: widget.contactSelectionState[index].isSelected
                                                      ? DynamicTheme.of(context).data.primaryColorLight
                                                      : DynamicTheme.of(context).data.primaryColor,
                                                ),
                                              ),
                                            ),
                                            trailing: Padding(
                                              padding: EdgeInsets.fromLTRB(0, 7, 0, 0),
                                              child: 
                                              // IconButton(
                                              // icon: 
                                              widget.contactSelectionState[index].isSelected
                                                ? Icon(IconData(58957, fontFamily: 'MaterialIcons'),
                                                    size: 45, color: DynamicTheme.of(context).data.accentColor)
                                                : Icon(IconData(58958, fontFamily: 'MaterialIcons'),
                                                    size: 45, color: DynamicTheme.of(context).data.primaryColor),
                                            ),
                                            onTap: () async {
                                              Navigator.pop(context, widget.contactSelectionState[index]);
                                            },
                                          // ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ],
                          )]
                      ),
                    ),
                  ],
              )),
      ]);
  }
}