import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iqiperclient/app/model/Communication.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/view/custom_widget/communicationLogsListItem.dart';

class CommunicationListItem extends StatefulWidget {
  final Communication communication;
  CommunicationListItem({this.communication});

  @override
  CommunicationListItemState createState() => CommunicationListItemState();
}

class CommunicationListItemState extends State<CommunicationListItem> {
  /// This is a communication item that is displayed in the report tab
  @override
  Widget build(BuildContext context) {
    Widget _communicationLogsListItem(BuildContext context, int index) {
      return CommunicationLogsListItem(log: widget.communication.logs[index]);
    }

    return Center(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 320,
                  maxWidth: 600,
                ),
                child: Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                    decoration: BoxDecoration(
                      color: DynamicTheme.of(context).data.primaryColor,
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                    child: Row(
                      children: [
                        // title:
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 0, 10),
                            child: Wrap(
                              direction: Axis.vertical,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Type: ' + widget.communication.type.toString(),
                                    style: TextStyle(
                                      color: DynamicTheme.of(context).data.primaryColorLight,
                                    )),
                                    Text('Reason: ' + widget.communication.reason.toString(),
                                    style: TextStyle(
                                      color: DynamicTheme.of(context).data.primaryColorLight,
                                    )),
                                    widget.communication.linkedConstraint == null ? Container() :
                                      Container(
                                        width: 250,
                                        child: Text('Message: ' + widget.communication.linkedConstraint.message.toString(),
                                          style: TextStyle(
                                            color: DynamicTheme.of(context).data.primaryColorLight,
                                          )),
                                      ),
                                    Text('Created_at: ' + DateFormat('dd/MM/yyyy HH:mm').format(DateTime.parse(widget.communication.createdAt).toUtc().add(DateTime.now().timeZoneOffset)),
                                    style: TextStyle(
                                      color: DynamicTheme.of(context).data.primaryColorLight,
                                    )),
                                    widget.communication.logs.length > 0 ? 
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Logs:',
                                        style: TextStyle(
                                          color: DynamicTheme.of(context).data.primaryColorLight,
                                        )),
                                        Container (
                                          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                          height: MediaQuery.of(context).size.height * 0.35,
                                          width: MediaQuery.of(context).size.width * 0.9,
                                          child: ListView.builder(
                                            key: Key('CommunicationLogsListView'),
                                            padding: EdgeInsets.symmetric(vertical: 2.0),
                                            itemCount: widget.communication.logs.length,
                                            itemBuilder: _communicationLogsListItem
                                          ),
                                        ),
                                      ],
                                    ) : Container()
                                  ]
                                )
                              ],
                            ),
                          ),
                        ),
                      ]
                    ),
                  ),
              ),
            ),
          );
  }
}
