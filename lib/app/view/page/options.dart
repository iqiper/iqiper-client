import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:iqiperclient/app/view/custom_widget/activityAlertDialog.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:provider/provider.dart';
import 'package:flag/flag.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/AppTheme.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:iqiperclient/app/view/custom_widget/themePreview.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class MyBrowser extends ChromeSafariBrowser {
  MyBrowser();

  @override
  void onOpened() {
    close();
  }
}

class Options extends StatefulWidget {
  Options() : super(key: new Key('OptionsWidget'));

  @override
  OptionsListState createState() => OptionsListState();
}

class OptionsListState extends State<Options> {

  /// This widget display all options
  /// User can change language and theme
  /// User can log out
  @override
  Widget build(BuildContext context) {
    return Consumer<MainState>(builder: (context, ms, _) {
      return ms == null
        ? FullScreenLoading()
        : Container(
        child: ListView(
          key: Key('ListOfOptions'),
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          children: <Widget>[
            RaisedButton(
                  elevation: 5,
                  padding: EdgeInsets.only(top: 1),
                  color: Color(0xFFBDD9BF),
                  onPressed: () async {
                    final ChromeSafariBrowser browser = new MyBrowser();
                    final configJson = json.decode(await rootBundle.loadString(
                      'assets/config/dev.json'
                    ));
                    await browser.open(url: configJson['keycloakSettingsUrl']);
                  },
                  child: Text(
                    AppLocalizations.of(context).translate('keycloak-settings'),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color(0xFF0D2134),
                  ),
                ),
            ),
            SizedBox(height: 5),
            RaisedButton(
                  elevation: 5,
                  padding: EdgeInsets.only(top: 1),
                  color: Color(0xFFBDD9BF),
                  onPressed: () async {
                    final result = await showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return ActivityAlertDialog(
                          question: AppLocalizations.of(context).translate('destroy-user-question'),
                          confirmation: AppLocalizations.of(context).translate('destroy-user-confirmation')
                        );
                      }
                    );
                    if (result) {
                      Navigator.pop(context);
                      Provider.of<MainState>(context, listen: false).cancelNotification();
                      await ms.api.destroyUser(context);
                    }
                  },
                  child: Text(
                    AppLocalizations.of(context).translate('destroy-user'),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color(0xFF0D2134),
                  ),
                ),
            ),
            SizedBox(height: 5),
            RaisedButton(
                  elevation: 5,
                  padding: EdgeInsets.only(top: 1),
                  color: Color(0xFFBDD9BF),
                  onPressed: () async {
                    Navigator.pop(context);
                    Provider.of<MainState>(context, listen: false).cancelNotification();
                    return await ms.api.logout(context);
                  },
                  child: Text(
                    AppLocalizations.of(context).translate('logout'),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Color(0xFF0D2134),
                  ),
                ),
            ),
            SizedBox(height: 50),
            Container(
                height: 100,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    
            Center(
                child: Container(
                  height: 50,
                  child: new Text(
                    AppLocalizations.of(context).translate('language'),
                    key: Key('LanguageOptionTitle'),
                    style: TextStyle(
                      fontWeight: FontWeight.bold, 
                      fontSize: 40,
                      color: Color(0xFFBDD9BF),
                    )
                  ),
                )
            ),
                  ]),),
            Container(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                        key: Key('LanguageOptionEng'),
                        onTap: () =>
                            setState(() {
                              ms.changeLanguage(Locale('en_US'));
                            }),
                        child: Stack(
                          children: [
                            Container(
                              height: 100,
                              width: 120,
                              decoration: new BoxDecoration(
                                  color: AppLocalizations
                                      .of(context)
                                      .locale
                                      .toString() == 'en_US' ? Color(0x55FFFFFF) : Colors.transparent,
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.all(Radius.circular(25.0))
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.only(left: 15, top: 10),
                                child: Stack(
                                  children: [
                                    Container(
                                      height: 60,
                                      width: 90,
                                      child: Flag('gb',
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    Container(
                                      height: 60,
                                      width: 90,
                                      decoration: new BoxDecoration(
                                        color: AppLocalizations
                                            .of(context)
                                            .locale
                                            .toString() == 'en_US' ? Colors.transparent : Color(0xAA000000),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 55),
                                      height: 90,
                                      width: 90,
                                      child: Center(child: Text(
                                        'English', 
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: Color(0xFFBDD9BF),
                                          ))),
                                    )
                                  ],
                                )
                            )
                          ],
                        )
                    ),
                    SizedBox(width: 10),
                    GestureDetector(
                        key: Key('LanguageOptionFr'),
                        onTap: () =>
                            setState(() {
                              ms.changeLanguage(Locale('fr_FR'));
                            }),
                        child: Stack(
                          children: [
                            Container(
                              height: 100,
                              width: 120,
                              decoration: new BoxDecoration(
                                  color: AppLocalizations
                                      .of(context)
                                      .locale
                                      .toString() == 'fr_FR' ? Color(0x55FFFFFF) : Colors.transparent,
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.all(Radius.circular(25.0))
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.only(left: 15, top: 10),
                                child: Stack(
                                  children: [
                                    Container(
                                      height: 60,
                                      width: 90,
                                      child: Flag('fr',
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    Container(
                                      height: 60,
                                      width: 90,
                                      decoration: new BoxDecoration(
                                        color: AppLocalizations
                                            .of(context)
                                            .locale
                                            .toString() == 'fr_FR' ? Colors.transparent : Color(0xAA000000),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 55),
                                      height: 90,
                                      width: 90,
                                      child: Center(child: Text('Français', style: TextStyle(fontSize: 20,
                                          color: Color(0xFFBDD9BF),))),
                                    )
                                  ],
                                )
                            )
                          ],
                        )
                    ),
                  ],
                )
            ),
            SizedBox(height: 50),
            Center(
                child: Container(
                  height: 50,
                  child: new Text(
                    AppLocalizations.of(context).translate('theme'),
                    key: Key('ThemeOptionTitle'),
                    style: TextStyle(
                      fontWeight: FontWeight.bold, 
                      fontSize: 40,
                      color: Color(0xFFBDD9BF),
                      )),
                )
            ),
            Container(
                height: 300,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ThemePreview(theme: AppTheme.standard),
                    SizedBox(width: 10),
                    ThemePreview(theme: AppTheme.greyScale),
                  ],
                )
            ),
          ],
        )
      );
    });
  }
}

var iqiperLanguages = <LanguageString>[
  LanguageString(langCode: 'en', langName: 'English', langMessage: "Language set to English"),
  LanguageString(langCode: 'fr', langName: 'Français', langMessage: "Langue configuré en Français"),
];

String getCodeFromName(String name) {
  String tmp = 'en';
  iqiperLanguages.forEach((e) {
    if (e.langName == name) {
      tmp = e.langCode;
    }
  });
  return tmp;
}

String getNameFromCode(String code) {
  String tmp = 'English';
  iqiperLanguages.forEach((e) {
    if (e.langCode == code) {
      tmp = e.langName;
    }
  });
  return tmp;
}

String getMessageFromName(String name) {
  String tmp = 'flushbar error - loading message';
  iqiperLanguages.forEach((e) {
    if (e.langName == name) {
      tmp = e.langMessage;
    }
  });
  return tmp;
}

String getMessageFromCode(String code) {
  String tmp = 'flushbar error - loading message';
  iqiperLanguages.forEach((e) {
    if (e.langCode == code) {
      tmp = e.langMessage;
    }
  });
  return tmp;
}

class LanguageString {
  String langCode;
  String langName;
  String langMessage;

  LanguageString({this.langCode, this.langName, this.langMessage});
}