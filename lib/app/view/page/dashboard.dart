import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/custom_widget/dashboardActivityListItem.dart';
import 'package:iqiperclient/app/view_model/custom_widget/alertTile.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class DashboardActivity {
  final String title;
  final IconData icon;
  final String start;
  final String end;

  const DashboardActivity({this.title, this.icon, this.start, this.end});
}

class Dashboard extends StatefulWidget {
  Dashboard() : super(key: Key('DashboardWidget'));
  @override
  DashboardState createState() => DashboardState();
}

class DashboardState extends State<Dashboard> {
  MainState ms;

  @override
  void initState() {
    super.initState();
    ms = Provider.of<MainState>(context, listen: false);
  }
  /// This widget display all activities and allow user to add or update any activities
  @override
  Widget build(BuildContext context) {
    Widget _dashboardActivityListItem(BuildContext context, int index) {
      return DashboardActivityListItem(index: index);
    }

    return Consumer<ActivityProvider>(builder: (context, activities, _) {
      RefreshController _refreshController = RefreshController(initialRefresh: false);
      return Provider.of<MainState>(context).globalLoading
          ? FullScreenLoading()
          : Scaffold(
            backgroundColor: DynamicTheme.of(context).data.backgroundColor,
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  children: <Widget>[
                    Container(
                      height: 100,
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: AlertTile()
                    ),
                  ],
                ),
                Expanded(
                  child: Center(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 320,
                        minHeight: MediaQuery.of(context).size.height,
                        maxWidth: 600,
                        maxHeight: MediaQuery.of(context).size.height,
                      ),
                      child: activities.list.length == 0 && activities != null
                        ? SmartRefresher(
                          controller: _refreshController,
                          enablePullDown: true,
                          enablePullUp: false,
                          onRefresh: () {
                            Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
                            _refreshController.refreshCompleted();
                          },
                          child: Center(
                            child: Text(
                              AppLocalizations.of(context).translate('no-activities'),
                              key: Key("DashboardNoListView"),
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 19.0,
                                color: DynamicTheme.of(context).data.accentColor,
                              ),
                            ),
                          )
                        )
                        : SmartRefresher(
                          controller: _refreshController,
                          enablePullDown: true,
                          enablePullUp: false,
                          onRefresh: () {
                            Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
                            _refreshController.refreshCompleted();
                          },
                          child: ListView.builder(
                            key: Key('DashboardListView'),
                            padding: EdgeInsets.symmetric(vertical: 8.0),
                            itemCount: activities.list.length,
                            itemBuilder: _dashboardActivityListItem)
                    )),
                  ),
                ),
              ],
            ),
          );
    });
  }
}
