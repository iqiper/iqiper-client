import 'package:flutter/material.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/custom_widget/contactFormOpener.dart';
import 'package:iqiperclient/app/view/custom_widget/contactListItem.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Contacts extends StatefulWidget {
  Contacts() : super(key: new Key('ContactsWidget'));
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contacts> {
  
  /// This widget display all constacts and allow user to add or update any contacts
  @override
  Widget build(BuildContext context) {

  return Consumer<ContactProvider>(builder: (context, contacts, _) {
    RefreshController _refreshController = RefreshController(initialRefresh: false);
    return Provider.of<MainState>(context).globalLoading
      ? FullScreenLoading()
      : Scaffold(
        backgroundColor: DynamicTheme.of(context).data.backgroundColor,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Center(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: 320,
                    minHeight: MediaQuery.of(context).size.height,
                    maxWidth: 600,
                    maxHeight: MediaQuery.of(context).size.height,
                  ),
                  child: contacts.list.length == 0 && contacts != null
                      ? SmartRefresher(
                      controller: _refreshController,
                      enablePullDown: true,
                      enablePullUp: false,
                      onRefresh: () {
                        Provider.of<ContactProvider>(context, listen: false).fetchAllContacts();
                        _refreshController.refreshCompleted();
                      },
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context).translate('no-contacts'),
                          key: Key("ContactsNoListView"),
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 19.0,
                            color: DynamicTheme.of(context).data.accentColor,
                          ),
                        ),
                      )
                    )
                  : SmartRefresher(
                      controller: _refreshController,
                      enablePullDown: true,
                      enablePullUp: false,
                      onRefresh: () {
                        Provider.of<ContactProvider>(context, listen: false).fetchAllContacts();
                        _refreshController.refreshCompleted();
                      },
                      child: ListView.builder(
                      key: Key('ContactsListView'),
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      itemCount: contacts.list.length,
                      itemBuilder: _contactListItem
                    ),
                )),
              ),
            ),
          ],
        ),
        floatingActionButton: Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Container(
              height: 80.0,
              width: 80.0,
              child: FittedBox(
                child: FloatingActionButton(
                  key: Key('AddContactsButton'),
                  onPressed: () async {
                    final result = await pushOrShow(
                      context,
                      ContactFormOpener(
                        isPost: true,
                        action: AppLocalizations.of(context).translate('contact-add'),
                      ),
                    );
                    if (result != null && result != "") {
                      if (result == AppLocalizations.of(context).translate('server-error')) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text("$result",
                            style: TextStyle(
                                fontSize: 18.0,
                                color: DynamicTheme.of(context).data.primaryColorLight,
                              ),
                            ),
                            backgroundColor: DynamicTheme.of(context).data.errorColor,
                          )
                        );
                      } else {
                        showDurationDialog(context, "$result", 1);
                      }
                    }
                  },
                  child: Icon(IconData(59294, fontFamily: 'MaterialIcons'), color: DynamicTheme.of(context).data.primaryColorDark),
                  backgroundColor: DynamicTheme.of(context).data.accentColor,
                ),
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget _contactListItem(BuildContext context, int index) {
    return ContactListItem(index: index);
  }
}
