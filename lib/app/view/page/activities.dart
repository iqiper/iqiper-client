import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/FormValues.dart';
import 'package:iqiperclient/app/view/custom_widget/activityListItem.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:iqiperclient/app/view/custom_widget/activityFormOpener.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Activities extends StatefulWidget {
  Activities() : super(key: new Key('ActivitiesWidget'));
  @override
  _ActivityState createState() => _ActivityState();
}

class _ActivityState extends State<Activities> {

  /// This widget display all models and allow user to add or update any models
  @override
  Widget build(BuildContext context) {
    Widget _activityListItem(BuildContext context, int index) {
      return ActivityListItem(index: index);
    }

    return Consumer<ModelProvider>(builder: (context, models, _) {
      RefreshController _refreshController = RefreshController(initialRefresh: false);
      return Provider.of<MainState>(context).globalLoading
        ? FullScreenLoading()
        : Scaffold(
          backgroundColor: DynamicTheme
              .of(context)
              .data
              .backgroundColor,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
            Expanded(
              child: Center(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: 320,
                    minHeight: MediaQuery.of(context).size.height,
                    maxWidth: 600,
                    maxHeight: MediaQuery.of(context).size.height,
                  ),
                  child: models.list.length == 0 && models != null
                  ? SmartRefresher(
                    controller: _refreshController,
                    enablePullDown: true,
                    enablePullUp: false,
                    onRefresh: () {
                      Provider.of<ModelProvider>(context, listen: false).fetchAllModels();
                      _refreshController.refreshCompleted();
                    },
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context).translate('no-models'),
                        key: Key("ActivitiesNoListView"),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 19.0,
                          color: DynamicTheme.of(context).data.accentColor,
                        ),
                      ),
                    )
                  )
                  : SmartRefresher(
                      controller: _refreshController,
                      enablePullDown: true,
                      enablePullUp: false,
                      onRefresh: () {
                        Provider.of<ModelProvider>(context, listen: false).fetchAllModels();
                        _refreshController.refreshCompleted();
                      },
                      child: ListView.builder(
                      key: Key('ActivityModelListView'),
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      itemCount: models.list.length,
                      itemBuilder: _activityListItem
                  )),
                ),
              ),
            ),
          ]),
          floatingActionButton: Align(
            key: Key('AddActivityModelButton'),
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Container(
                height: 80.0,
                width: 80.0,
                child: FittedBox(
                  child: SelectionButton()
                ),
              ),
            ),
          ),
        );
    });
  }
}

class SelectionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      key: Key("addActivityButton"),
      onPressed: () {
        //add_activity_model
        _navigateAndDisplaySelection(context);
      },
      child: Icon(IconData(0xe69c, fontFamily: 'MaterialIcons'), color: DynamicTheme.of(context).data.primaryColorDark),
      backgroundColor: DynamicTheme
          .of(context)
          .data
          .accentColor,
    );
  }

  /// A method that launches the SelectionScreen and awaits the result from
  /// Navigator.pop.
  _navigateAndDisplaySelection(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await pushOrShow(
      context,
      ActivityFormOpener(
        isPost: true,
        isModel: true,
        action: AppLocalizations.of(context).translate('activity-model-create'),
        formValues: FormValues(
          step: 0,
          title: AppLocalizations.of(context).translate('activity-model-create-title'),
          message: AppLocalizations.of(context).translate('activity-default-message'),
          icon: 58947,
          id: null,
          constraintError: false,
          dropdownValue: 'timer',
          constraints: new List<Constraint>(),
          contactError: false,
          contactSelectionState: contactForContactSelection(
              Provider.of<ContactProvider>(context, listen: false).list, 
              favoriteContactSelection(Provider.of<ContactProvider>(context, listen: false).list)
            ),
          contacts: favoriteContactSelection(Provider.of<ContactProvider>(context, listen: false).list),
        ),
      ),
    );
    if (result != null && result != "") {
      if (result == AppLocalizations.of(context).translate('server-error')) {
        Scaffold.of(context).showSnackBar(
          SnackBar(content: Text(result,
            style: TextStyle(
                fontSize: 18.0,
                color: DynamicTheme.of(context).data.primaryColorLight,
              ),
            ),
            backgroundColor: DynamicTheme.of(context).data.errorColor,
          )
        );
      } else {
        showDurationDialog(context, "$result", 1);
      }
    }
  }
}