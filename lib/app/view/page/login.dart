import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _scaffoldKeyLogin = GlobalKey<ScaffoldState>(); 
  ImageProvider logo = AssetImage("assets/img/logo.png");

  /// This widget is a simple login page that will redirect the user after login to the main app
  @override
  Widget build(BuildContext context) {
    MainState ms = Provider.of<MainState>(context, listen: false);

    return Scaffold(
      key: _scaffoldKeyLogin,
      backgroundColor: DynamicTheme.of(context).data.backgroundColor,
      body: LayoutBuilder(
      builder: (context, constraints) {
        // adapt Text widget if parent widget size is <= 300
        if (constraints.maxHeight <= 300)
          return Container(
              color: DynamicTheme.of(context).data.primaryColorDark,
              child: Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.spaceEvenly, 
                runSpacing: 1.0, 
                children: [
                  Column(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Center(
                        child: Image(image: logo, width: 50.0, height: 50.0,),
                              ),
                            ),
                          ]),
                  Column(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                      child: Text(
                        "iqiper",
                        style: TextStyle(
                          fontSize: 50,
                          fontWeight: FontWeight.bold,
                          color: DynamicTheme.of(context).data.accentColor,
                        ),
                      ),
                    ),
                    ),
                  ]),
                  Column(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: CircularProgressIndicator(
                            backgroundColor: DynamicTheme.of(context).data.backgroundColor,
                            valueColor: AlwaysStoppedAnimation<Color>(DynamicTheme.of(context).data.accentColor),
                          ),
                        ),
                      )
                    ]),
                    Visibility(
            visible: ms.api.isWorking,
            child: const CircularProgressIndicator(),
          ),
                ]),
              );
        else
          return Container(
          color: DynamicTheme.of(context).data.primaryColorDark,
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Center(
                  child: Image(image: logo, width: 100.0, height: 100.0,),
                  ),
              ),
              Container(
                height: 75,
                child: Center(
                    child: Text(
                  "iqiper",
                  style: TextStyle(
                    fontSize: 50,
                    fontWeight: FontWeight.bold,
                    color: DynamicTheme.of(context).data.accentColor,
                  ),
                )),
              ),
              Expanded(
                flex: 1,
                child: Center(
                  child: RaisedButton(
            elevation: 5,
            padding: EdgeInsets.all(5),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              alignment: WrapAlignment.center,
              direction: Axis.horizontal,
              children: [
                Text(
                'login', 
                style: TextStyle(
                  color: DynamicTheme.of(context).data.accentColor, 
                  fontSize: 18
                  )
                ),
                // Icon(IconData(59294, fontFamily: 'MaterialIcons'), color: DynamicTheme.of(context).data.accentColor),
              ]),
              color: DynamicTheme.of(context).data.primaryColor,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              onPressed: () async {
              Response response = await ms.api.login();
              if (response.statusCode == 200) {
                _scaffoldKeyLogin.currentState.showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context).translate('login-success'),
                    style: TextStyle(
                        fontSize: 18.0,
                        color: DynamicTheme.of(context).data.backgroundColor,
                      ),
                    ),
                    backgroundColor: DynamicTheme.of(context).data.primaryColorLight,
                  )
                );
              } else if (response.statusCode == 400) {
                _scaffoldKeyLogin.currentState.showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context).translate('login-failed'),
                    style: TextStyle(
                        fontSize: 18.0,
                        color: DynamicTheme.of(context).data.accentColor,
                      ),
                    ),
                    backgroundColor: DynamicTheme.of(context).data.errorColor,
                  )
                );
              } else {
                _scaffoldKeyLogin.currentState.showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context).translate('server-error'),
                    style: TextStyle(
                        fontSize: 18.0,
                        color: DynamicTheme.of(context).data.accentColor,
                      ),
                    ),
                    backgroundColor: DynamicTheme.of(context).data.errorColor,
                  )
                );
              }
            },
          ),
                ),
              ),
              Visibility(
            visible: ms.api.isWorking,
            child: const CircularProgressIndicator(),
          ),
            ],
          ),
        );
      },)
    );
      
  }
}
