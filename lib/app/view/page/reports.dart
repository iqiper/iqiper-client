import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/view/custom_widget/reportListItem.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/view/custom_widget/loading.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Reports extends StatefulWidget {
  Reports() : super(key: Key('ReportWidget'));
  @override
  ReportState createState() => ReportState();
}

class ReportState extends State<Reports> {
  MainState ms;

  @override
  void initState() {
    super.initState();
    ms = Provider.of<MainState>(context, listen: false);
  }
  /// This widget display all activities and allow user to add or update any activities
  @override
  Widget build(BuildContext context) {
    Widget _reportActivityListItem(BuildContext context, int index) {
      return ReportListItem(index: index);
    }

    return Consumer<ActivityProvider>(builder: (context, activities, _) {
      RefreshController _refreshController = RefreshController(initialRefresh: false);
      return Provider.of<MainState>(context).globalLoading
          ? FullScreenLoading()
          : Scaffold(
            backgroundColor: DynamicTheme.of(context).data.backgroundColor,
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Center(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minWidth: 320,
                        minHeight: MediaQuery.of(context).size.height,
                        maxWidth: 600,
                        maxHeight: MediaQuery.of(context).size.height,
                      ),
                      child: activities.reportList.length == 0 && activities != null
                        ? SmartRefresher(
                          controller: _refreshController,
                          enablePullDown: true,
                          enablePullUp: false,
                          onRefresh: () {
                            Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
                            _refreshController.refreshCompleted();
                          },
                          child: Center(
                            child: Text(
                              AppLocalizations.of(context).translate('no-reports'),
                              key: Key("ReportNoListView"),
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 19.0,
                                color: DynamicTheme.of(context).data.accentColor,
                              ),
                            ),
                          )
                        )
                        : SmartRefresher(
                          controller: _refreshController,
                          enablePullDown: true,
                          enablePullUp: false,
                          onRefresh: () {
                            Provider.of<ActivityProvider>(context, listen: false).fetchAllActivities();
                            _refreshController.refreshCompleted();
                          },
                          child: ListView.builder(
                            key: Key('ReportListView'),
                            padding: EdgeInsets.symmetric(vertical: 8.0),
                            itemCount: activities.reportList.length,
                            itemBuilder: _reportActivityListItem)
                    )),
                  ),
                ),
              ],
            ),
          );
    });
  }
}
