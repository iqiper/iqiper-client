import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Contact.dart';

/// Convert a HttpClientResponse into a String
Future<String> readResponse(HttpClientResponse response) {
  final completer = Completer<String>();
  final contents = StringBuffer();

  response.transform(utf8.decoder).listen((data) {
    contents.write(data);
  }, onDone: () => completer.complete(contents.toString()));
  return completer.future;
}

/// Depending if client is running in mobile or web
/// Either show a dialog box or use another route to show a widget [widget]
pushOrShow(BuildContext context, Widget widget) {
  return Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => widget
    )
  );
}

/// Convert a duration [offset] into a readable String and return it
String displayCountdown(BuildContext context, Duration offset, bool inDashboard) {
  String countdown = "";
  if (offset != null) {
    int d = offset.inDays;
    int h = offset.inHours - (offset.inDays * 24);
    int m = offset.inMinutes - (offset.inDays * 1440) - ((offset.inHours - (offset.inDays * 24)) * 60);
    countdown += d > 0 ? d.toString() + AppLocalizations.of(context).translate('offset-countdown-string-d') : "";
    countdown += h > 0 ? h.toString() + " h " : m > 0 && d > 0 ? " 0 h " : "";
    countdown += m > 0 ? m.toString() + " min " : "";
    if (countdown == "" || (d == 0 && h == 0 && m == 0)) {
      if (inDashboard) {
        countdown = AppLocalizations.of(context).translate('dashboard-time-imminent');
      } else {
        countdown = "0";
      }
    }
  }
  return countdown;
}

/// Determine context form [start] and [end] from now and return a readable String
String getDateDifference(BuildContext context, String start, String end) {
  DateTime nowi = DateTime.now().toUtc().add(DateTime.now().timeZoneOffset);
  Duration diff = DateTime.parse(end).toUtc().add(DateTime.now().timeZoneOffset).difference(nowi);
  Duration passed = nowi.difference(DateTime.parse(start).toUtc().add(DateTime.now().timeZoneOffset));
  if (passed.inSeconds <= 0) {
    Duration tmp = DateTime.parse(start).toUtc().add(DateTime.now().timeZoneOffset).difference(nowi);
    return AppLocalizations.of(context).translate('dashboard-start-remaining') + ' ' + displayCountdown(context, tmp, true); 
  } else if (diff.inSeconds > 0) {
    return AppLocalizations.of(context).translate('dashboard-time-remaining') + ' : ' + displayCountdown(context, diff, true); 
  } else {
    return AppLocalizations.of(context).translate('dashboard-timelimit-reached');
  }
}

/// Use the list of contacts [contacts] and return a readable String from these contacts with their names
String getActivityContacts(List<Contact> contacts) {
  String activityContacts = "";

  if (contacts.length == 0) {
    return "nobody";
  }
  for (int i = 0; i < contacts.length; i++) {
    if (i < contacts.length - 1) {
      activityContacts += contacts[i].nickname + ", ";
    } else {
      activityContacts += contacts[i].nickname;
    }
  }

  return activityContacts;
}

/// Return a list of contacts form the list [contacts]
/// Set their selected value to true if they are also present in contact list [selected]
List<Contact> contactForContactSelection(List<Contact> contacts, List<Contact> selected) {
  List<Contact> toS = [];
  if (contacts != null && selected != null) {
    contacts.forEach((element) {
      toS.add(new Contact.forSelection(
          uid: element.uid,
          nickname: element.nickname,
          subtitle: element.subtitle,
          favorite: element.favorite,
          isSelected: ((selected.where((contact) => contact.uid == element.uid)).isEmpty) ? false : true)
      );
    });
  }
  return toS;
}

/// Return a list of contacts form the list [contacts]
/// Set their selected value to true if they are also in favorites
List<Contact> favoriteContactSelection(List<Contact> contacts) {
  List<Contact> toS = [];
  if (contacts != null) {
    contacts.forEach((element) {
      if (element.favorite == true) {
        toS.add(new Contact.forSelection(
            uid: element.uid,
            nickname: element.nickname,
            subtitle: element.subtitle,
            favorite: element.favorite,
            isSelected: true
        ));
      }
    });
  }
  return toS;
}

/// Return a round number duration from [duration]
Duration secondRounder(Duration duration) {
  int roundedDuration;

  if (duration.inMilliseconds > (duration.inSeconds * 1000)) {
    roundedDuration = duration.inSeconds + 1;
  } else {
    roundedDuration = duration.inSeconds;
  }

  return new Duration(seconds: roundedDuration);
}