import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppLanguage {
  Locale _appLocale = Locale('en');

  Locale get appLocal => _appLocale ?? Locale('en');

  /// Fetch localization setup from SharedPreferences memory
  /// If no data in memory, set language to English
  fetchLocale() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString('language_code') == null) {
      _appLocale = Locale('en');
      return Null;
    }
    _appLocale = Locale(prefs.getString('language_code'));
    return Null;
  }

  /// Set application locale to [type] and save it in SharedPreferences memory
  Future<void> changeLanguage(Locale type) async {
    var prefs = await SharedPreferences.getInstance();
    if (_appLocale == type) {
      return;
    }
    if (type.toLanguageTag() == 'fr_FR') {
      _appLocale = Locale('fr');
      await prefs.setString('language_code', 'fr');
      await prefs.setString('countryCode', 'FR');
    } else {
      _appLocale = Locale('en');
      await prefs.setString('language_code', 'en');
      await prefs.setString('countryCode', 'US');
    }
  }
}