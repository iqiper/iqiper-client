import 'dart:convert';
import 'dart:io';
import 'package:diacritic/diacritic.dart';
import 'package:http/http.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/Model.dart';
import 'package:iqiperclient/app/model/ClientApi.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';

class FormValues extends Model {
  String id;
  String title;
  String message;
  int icon;

  // constraints
  bool constraintError;
  String dropdownValue;
  List<Constraint> constraintsEdit;
  List<Constraint> constraints;

  // contacts
  bool contactError;
  List<Contact> contactSelectionState; // first list
  List<Contact> contacts; // final selection

  // form
  int step = 0;

  String createdAt;
  String updatedAt;

  FormValues({
    this.title,
    this.message,
    this.icon,
    this.id,
    this.constraintError,
    this.dropdownValue,
    this.constraints,
    this.constraintsEdit,
    this.contactError,
    this.contactSelectionState,
    this.contacts,
    this.step
  });

  /// Create a new activity or activity model, depending on boolean [isModel]
  /// [id] will set the activity's id
  Activity getNewActivity(String id, bool isModel) {
    this.id = id;
    // generate new object

    Map<String, dynamic> data = toMainList();
    if (isModel) {
      Model activity = new Model.fromForm(data, 'uid');
      return activity;
    } else {
      Activity activity = new Activity.fromForm(data);
      return activity;
    }
  }
  
  /// Post this activity in ddb using client api [api]
  postNewActivity(ClientApi api) async {
    bool hasError = false;
    var response;
    String id = "";

    response = await api.post('/activity',
      json.encode(newActivityToJson(title, message, icon)),
      true, api.config.maxNumberOfRequestAttempt);

    if (response.statusCode != 201) {
      // send error to front 
      hasError = true;
      if (response.statusCode == 400) {
        response = new Response("Bad Request", 400);
      } else {
        response = new Response("Server Error", 500);
      }
    } else {
      id = json.decode(response.body)['id'].toString();
    }

    if (!hasError) {
      for (int i = 0; i < contactSelectionState.length; i++) {
        if (contactSelectionState[i].isSelected) {
          var response1 = await api.post('/activity/' + id + '/contact/' + contactSelectionState[i].uid.toString(),
            '', true, api.config.maxNumberOfRequestAttempt);
          if (response1.statusCode != 200) {
            // send error to front + delete what exists of invalid activity
            await api.delete('/activity/' + id,
              api.config.maxNumberOfRequestAttempt);
            hasError = true;
            if (response.statusCode == 400) {
              response = new Response("Bad Request", 400);
            } else {
              response = new Response("Server Error", 500);
            }
            break ;
          } 
        }
      }
    }

    if (!hasError) {
      if (constraintsEdit.length > 0) {
        for (int i = 0; i < constraintsEdit.length; i++) {
          var response2 = await api.post('/activity/' + id + '/constraint',
            json.encode(constraintsEdit[i].postConstraintToJson()),
            true, api.config.maxNumberOfRequestAttempt);
            if (response2.statusCode != 201) {
              // send error to front + delete what exists of invalid activity
              await api.delete('/activity/' + id,
                api.config.maxNumberOfRequestAttempt);
              hasError = true;
              if (response.statusCode == 400) {
                response = new Response("Bad Request", 400);
              } else {
                response = new Response("Server Error", 500);
              }
              break ;
            } else {
              constraintsEdit[i].id = json.decode(response2.body)['id'].toString();
            }
        }
      }
    }

    return response;
  }

  /// Post contact [cid] to this activity using client api [api]
  postContact(String cid, ClientApi api) async {
    return api.post('/activity/' + id + '/contact/' + cid, '',
        false, api.config.maxNumberOfRequestAttempt);
  }

  /// Post constraint from [jsonString] to this activity using client api [api]
  postConstraint(String jsonString, ClientApi api) async {
    return api.post('/activity/' + id + '/constraint', jsonString,
        false, api.config.maxNumberOfRequestAttempt);
  }

  /// Update constraint [cid] informations from [jsonString] to this activity using client api [api]
  updateActivityConstraint(String cid, String jsonString, ClientApi api) async {
    return api.put('/activity/' + id + '/constraint/' + cid,
        jsonString, api.config.maxNumberOfRequestAttempt);
  }

  /// Update name of this activity using client api [api]
  updateActivityName(ClientApi api) async {
    return api.put('/activity/' + id + '/name',
        json.encode(titleJson()), api.config.maxNumberOfRequestAttempt);
  }

  /// Update alert_message of this activity using client api [api]
  updateActivityAlertMessage(ClientApi api) async {
    return api.put('/activity/' + id + '/alert_message',
        json.encode(alertMessageJson()), api.config.maxNumberOfRequestAttempt);
  }

  /// Update icon of this activity using client api [api]
  updateActivityIcon(ClientApi api) async {
    return api.put('/activity/' + id + '/icon',
        json.encode(iconJson()), api.config.maxNumberOfRequestAttempt);
  }

  /// Delete contact [cid] from this activity using client api [api]
  deleteActivityContact(String cid, ClientApi api) async {
    return api.delete('/activity/' + id + '/contact/' + cid,
        api.config.maxNumberOfRequestAttempt);
  }

  /// Delete constraint [cid] from this activity using client api [api]
  deleteActivityConstraint(String cid, ClientApi api) async {
    return api.delete('/activity/' + id + '/constraint/' + cid,
        api.config.maxNumberOfRequestAttempt);
  }

  /// Post this activity model in ddb using client api [api]
  postNewActivityModel(ClientApi api) async {
    bool hasError = false;
    var response;
    String id = "";

    response = await api.post('/model/activity',
      json.encode(newModelToJson(title, message, icon)),
      true, api.config.maxNumberOfRequestAttempt);

    if (response.statusCode != 201) {
      // send error to front 
      hasError = true;
      if (response.statusCode == 400) {
        response = new Response("Bad Request", 400);
      } else {
        response = new Response("Server Error", 500);
      }
    } else {
      id = json.decode(response.body)['id'].toString();
    }

    if (!hasError) {
      for (int i = 0; i < contactSelectionState.length; i++) {
        if (contactSelectionState[i].isSelected) {
          var response1 = await api.post('/model/activity/' + id + '/contact/' + contactSelectionState[i].uid.toString(),
            '', true, api.config.maxNumberOfRequestAttempt);
          if (response1.statusCode != 200) {
            // send error to front + delete what exists of invalid activity
            await api.delete('/model/activity/' + id,
              api.config.maxNumberOfRequestAttempt);
            hasError = true;
            if (response.statusCode == 400) {
              response = new Response("Bad Request", 400);
            } else {
              response = new Response("Server Error", 500);
            }
            break ;
          } 
        }
      }
    }

    if (!hasError) {
      if (constraintsEdit.length > 0) {
        for (int i = 0; i < constraintsEdit.length; i++) {
          var response2 = await api.post('/model/activity/' + id + '/constraint',
          json.encode(constraintsEdit[i].postConstraintModelToJson()),
          true, api.config.maxNumberOfRequestAttempt);
          if (response2.statusCode != 201) {
            // send error to front + delete what exists of invalid activity
            await api.delete('/model/activity/' + id,
              api.config.maxNumberOfRequestAttempt);
            hasError = true;
            if (response.statusCode == 400) {
              response = new Response("Bad Request", 400);
            } else {
              response = new Response("Server Error", 500);
            }
            break ;
          } else {
            constraintsEdit[i].id = json.decode(response2.body)['id'].toString();
          }
        }
      }
    }

    return response;
  }

  /// Post constraint from [jsonString] to this activity model using client api [api]
  postConstraintToModel(String jsonString, ClientApi api) async {
    return api.post('/model/activity/' + id + '/constraint',
        jsonString, false, api.config.maxNumberOfRequestAttempt);
  }

  /// Post contact [cid] to this activity model using client api [api]
  postContactToModel(String cid, ClientApi api) async {
    return api.post('/model/activity/' + id + '/contact/' + cid,
        '', false, api.config.maxNumberOfRequestAttempt);
  }

  /// Update constraint [cid] informations from [jsonString] to this activity model using client api [api]
  updateConstraintFromModel(String cid, String jsonString, ClientApi api) async {
    return api.put('/model/activity/' + this.id + '/constraint/' + cid,
        jsonString, api.config.maxNumberOfRequestAttempt);
  }

  /// Update name of this activity model using client api [api]
  updateActivityModelName(ClientApi api) async {
    return api.put('/model/activity/' + id + '/name',
        json.encode(titleJson()), api.config.maxNumberOfRequestAttempt);
  }

  /// Update alert_message of this activity model using client api [api]
  updateActivityModelAlertMessage(ClientApi api) async {
    return api.put('/model/activity/' + id + '/alert_message',
        json.encode(alertMessageJson()), api.config.maxNumberOfRequestAttempt);
  }

  /// Update icon of this activity model using client api [api]
  updateActivityModelIcon(ClientApi api) async {
    return api.put('/model/activity/' + id + '/icon',
        json.encode(iconJson()), api.config.maxNumberOfRequestAttempt);
  }

  /// Delete contact [cid] from this activity model using client api [api]
  deleteContactFromModel(String cid, ClientApi api) async {
    return api.delete('/model/activity/' + id + '/contact/' + cid,
        api.config.maxNumberOfRequestAttempt);
  }

  /// Delete constraint [cid] from this activity model using client api [api]
  deleteConstraintFromModel(String cid, ClientApi api) async {
    return api.delete(
        '/model/activity/' + id + '/constraint/' + cid,
        api.config.maxNumberOfRequestAttempt);
  }

  Map<String, dynamic> toMainList() => {
    "id": id,
    "name": removeDiacritics(title),
    "alert_message": removeDiacritics(message),
    "icon": icon,
    "contacts": contacts.map((e) => e.postContactToJson()).toList(),
    "constraints": constraints.map((e) => e.fullConstraintToJson()).toList(),
    "updated_at" : DateTime.now().toIso8601String(),
  };

  FormValues.clone(FormValues source)
      : this.title = source.title,
        this.message = source.message,
        this.icon = source.icon,
        this.id = source.id,
        this.constraintError = source.constraintError,
        this.dropdownValue = source.dropdownValue,
        this.constraints = source.constraints
            .map((item) => new Constraint.clone(item))
            .toList(),
        this.constraintsEdit = source.constraints
            .map((item) => new Constraint.clone(item))
            .toList(),
        this.contactError = source.contactError,
        this.contactSelectionState = source.contactSelectionState
            .map((item) => new Contact.clone(item))
            .toList(),
        this.contacts = source.contacts
            .map((item) => new Contact.clone(item))
            .toList(),
        this.step = source.step;
}
