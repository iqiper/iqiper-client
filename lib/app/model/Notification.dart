class ReceivedNotification {

    ReceivedNotification({
    this.id,
    this.title,
    this.body,
    this.payload,
    this.date
  });

  final int id;
  final String title;
  final String body;
  final String payload;
  DateTime date;

}