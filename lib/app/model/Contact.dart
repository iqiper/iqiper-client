import 'dart:convert';
import 'package:diacritic/diacritic.dart';
import 'package:iqiperclient/app/model/ClientApi.dart';

class Contact {
  String uid;
  String nickname;
  String subtitle;

  String email;
  String username;
  bool favorite;

  String lastName;
  String firstName;

  bool isSelected;

  Contact({this.uid, this.nickname, this.subtitle, this.favorite});
  Contact.forUser({this.firstName, this.lastName, this.username, this.uid});
  Contact.forSelection(
      {this.uid, this.nickname, this.subtitle, this.favorite, this.isSelected});

  setNickname(String nm) {
    this.nickname = nm;
  }

  setFavorite() {
    this.favorite = !this.favorite;
  }

  setEmail(String nm) {
    this.email = nm;
  }

  setUsername(String nm) {
    this.username = nm;
  }

  setUid(String uid) {
    this.uid = uid;
  }

  setNames(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  /// Delete this contact using client api [api]
  deleteContact(ClientApi api) async {
    return api.delete('/contact/' + this.uid,
      api.config.maxNumberOfRequestAttempt);
  }

  /// Change the favorite status of this contact using client api [api]
  changeFavouriteStatus(ClientApi api) async {
    return api.put('/contact/' + this.uid + '/favorite',
      json.encode({"favorite" : !this.favorite}),
      api.config.maxNumberOfRequestAttempt);
  }

  /// Create this contact using client api [api]
  createContact(ClientApi api) async {
    return api.post('/contact', json.encode(addContactJson()),
      true, api.config.maxNumberOfRequestAttempt);
  }

  /// Change this contact's nickname to [newNickname] using client api [api]
  modifyContactNickname(String newNickname, ClientApi api) async {
    return api.put('/contact/' + uid + '/nickname',
      json.encode(_nicknameJson(newNickname)),
      api.config.maxNumberOfRequestAttempt);
  }

  /// Search this contact in ddb using client api [api]
  searchByUsername(ClientApi api) async {
    return api.get(
      '/contact/search/' + username, false,
      api.config.maxNumberOfRequestAttempt);
  }

  Map<String, dynamic> postContactToJson() => (username == null)
      ? {'uid': uid, 'nickname': removeDiacritics(nickname), 'favorite': false, 'email': removeDiacritics(subtitle)}
      : {'uid': uid, 'nickname': removeDiacritics(nickname), 'favorite': false, 'username': removeDiacritics(subtitle)};

  Map<String, dynamic> _nicknameJson(String newNickname) => {"nickname": newNickname};

  Map<String, dynamic> addContactJson() => (username == null)
      ? {'nickname': nickname, 'favorite': favorite, 'email': email}
      : {'nickname': nickname, 'favorite': favorite, 'target_uid': uid};

  Contact.clone(Contact source)
      : uid = source.uid,
        nickname = source.nickname,
        subtitle = source.subtitle,
        favorite = source.favorite,
        this.isSelected = source.isSelected;

  /// Create a new contact from [json]
  factory Contact.userFromJson(Map<String, dynamic> json) {
    return new Contact.forUser(
        firstName: json['first_name'],
        lastName: json['last_name'],
        username: json['username'],
        uid: json['id']);
  }

  /// Create a new contact from [json]
  /// [idType] is used for the id name from json data
  factory Contact.fromJson(Map<String, dynamic> json, String idType) {
    return new Contact(
      uid: json[idType],
      nickname: json['nickname'].toString(),
      subtitle: json['email'] == null
          ? ""
          // ? json['target_uid']
          : json['email'],
      favorite: json['favorite'] ?? false,
    );
  }
}
