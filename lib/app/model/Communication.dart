import 'dart:convert';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/ClientApi.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/CommunicationLog.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';

class Communication {
  String id;
  String type;
  String reason;
  String createdAt;
  Activity linkedActivity;
  Constraint linkedConstraint;
  List<CommunicationLog> logs = new List<CommunicationLog>();

  Communication({this.id, this.type, this.reason, this.createdAt, this.linkedActivity, this.linkedConstraint});
  Communication.full({this.id, this.type, this.reason, this.createdAt, this.linkedActivity, this.linkedConstraint, this.logs});

  /// Fetch linked contacts to get every logs for this communication and store the list in [logs]
  /// Using client api [api]
  fetchLogs(ClientApi api, BuildContext context) async {
    var responseContacts = await api.get('/communication/'+ this.id +'/recipient',
      false, api.config.maxNumberOfRequestAttempt);

    if (responseContacts.statusCode == 200) {
      for (String contactId in json.decode(responseContacts.body)) {
        var responseContact = await api.get('/contact/'+ contactId,
          false, api.config.maxNumberOfRequestAttempt);

        if (responseContact.statusCode == 200) {
          Contact contact = new Contact.fromJson(json.decode(responseContact.body), 'id');

          var responseLog = await api.get('/communication/'+ this.id +'/recipient/' + contactId + '/log',
            false, api.config.maxNumberOfRequestAttempt);

          if (responseLog.statusCode == 200) {
            for (var e in json.decode(responseLog.body)) {
              CommunicationLog tmpLog = logFromJson(e, contact, api);
              logs.add(tmpLog);
            }
          } else {
            if (responseLog.statusCode == 400) {
              String messageError = AppLocalizations.of(context).translate('error-occured');
              showDurationDialog(context, messageError, 1);
            } else if (responseLog.statusCode == 500) {
              Scaffold.of(context).showSnackBar(
                SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                  style: TextStyle(
                      fontSize: 18.0,
                      color: DynamicTheme.of(context).data.primaryColorLight,
                    ),
                  ),
                  backgroundColor: DynamicTheme.of(context).data.errorColor,
                )
              );
            }
          }
        } else {
          if (responseContact.statusCode == 400) {
            String messageError = AppLocalizations.of(context).translate('error-occured');
            showDurationDialog(context, messageError, 1);
          } else if (responseContact.statusCode == 500) {
            Scaffold.of(context).showSnackBar(
              SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
                style: TextStyle(
                    fontSize: 18.0,
                    color: DynamicTheme.of(context).data.primaryColorLight,
                  ),
                ),
                backgroundColor: DynamicTheme.of(context).data.errorColor,
              )
            );
          }
        }
      }
    } else {
      if (responseContacts.statusCode == 400) {
        String messageError = AppLocalizations.of(context).translate('error-occured');
        showDurationDialog(context, messageError, 1);
      } else if (responseContacts.statusCode == 500) {
        Scaffold.of(context).showSnackBar(
          SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
            style: TextStyle(
                fontSize: 18.0,
                color: DynamicTheme.of(context).data.primaryColorLight,
              ),
            ),
            backgroundColor: DynamicTheme.of(context).data.errorColor,
          )
        );
      }
    }
  }
}

/// Return a communication with data stored in json [data] and constraint [c]
/// Using the client api [api]
communicationFromJson(Map<String, dynamic> data, Activity a, Constraint c, ClientApi api) {
  return new Communication(
    id: data['id'],
    createdAt: data['created_at'],
    type: data['type'],
    reason: data['reason'],
    linkedActivity: a,
    linkedConstraint: c
  );
}