import 'dart:io';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:http/http.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/MainState.dart';
import 'package:iqiperclient/app/model/Model.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:iqiperclient/app/view_model/home.dart';
import 'dart:convert';
import 'package:iqiperclient/app/model/Config.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class MyBrowser extends ChromeSafariBrowser {
  MyBrowser();

  @override
  void onOpened() {
    close();
  }
}

class ClientApi {
  Config _config;

  final FlutterAppAuth _appAuth = FlutterAppAuth();
  final String _clientId = 'iqiper';
  final String _redirectUrl = 'com.iqiper.iqiperclient.appauth://authorize';
  final String _redirectUrlLogout = 'com.iqiper.iqiperclient.custom://logout';
  HttpClient _client;
  String _scheme;
  String _baseUrl;
  String _authorizationEndPoint;
  String _testingEndPoint;
  String _tokenEndPoint;
  final List<String> _scopes = <String>[
    'openid',
    'email',
    'offline_access',
    'profile',
    'iqiper.io/contact',
    'iqiper.io/activity',
    'iqiper.io/model.activity',
    'iqiper.io/activity.constraint',
    'iqiper.io/model.activity.constraint',
    'iqiper.io/communication',
    'iqiper.io/notification',
    'iqiper.io/self:delete',
  ];
  MainApp _mainApp;
  bool _tokenValid = false;
  bool _isWorking = false;
  String _accessToken = "";
  String _refreshToken = "";
  String _idToken = "";

  bool get tokenValid => _tokenValid;
  bool get isWorking => _isWorking;
  Config get config { return _config; }
  
  /// Setup client api to be used in production without mocker
  setupClientProd() async {
    final configJson = json.decode(await rootBundle.loadString(
      'assets/config/dev.json'
    ));
    _config = Config();
    _scheme = configJson['scheme'];
    _baseUrl = configJson['serverUrl'];
    _authorizationEndPoint = configJson['authorizationEndPoint'];
    _testingEndPoint = configJson['testingEndPoint'];
    _tokenEndPoint = configJson['tokenEndPoint'];

    ByteData data = await rootBundle.load(configJson['certificatePath']);
    SecurityContext context = SecurityContext.defaultContext;
    context.setTrustedCertificatesBytes(data.buffer.asUint8List());
    _client = HttpClient(context: context);
  }

  /// Set local mainApp to general mainApp [mainApp]
  /// Local mainApp is used to force user to login page if necessary
  void setMainAppSetState(MainApp mainApp) {
    if (_mainApp == null) {
      _mainApp = mainApp;
    }
  }

  /// Try and fetch authorization tokens from SharedPreferences memory
  /// Fetched tokens are then tested to be valid
  /// If it's not valid then refresh the token
  fetchTokens() async {
    final SharedPreferences _storage = await SharedPreferences.getInstance();
    final String _accessTokenStocked = _storage.getString('access_token');
    final String _refreshTokenStocked = _storage.getString('refresh_token');
    final String _idTokenStocked = _storage.getString('id_token');

    if (_accessTokenStocked != null && _refreshTokenStocked != null && _idTokenStocked != null) {
      _accessToken = _accessTokenStocked;
      _refreshToken = _refreshTokenStocked;
      _idToken = _idTokenStocked;

      Response testResponse = await testToken();

      if (testResponse.statusCode == 401 && _refreshToken != null) {
        await useRefreshToken();
      }
    }
  }

  /// Save the authorization token [token] informations in SharedPreferences memory
  changeTokenPrefs(TokenResponse token) async {
    final SharedPreferences _storage = await SharedPreferences.getInstance();

    _accessToken = token.accessToken;
    _refreshToken = token.refreshToken;
    _idToken = token.idToken;

    _storage.setString('access_token', _accessToken);
    _storage.setString('refresh_token', _refreshToken);
    _storage.setString('id_token', _idToken);
  }

  /// Test the authorization token with api
  /// Grant access to application if valid
  testToken() async {
    try {
      Map<String, String> headers = {
        "Content-Type": "application/json",
      };
      headers[HttpHeaders.authorizationHeader] = 'Bearer ' + _accessToken;
      http.Response response = await http.get(_testingEndPoint,
        headers: headers
      );
      if (response.statusCode == 200) {
        _tokenValid = true;
      } else {
        _tokenValid = false;
        return new http.Response("Unauthorized", 401);
      }
      return response;
    } catch (e) {
      return new http.Response("Network error", 504);
    }
  }

  /// Use the refresh token to try and get a new access token
  /// If refresh token is not valid then go back to log in page
  useRefreshToken() async {
    // TRY TO REFRESH TOKEN
    try {
      TokenResponse _token =
        await _appAuth.token(TokenRequest(_clientId, _redirectUrl,
          serviceConfiguration: AuthorizationServiceConfiguration(_authorizationEndPoint, _tokenEndPoint),
          scopes: _scopes,
          refreshToken: _refreshToken,
          grantType: GrantType.refreshToken,
          allowInsecureConnections: true)
        );
      if (_token != null) {
        // TOKEN REFRESHED
        await changeTokenPrefs(_token);
        Response response = await testToken();
        if (response.statusCode == 200) {
          return response;
        } else {
          // TOKEN NOT REFRESHED
          _tokenValid = false;
          _mainApp.changeState();
          return new http.Response("Unauthorized", 401);
        }
      } else {
        // TOKEN NOT REFRESHED - RELOG
        _tokenValid = false;
        _mainApp.changeState();
        return new http.Response("Unauthorized", 401);
      }
    } catch (err) {
      return new http.Response("Network error", 504);
    }
  }

  /// Open a webview to keycloak and use it to generate an authorization token
  /// If succeeded: save the token and test it
  login({bool preferEphemeralSession = false}) async {
    try {
      _isWorking = true;

      AuthorizationResponse authCode =
        await _appAuth.authorize(AuthorizationRequest(_clientId, _redirectUrl,
          promptValues: ['login'],
          serviceConfiguration: AuthorizationServiceConfiguration(_authorizationEndPoint, _tokenEndPoint),
          scopes: _scopes,
          additionalParameters: {"grant_type": "code"},
          allowInsecureConnections: true)
        );

      TokenResponse _token =
        await _appAuth.token(TokenRequest(_clientId, _redirectUrl,
          serviceConfiguration: AuthorizationServiceConfiguration(_authorizationEndPoint, _tokenEndPoint),
          scopes: _scopes,
          codeVerifier: authCode.codeVerifier,
          grantType: GrantType.authorizationCode,
          allowInsecureConnections: true,
          authorizationCode: authCode.authorizationCode)
        );

      if (_token != null) {
        // LOGIN SUCCESS
        await changeTokenPrefs(_token);
        // TOKEN CHANGES SUCCESS
        Response response = await testToken();
        _mainApp.changeState();
        _isWorking = false;
        return response;
      } else {
        // LOGIN FAILED
        _isWorking = false;
        return new http.Response("Bad Request", 400);
      }
    } catch (err) {
      _isWorking = false;
      return new http.Response("server not responding", 500);
    }
  }

  /// Erase all token data in SharedPreferences memory
  /// Erase all activities, models and contacts related local data
  /// Invalidate all actual tokens and go back to log in page
  logout(BuildContext context) async {
    final SharedPreferences _storage = await SharedPreferences.getInstance();
    final ChromeSafariBrowser browser = new MyBrowser();

    // CLEAN UP ALL LOCAL LISTS
    Provider.of<ActivityProvider>(context, listen: false).setList(new List<Activity>());
    Provider.of<ContactProvider>(context, listen: false).setList(new List<Contact>());
    Provider.of<ModelProvider>(context, listen: false).setList(new List<Model>());

    // ERASE ALL TOKENS IN LOCAL AND MEMORY
    _accessToken = '';
    _refreshToken = '';
    _idToken = '';

    _storage.setString('access_token', _accessToken);
    _storage.setString('refresh_token', _refreshToken);
    _storage.setString('id_token', _idToken);

    _tokenValid = false;
    _isWorking = false;
    _mainApp.changeState();

    final configJson = json.decode(await rootBundle.loadString(
      'assets/config/dev.json'
    ));
    await browser.open(url: configJson['keycloakLogoutUrl'].toString() + "?redirect_uri=" + _redirectUrlLogout);
  }

  /// Erase account from server and log out
  destroyUser(BuildContext context) async {
    // Send destroy account request
    var response = await delete('/self/destroy', config.maxNumberOfRequestAttempt);
    if (response.statusCode == 200) {
      await logout(context);
    } else {
      if (response.statusCode == 400) {
        showDurationDialog(context, AppLocalizations.of(context).translate('delete-account-fail'), 1);
      } else if (response.statusCode == 500) {
        Scaffold.of(context).showSnackBar(
          SnackBar(content: Text(
            AppLocalizations.of(context).translate('server-error'),
            style: TextStyle(
                fontSize: 18.0,
                color: DynamicTheme.of(context).data.primaryColorLight,
              ),
            ),
            backgroundColor: DynamicTheme.of(context).data.errorColor,
          )
        );
      }
    }
  }

  /// Send GET request to server
  /// [path] is actual request to make, example: '/model/activity'
  /// [query] if true, the request will demand a specific example (only used with mocker)
  /// [attemptNumber] is the current attempt to this request, if 0 then consider token is no more valid
  get(String path, bool query, int attemptNumber) async {
    try {
      Uri url = _scheme == 'http' ? new Uri.http(_baseUrl, path) : new Uri.https(_baseUrl, path);
      HttpClientRequest request = await _client.getUrl(url);

      request.headers.contentType
        = new ContentType("application", "json", charset: "utf-8");
      if (_config.skipLogin == false) {
        request.headers.add(HttpHeaders.authorizationHeader, 'Bearer ' + _accessToken);
      } else if (query == true && _config.skipLogin == true) {
        request.headers.add('X-iqiper-mocker-example', 'example_4');
      }
      HttpClientResponse response = await request.close();
      if (response.statusCode == 200 || response.statusCode == 201) {
        return new http.Response(await readResponse(response), response.statusCode, headers: {
          "content-type": "application/json; charset=utf-8"
        });
      } else if (response.statusCode == 400 || response.statusCode == 404 || response.statusCode == 409) {
        return new http.Response("Bad Request", 400, headers: {
          "content-type": "application/json; charset=utf-8"
        });
      } else if (response.statusCode == 500 || response.statusCode == 504) {
        return new http.Response("server not responding", 500, headers: {
          "content-type": "application/json; charset=utf-8"
        });
      } else {
        throw Exception('Unauthorized');
      }
    } catch (e) {
      /// If token not valable anymore try to refresh it
      if (_refreshToken != null && _config.skipLogin == false && attemptNumber > 1) {
        Response response = await useRefreshToken();
        if (response.statusCode == 200) {
          return await this.get(path, query, attemptNumber - 1);
        } else if (response.statusCode == 401) {
          _tokenValid = false;
          _mainApp.changeState();
        } else if (response.statusCode == 500 || response.statusCode == 504) {
          return new http.Response("server not responding", 500);
        } else {
          return new http.Response("Bad Request", 400);
        }
      } else if (attemptNumber <= 1 && _config.skipLogin == false) {
        /// After a few attempts if refreshing token is not working : go back to login page
        _tokenValid = false;
        _mainApp.changeState();
        return new http.Response("Unauthorized", 401);
      }
    }
  }

  /// Send POST request to server
  /// [path] is actual request to make, example: '/model/activity'
  /// [payload] is the payload (usually a json) to send with request
  /// [query] if true, the request will demand a specific example (only used with mocker)
  /// [attemptNumber] is the current attempt to this request, if 0 then consider token is no more valid
  post(String path, String payload, bool query, int attemptNumber) async {
    try {
      Uri url = _scheme == 'http' ? new Uri.http(_baseUrl, path) : new Uri.https(_baseUrl, path);
      HttpClientRequest request = await _client.postUrl(url);

      request.headers.contentType
        = new ContentType("application", "json", charset: "utf-8");
      if (_config.skipLogin == false) {
        request.headers.add(HttpHeaders.authorizationHeader, 'Bearer ' + _accessToken);
      } else if (query == true && _config.skipLogin == true) {
        request.headers.add('X-iqiper-mocker-example', 'example_4');
      }

      var encodedPayload = utf8.encode(payload);
      request.headers.contentLength = encodedPayload.length;
      request.write(payload);
      HttpClientResponse response = await request.close();
      if (response.statusCode == 200 || response.statusCode == 201) {

        return new http.Response(await readResponse(response), response.statusCode);
      } else if (response.statusCode == 400 || response.statusCode == 404 || response.statusCode == 409) {
        return new http.Response("Bad Request", 400);
      } else if (response.statusCode == 500 || response.statusCode == 504) {
        return new http.Response("server not responding", 500);
      } else {
        throw Exception('Unauthorized');
      }
    } catch (e) {
      if (_refreshToken != null && _config.skipLogin == false && attemptNumber > 1) {
        Response response = await useRefreshToken();
        if (response.statusCode == 200) {
          return await this.post(path, payload, query, attemptNumber - 1);
        } else if (response.statusCode == 401) {
          _tokenValid = false;
          _mainApp.changeState();
        } else if (response.statusCode == 500 || response.statusCode == 504) {
          return new http.Response("server not responding", 500);
        } else {
          return new http.Response("Bad Request", 400);
        }
      } else if (attemptNumber <= 1) {
        _tokenValid = false;
        _mainApp.changeState();
        return new http.Response("Unauthorized", 401);
      }
    }
  }

  /// Send PUT request to server
  /// [path] is actual request to make, example: '/model/activity'
  /// [payload] is the payload (usually a json) to send with request
  /// [attemptNumber] is the current attempt to this request, if 0 then consider token is no more valid
  put(String path, String payload, int attemptNumber) async {
    try {
      Uri url = _scheme == 'http' ? new Uri.http(_baseUrl, path) : new Uri.https(_baseUrl, path);
      HttpClientRequest request = await _client.putUrl(url);

      request.headers.contentType
        = new ContentType("application", "json", charset: "utf-8");
      if (_config.skipLogin == false) {
        request.headers.add(HttpHeaders.authorizationHeader, 'Bearer ' + _accessToken);
      }

      var encodedPayload = utf8.encode(payload);
      request.headers.contentLength = encodedPayload.length;
      request.write(payload);
      HttpClientResponse response = await request.close();

      if (response.statusCode == 200 || response.statusCode == 201) {
        return new http.Response(await readResponse(response), response.statusCode);
      } else if (response.statusCode == 400 || response.statusCode == 404 || response.statusCode == 409) {
        return new http.Response("Bad Request", 400);
      } else if (response.statusCode == 500 || response.statusCode == 504) {
        return new http.Response("server not responding", 500);
      } else {
        throw Exception('Unauthorized');
      }
    } catch (e) {
      if (_refreshToken != null && _config.skipLogin == false && attemptNumber > 1) {
        Response response = await useRefreshToken();
        if (response.statusCode == 200) {
          return await this.put(path, payload, attemptNumber - 1);
        } else if (response.statusCode == 401) {
          _tokenValid = false;
          _mainApp.changeState();
        } else if (response.statusCode == 500 || response.statusCode == 504) {
          return new http.Response("server not responding", 500);
        } else {
          return new http.Response("Bad Request", 400);
        }
      } else if (attemptNumber <= 1) {
        _tokenValid = false;
        _mainApp.changeState();
        return new http.Response("Unauthorized", 401);
      }
    }
  }

  /// Send DELETE request to server
  /// [path] is actual request to make, example: '/model/activity'
  /// [attemptNumber] is the current attempt to this request, if 0 then consider token is no more valid
  delete(String path, int attemptNumber) async {
    try {
      Uri url = _scheme == 'http' ? new Uri.http(_baseUrl, path) : new Uri.https(_baseUrl, path);
      HttpClientRequest request = await _client.deleteUrl(url);

      request.headers.contentType
        = new ContentType("application", "json", charset: "utf-8");
      if (_config.skipLogin == false) {
        request.headers.add(HttpHeaders.authorizationHeader, 'Bearer ' + _accessToken);
      }

      HttpClientResponse response = await request.close();

      if (response.statusCode == 200 || response.statusCode == 201) {
        return new http.Response(await readResponse(response), response.statusCode);
      } else if (response.statusCode == 400 || response.statusCode == 404 || response.statusCode == 409) {
        return new http.Response("Bad Request", 400);
      } else if (response.statusCode == 500 || response.statusCode == 504) {
        return new http.Response("server not responding", 500);
      } else {
        throw Exception('Unauthorized');
      }
    } catch (e) {
      if (_refreshToken != null && _config.skipLogin == false && attemptNumber > 1) {
        Response response = await useRefreshToken();
        if (response.statusCode == 200) {
          return await this.delete(path, attemptNumber - 1);
        } else if (response.statusCode == 401) {
          _tokenValid = false;
          _mainApp.changeState();
        } else if (response.statusCode == 500 || response.statusCode == 504) {
          return new http.Response("server not responding", 500);
        } else {
          return new http.Response("Bad Request", 400);
        }
      } else if (attemptNumber <= 1) {
        _tokenValid = false;
        _mainApp.changeState();
        return new http.Response("Unauthorized", 401);
      }
    }
  }
}
