import 'package:http/http.dart';
import 'package:iqiperclient/app/model/ClientApi.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'dart:convert';
import 'package:iqiperclient/app/view_model/utils/common_functions.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/Activity.dart';

class Model extends Activity {
  String id;
  String title;
  String message = "";
  int icon;
  List<Constraint> constraints = [];
  List<Contact> contacts = [];

  String createdAt;
  String updatedAt;

  Model({this.id, this.title, this.message, this.icon, this.contacts, this.constraints, this.createdAt, this.updatedAt});

  Model.clone(Model source)
    : id = source.id,
      title = source.title,
      message = source.message,
      icon = source.icon,
      constraints = source.constraints,
      contacts = source.contacts,
      createdAt = source.createdAt,
      updatedAt = source.updatedAt;
      
  /// Create a copy of this activity model and post it to server using client api [api]
  duplicateActivityModel(ClientApi api) async {
    var response = await api.post('/model/activity',
      json.encode(newModelToJson(title, message, icon)),
      true, api.config.maxNumberOfRequestAttempt);

    if (response.statusCode == 201) {
      for (Contact e in contacts) {
        await api.post('/model/activity/' + json.decode(response.body)['id'].toString() + '/contact/' + e.uid.toString(),
          '', true, api.config.maxNumberOfRequestAttempt);
      }
  
      for (Constraint e in constraints) {
        await api.post('/model/activity/' + json.decode(response.body)['id'] + '/constraint',
          json.encode(e.postConstraintModelToJson()),
          true, api.config.maxNumberOfRequestAttempt);
      }
    }
    return response;
  }

  /// Delete this activity model using client api [api]
  deleteActivityModel(ClientApi api) async {
    return api.delete('/model/activity/' + id,
        api.config.maxNumberOfRequestAttempt);
  }

  Map<String, dynamic> newModelToJson(String title, String message, int icon) => {
    "name": title,
    "alert_message": message == null ? "" : message,
    "icon": icon,
  };

  /// Create a new model activity from [json]
  /// [idType] is used for the id name from json data
  factory Model.fromForm(Map<String, dynamic> json, String idType) {
    List<Contact> activityContacts = new List<Contact>();
    List<Constraint> activityConstraints = new List<Constraint>();
    List<dynamic> newConstraint = json['constraints'];
    List<dynamic> newContact = json['contacts'];

    newContact.forEach((e) {
      activityContacts.add(new Contact.fromJson(e, idType));
    });
    newConstraint.forEach((e) {
      activityConstraints.add(new Constraint.formToList(e));
    });

    return new Model(
        id: json['id'],
        title: json['name'],
        message: json['alert_message'] == null ? "" : json['alert_message'],
        icon: json['icon'] == null ? 58740 : json['icon'],
        contacts: activityContacts,
        constraints: activityConstraints,
        createdAt: json['created_at'],
        updatedAt: json['updated_at']
    );
  }

  /// Create a new model activity from [json]
  factory Model.fromJson(Map<String, dynamic> json) {
    List<Contact> activityContacts = new List<Contact>();
    List<String> _constraintsType = new List<String>();
    List<Constraint> activityConstraints = new List<Constraint>();
    List<dynamic> newConstraint = json['constraints'];
    List<dynamic> newContact = json['contacts'];

    newConstraint.forEach((e) {
      if (e['type'] != null) {
        _constraintsType.add(e['type'].toString());
      }
    });

    newContact.forEach((e) => {
      activityContacts.add(new Contact.fromJson(e, 'id'))
    });

    activityConstraints = getConstraintsFromStringList(_constraintsType, json['constraints']);

    return new Model(
        id: json['id'],
        title: json['name'],
        message: json['alert_message'] == null ? "" : json['alert_message'],
        icon: json['icon'] == null ? 58740 : json['icon'],
        contacts: activityContacts,
        constraints: activityConstraints,
        createdAt: json['created_at'],
        updatedAt: json['updated_at']
    );
  }
}

/// Return a model with multiple requests with data stored in json [data]
/// Using the client api [api]
modelFromJson(Map<String, dynamic> data, ClientApi api) async {
  List<Contact> activityContacts = new List<Contact>();
  List<String> _constraintsType = new List<String>();

  final responseConstraint = await api.get(
    "/model/activity/" + data['id'] + "/constraint", false,
    api.config.maxNumberOfRequestAttempt);

  final responseContact = await api.get(
    "/model/activity/" + data['id'] + "/contact", false,
    api.config.maxNumberOfRequestAttempt);

  json.decode(responseConstraint.body).forEach((e) {
    if (e['type'] != null) {
      _constraintsType.add(e['type'].toString());
    }
  });

  json.decode(responseContact.body).forEach((e) {
    activityContacts.add(new Contact.fromJson(e, 'id'));
  });

  return new Model(
    id: data['id'],
    title: data['name'],
    message: data['alert_message'] == null ? "" : data['alert_message'],
    icon: data['icon'] == null ? 58740 : data['icon'],
    contacts: activityContacts,
    constraints: getConstraintsFromStringList(_constraintsType, json.decode(responseConstraint.body)),
    createdAt: data['created_at'],
    updatedAt: data['updated_at']
  );
}
