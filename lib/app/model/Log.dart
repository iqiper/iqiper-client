class Log {
  String type;
  String log;
  String constraintId;
  String createdAt;
  String updatedAt;
  String oeilId;
  String msgId;
  Log({this.type, this.log, this.constraintId, this.createdAt, this.updatedAt, this.oeilId, this.msgId});

  factory Log.fromJson(Map<String, dynamic> json) {
    return new Log(
      type: json['type'].toString(),
      log: json['log'].toString(),
      constraintId: json['constraint_id'],
      createdAt: json['created_at'].toString(),
      updatedAt: json['updated_at'].toString(),
      oeilId: json['oeil_id'],
      msgId: json['msgid'],
    );
  }
}
