import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppTheme {
  static final IqiperTheme standard = new IqiperTheme(
    name: 'Standard',
    theme: new ThemeData(
      primaryColor: Color(0xFF2E4052),
      primaryColorLight: Color(0xFFBDD9BF),
      primaryColorDark: Color(0xFF0D2134),
      accentColor: Color(0xFFFFC857),
      backgroundColor: Color(0xFF2E4052),
    ),
  );

  static final IqiperTheme greyScale = new IqiperTheme(
    name: 'Black and White',
    theme: new ThemeData(
      primaryColor: Color(0xFF6F6D6D), // +++
      primaryColorLight: Color(0xFF1B1713), // ++
      primaryColorDark: Color(0xFF9D9C9C), // ++
      accentColor: Color(0xFFFFFFFF), // +
      backgroundColor: Color(0xFF6F6D6D), // +++
      errorColor: Color(0xFFFFFFFF),  // +
    ),
  );

  String _currentThemeName = standard.name;
  ThemeData _currentTheme = standard.theme;

  String get appThemeName => _currentThemeName ?? standard.name;
  ThemeData get appTheme => _currentTheme ?? standard.theme;

  /// Fetch theme setup from SharedPreferences memory
  /// If no data in memory, set theme to standard
  fetchTheme() async {
    var prefs = await SharedPreferences.getInstance();
    _currentThemeName = prefs.getString('actual_theme') ?? standard.name;
    _currentTheme = getThemeFromName(prefs.getString('actual_theme')).theme ?? standard.theme;
  }


  /// Set application theme to [newTheme] and save it in SharedPreferences memory
  Future<void> changeTheme(IqiperTheme newTheme) async {
    var prefs = await SharedPreferences.getInstance();
    if (_currentThemeName == newTheme.name) {
      return;
    }
    _currentThemeName = newTheme.name;
    _currentTheme = newTheme.theme;
    await prefs.setString('actual_theme', newTheme.name);
  }
}

class IqiperTheme {
  String name;
  ThemeData theme;

  IqiperTheme({this.name, this.theme});
}

/// Return an IqiperTheme with the title [name]
IqiperTheme getThemeFromName(String name) {
  IqiperTheme tmp = AppTheme.standard;
  [AppTheme.standard, AppTheme.greyScale].forEach((e) {
    if (e.name == name) {
      tmp = e;
    }
  });
  return tmp;
}