import 'dart:convert';
import 'dart:ui';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:rxdart/rxdart.dart';
import 'package:iqiperclient/app/model/AlarmManager.dart' show AlarmManager;
import 'package:iqiperclient/app/model/AppLanguage.dart';
import 'package:iqiperclient/app/model/AppTheme.dart';
import 'package:iqiperclient/app/model/AppScale.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/Activity.dart';
import 'package:iqiperclient/app/model/Model.dart';
import 'package:iqiperclient/app/model/ClientApi.dart';
import 'package:iqiperclient/app/model/Notification.dart';

class SortingActivity {
  String aid;
  int endOffset;
  DateTime recall;
  SortingActivity({this.aid, this.endOffset, this.recall});
}

class ActivityProvider extends ChangeNotifier {
  MainState ms;
  ActivityProvider({this.ms});

  List<Activity> _activities = new List<Activity>();
  List<Activity> _reports = new List<Activity>();
  List<Activity> get list { return _activities; }
  List<Activity> get reportList { return _reports; }
  setList(List<Activity> activities) {  _activities = activities; }

  /// Sort the activity list and report list
  /// First to finish will be at the top
  /// Finished activities will not be in the returning list
  void sortActivityList() {
    List<Activity> tmpList = new List<Activity>();
    tmpList.addAll(_activities);

    /// get nearest constraint id list per activity
    List<SortingActivity> nearestTimeConstraintById = new List<SortingActivity>();
    List<Activity> newList = new List<Activity>();
    tmpList.forEach((activity) {
      SortingActivity temp = new SortingActivity(aid: "", endOffset: 0);
      for (int i = 0; i < activity.constraints.length; i++) {
        if (activity.constraints[i].type == "timer") {
          int tmp = DateTime.parse(activity.constraints[i].endDate).toUtc().add(DateTime.now().timeZoneOffset).difference(DateTime.now().toUtc().add(DateTime.now().timeZoneOffset)).inSeconds;
          if ((tmp < temp.endOffset && tmp > 0) || temp.endOffset == 0) {
            temp.endOffset = tmp;
          }
          if (i == activity.constraints.length - 1 && (temp.endOffset > 0)) {
            nearestTimeConstraintById.add(SortingActivity(aid: activity.id, endOffset: temp.endOffset));
            temp.endOffset = 0;
          }
        }
      }
    });
    /// sort nearestTimeConstraintById
    nearestTimeConstraintById.sort((a, b) => a.endOffset.compareTo(b.endOffset));
    /// set new ordered list
    nearestTimeConstraintById.forEach((n) {
      Activity tmpActivity = tmpList.firstWhere((element) => element.id == n.aid);

      if (tmpActivity.endDate == null) {
        newList.add(tmpActivity);
      }
    });

    /// call to update the plannification list of notifications
    notificationRefresh(newList);

    _activities = newList;
  }

  /// sorts a list of [SortingActivity] tha will be kept in [MainState]
  /// the list of DateTime [recall] contained in the object will be use 
  /// to plan the next notifications alerts
  notificationRefresh(List<Activity> activities) async {
    List<SortingActivity> nearestTimeConstraintById = new List<SortingActivity>();

    int now = DateTime.now().toUtc().add(DateTime.now().timeZoneOffset).millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond;
    activities.forEach((activity) {
      SortingActivity temp = new SortingActivity(aid: "", endOffset: 0);
      // conditions to put constraint in the notification chain :
      // not being ended and now is before dateOffset
      for (int i = 0; i < activity.constraints.length; i++) {
        if (activity.constraints[i].type == "timer") {
          int recall = DateTime.parse(activity.constraints[i].dateOffset).toUtc().add(DateTime.now().timeZoneOffset).millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond;
          int tmp = DateTime.parse(activity.constraints[i].dateOffset).toUtc().add(DateTime.now().timeZoneOffset).difference(DateTime.now().toUtc().add(DateTime.now().timeZoneOffset)).inSeconds;
          if ((recall - now) > 0) {
            if ((tmp < temp.endOffset && tmp > 0) || temp.endOffset == 0) {
              temp.endOffset = tmp;
            }
            if (i == activity.constraints.length - 1 && (temp.endOffset > 0) && activity.endDate == null) {
              nearestTimeConstraintById.add(SortingActivity(aid: activity.id, endOffset: temp.endOffset, recall: DateTime.parse(activity.constraints[i].dateOffset)));
              temp.endOffset = 0;
            }
          }
        }
      }
    });
    nearestTimeConstraintById.sort((a, b) => a.endOffset.compareTo(b.endOffset));
    if (nearestTimeConstraintById.length > 0) {
      ms.setNotifList(nearestTimeConstraintById);
      ms.notificationCareNext();
    }
  }

  ///sort the activity list from elsewhere
  sortActivityProviderList() {
    sortActivityList();
  }

  /// Fetch all activities, sort them and store them in a local list
  Future<List<Activity>> fetchAllActivities() async {
    ms.globalLoading = true;
    try {
      final response = await ms.api.get(
        "/activity", true,
        ms.api.config.maxNumberOfRequestAttempt);
      if (response.statusCode == 200) {
        _activities = new List<Activity>();
        _reports = new List<Activity>();
        for (var e in json.decode(response.body)) {
          Activity tmpActivity = await activityFromJson(e, ms.api);
          _activities.add(tmpActivity);
          _reports.add(tmpActivity);
        }
        sortActivityList();
      } 
    } catch(e) {
      throw Exception(e);
    }
    ms.globalLoading = false;
    notifyListeners();
    return _activities;
  }

  /// add to list
  add(Activity activity) async {
    _activities.add(activity);
    _reports.add(activity);
    await ms.cancelNotification();
    sortActivityList();
    notifyListeners();
  }
  /// add to report list
  addReport(Activity activity) async {
    _reports.add(activity);
    await ms.cancelNotification();
    notifyListeners();
  }
  /// change from list
  update(Activity activity) async {
    _activities.removeWhere((item) {return item.id == activity.id;});
    _reports.removeWhere((item) {return item.id == activity.id;});
    await ms.cancelNotification();
    _activities.add(activity);
    _reports.add(activity);
    sortActivityList();
    notifyListeners();
  }
  /// remove from list
  remove(String id) async {
    _activities.removeWhere((item) {return item.id == id;});
    _reports.removeWhere((item) {return item.id == id;});
    sortActivityList();
    notifyListeners();
  }
}

class ContactProvider extends ChangeNotifier {
  MainState ms;
  ContactProvider({this.ms});

  List<Contact> _contacts = new List<Contact>();
  List<Contact> get list { return _contacts; }
  setList(List<Contact> contacts) { _contacts = contacts; }

  /// Sort the contact list [list]
  /// Sort by alphabetical order
  List<Contact> sortContactList(List<Contact> list) {
    list.sort((a, b) => a.nickname.compareTo(b.nickname));
    return list;
  }

  /// Fetch all activities, sort them and store them in a local list
  Future<List<Contact>> fetchAllContacts() async {
    ms.globalLoading = true;
    try {
      final response = await ms.api.get('/contact', false,
        ms.api.config.maxNumberOfRequestAttempt);
      if (response.statusCode == 200) {
        List<Contact> teup = (json.decode(response.body) as List)
            .map((data) => new Contact.fromJson(data, 'id'))
            .toList();
        _contacts = sortContactList(teup);
      }
    } catch(e) {
      throw Exception(e);
    }
    ms.globalLoading = false;
    notifyListeners();
    return _contacts;
  }
  /// add to list
  add(Contact contact) {
    List<Contact> teup = _contacts;
    teup.add(contact);
    _contacts = sortContactList(teup);
    notifyListeners();
  }
  /// change from list
  update(Contact contact) {
    _contacts.removeWhere((item) {return item.uid == contact.uid;});
    List<Contact> teup = _contacts;
    teup.add(contact);
    _contacts = sortContactList(teup);
    notifyListeners();
  }
  /// remove from list
  remove(String id) {
    _contacts.removeWhere((item) {return item.uid == id;});
    notifyListeners();
  }
}

class ModelProvider extends ChangeNotifier {
  MainState ms;
  ModelProvider({this.ms});

  List<Model> _models = new List<Model>();
  List<Model> get list { return _models; }
  setList(List<Model> models) { _models = models; }

  /// Sort the activity model list [list]
  /// Sort by update time
  List<Model> sortModelList(List<Model> list) {
    // get nearest update_date activity
    List<SortingActivity> nearestTimeUpdateById = new List<SortingActivity>();
    list.forEach((activity) {
      int passed;
      if (activity.updatedAt != null) {
        passed = DateTime.now().difference(DateTime.parse(activity.updatedAt)).inSeconds;
      } else {
        passed = DateTime.now().difference(DateTime.parse(activity.createdAt)).inSeconds;
      }
      nearestTimeUpdateById.add(SortingActivity(aid: activity.id, endOffset: passed));
    });
    // sort nearestTimeConstraintById
    nearestTimeUpdateById.sort((a, b) => a.endOffset.compareTo(b.endOffset));
    // set new ordered list
    List<Model> newList = new List<Model>();
    nearestTimeUpdateById.forEach((n) {
      newList.add(list.firstWhere((element) => element.id == n.aid));
    });
    return newList;
  }

  /// Fetch all activity models, sort them and store them in a local list
  Future<List<Model>> fetchAllModels() async {
    ms.globalLoading = true;
    try {
      final response = await ms.api.get(
        "/model/activity", false,
        ms.api.config.maxNumberOfRequestAttempt);
      if (response.statusCode == 200) {
        List<Model> teup = new List<Model>();
        for (var e in json.decode(response.body)) {
          Model tmpModel = await modelFromJson(e, ms.api);
          teup.add(tmpModel);
        }
        _models = sortModelList(teup);
      }
    } catch(e) {
      throw Exception(e);
    }
    ms.globalLoading = false;
    notifyListeners();
    return _models;
  }

  /// add to list
  add(Model model) {
    List<Model> teup = _models;
    teup.add(model);
    _models = sortModelList(teup);
    notifyListeners();
  }
  /// change from list
  update(Model model) {
    _models.removeWhere((item) {return item.id == model.id;});
    List<Model> teup = _models;
    teup.add(model);
    _models = sortModelList(teup);
    notifyListeners();
  }
  /// remove from list
  remove(String id) {
    _models.removeWhere((item) {return item.id == id;});
    notifyListeners();
  }
}

class MainState with ChangeNotifier {
  bool globalLoading = false;
  bool navIsLoading = false;
  AppLanguage _language = AppLanguage();
  AppTheme _theme = AppTheme();
  int _selectedDrawerIndex = 0;
  ClientApi _clientApi;

  String notifOffsetKey = 'schedule_time';
  List<SortingActivity> nextNotif = new List<SortingActivity>();

  String currentTimeZone;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
  final MethodChannel platform = MethodChannel('iqiper.com/iqiper-client');
  
  
  /// Streams are created so that app can respond to notification-related events
  final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject = BehaviorSubject<ReceivedNotification>();
  final BehaviorSubject<String> selectNotificationSubject = BehaviorSubject<String>();

  AppLanguage get language { return _language; }
  AppTheme get theme { return _theme; }
  int get drawerIndex { return _selectedDrawerIndex; }
  ClientApi get api { return _clientApi; }

  Future<void> configureLocalTimeZone() async {
    currentTimeZone = await FlutterNativeTimezone.getLocalTimezone();
    tz.initializeTimeZones();
    try {
      tz.setLocalLocation(tz.getLocation(currentTimeZone)); 
    } on Exception catch (_) {
      print('Cannot set to local timezone, defaulting...');
    }
  }
  
  setupForProd() async {
    _clientApi = ClientApi();
    await _clientApi.setupClientProd();

    // No need for tokens if skipLogin is enabled
    if (_clientApi.config.skipLogin == false) {
      await _clientApi.fetchTokens();
    }
    notifyListeners();
  }

  /// initialize the [AlarmManager] class which uses the [android_alarm_manager] pluggin
  Future<bool> initAlarmSettings() async {
    bool init = await AlarmManager.init(
      exact: true,
      alarmClock: true,
      wakeup: true,
    );
    return init;
  }

  /// initialize importants [MainState] vars at launch
  initMainState() async {
    await _language.fetchLocale();
    await _theme.fetchTheme();
    await initAlarmSettings();
    notifyListeners();
  }

  /// Determine if screen is either small or big
  bool whichScale(context) {
    AppScale().init(context);
    return AppScale.screenWidth < AppScale.isBig;
  }

  setNotifList(List<SortingActivity> offsets) =>  {nextNotif = offsets};

  /// this function permit to trigger the next notifications of the [nextNotif] list
  /// after [ActivityProvider] sorts the activity list and sets it.
  /// to subscribe a notification we use the [scheduleNotification] func
  /// and the [AlarmManager] class allows the app to run its callback 
  /// if the app is running in the background
  Future<void> notificationCareNext() async {
    if (nextNotif.length > 0) {
      ReceivedNotification notif = new ReceivedNotification(
        title: "IQIPER ALARM RECALL",
        body: "You have an alert about to be send",
        date: nextNotif[0].recall
      );
      scheduleNotification(notif);
      await AlarmManager.oneShotAt(
        nextNotif[0].recall,
        1,
        (int id) => notificationCareNext(),
      );
      if (nextNotif.length > 0) {
        nextNotif.removeAt(0);
      }
    }
  }

  /// this function use is to plan a notification with the informations contained in [notification]
  /// uses the [flutter_local_notification] pluggin
  Future<void> scheduleNotification(ReceivedNotification notification) async {
    tz.TZDateTime scheduledNotificationDateTime = new tz.TZDateTime.from(notification.date, tz.local);
    AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'iqiperchannel06',
      'iqiper',
      'iqiper client notification channel',
      icon: '@drawable/iqiper_logo',
      largeIcon: DrawableResourceAndroidBitmap('@drawable/iqiper_logo'),
    );
    IOSNotificationDetails iOSPlatformChannelSpecifics = IOSNotificationDetails();
    NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics, 
      iOS: iOSPlatformChannelSpecifics
    );
    
    await flutterLocalNotificationsPlugin.zonedSchedule(
    0,
    notification.title,
    notification.body,
    scheduledNotificationDateTime,
    platformChannelSpecifics,
    androidAllowWhileIdle: true,
    uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime);
  }

  Future<void> cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancel(0);
    await AlarmManager.cancel(1);
  }

  void setDrawerIndex(int index) {
    _selectedDrawerIndex = index;
    notifyListeners();
  }

  void changeLanguage(Locale type) async {
    await _language.changeLanguage(type);
    notifyListeners();
  }

  void changeTheme(IqiperTheme newTheme) async {
    await _theme.changeTheme(newTheme);
    notifyListeners();
  }

  void isLoading() {
    globalLoading = true;
    notifyListeners();
  }

  void notLoading() {
    globalLoading = false;
    notifyListeners();
  }

  void launchAlert() {
    notifyListeners();
  }

  void dismissAlert() {
    notifyListeners();
  }
}

