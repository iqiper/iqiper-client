import 'package:diacritic/diacritic.dart';
import 'package:iqiperclient/app/view_model/utils/common_functions.dart';

class Constraint {
  String id;
  String type;
  String description = "";
  int icon;
  bool isValid = false;

  String startDate;
  String endDate;
  String dateOffset;
  int timerOffsetCheck;
  int startOffset;
  int endOffset;
  String message = "";

  Constraint({this.type, this.icon, this.description});
  Constraint.fromForm({this.type, this.icon, this.description, this.id, this.isValid, this.startDate, this.endDate, this.dateOffset, this.timerOffsetCheck, this.startOffset, this.endOffset, this.message});

  Constraint.clone(Constraint source) :
        id = source.id,
        type = source.type,
        description = source.description,
        icon = source.icon,
        startDate = source.startDate,
        endDate = source.endDate,
        dateOffset = source.dateOffset,
        timerOffsetCheck = source.timerOffsetCheck,
        startOffset = source.startOffset,
        endOffset = source.endOffset,
        message = source.message,
        isValid = source.isValid;

  setData(dynamic constraint) {
    this.type = constraint['type'] == null ? 'timer' : constraint['type'];
    this.icon = constraint['icon'] == null ? 60031 : constraint['icon'];
    this.startDate = constraint['start_date'] == null ? null : constraint['start_date'];
    this.dateOffset = constraint['notify_date'] == null ? null : constraint['notify_date'];
    this.endDate = constraint['end_date'] == null ? null : constraint['end_date'];
    this.timerOffsetCheck = constraint['notify_offset'] == null ? 0 : constraint['notify_offset'];
    this.startOffset = constraint['start_offset'] == null ? null : constraint['start_offset'];
    this.endOffset = constraint['end_offset'] == null ? null : constraint['end_offset'];
    this.message = constraint['alert_message'] == null ? null : constraint['alert_message'];
  }

  setOffset(start, end, notify) {
    this.startOffset = start;
    this.timerOffsetCheck = notify;
    this.endOffset = end;
  }

  setId(String id) {
    this.id = id;
  }

  setDate(start, end, notify) {
    this.type = "timer";
    this.startDate = start;
    this.dateOffset = notify;
    this.endDate = end;
  }

  /// Create a json from this constraint for both activities and models
  Map<String, dynamic> fullConstraintToJson() {
    return {
    "id": this.id,
    "type": this.type,
    "icon": this.icon,
    "description": this.description,
    "alert_message": removeDiacritics(this.message),
    "notify_offset": this.timerOffsetCheck,
    "start_offset": this.startOffset,
    "end_offset": this.endOffset,
    "notify_date": this.dateOffset,
    "start_date": this.startDate,
    "end_date": this.endDate,
    };
  }

  /// Create a json from this constraint for an activity model
  Map<String, dynamic> postConstraintModelToJson() => {
    "type": this.type,
    "start_offset": this.startOffset,
    "notify_offset": this.timerOffsetCheck,
    "end_offset": this.endOffset,
    "alert_message": removeDiacritics(this.message)
  };

  /// Create a json from this constraint for an activity
  Map<String, dynamic> postConstraintToJson() => {
    "type": this.type,
    "start_date": this.startDate,
    "notify_date": this.dateOffset,
    "end_date": this.endDate,
    "alert_message": removeDiacritics(this.message)
  };

  /// Create a new constraint from [json] using fromForm constructor
  factory Constraint.formToList(Map<String, dynamic> json) {
    return new Constraint.fromForm(
      type: json['type'],
      icon: json['icon'],
      description: json['description'],
      id:  json['id'],
      isValid:  json['isValid'],
      startDate:  json['start_date'],
      endDate:  json['end_date'],
      dateOffset:  json['notify_date'],
      startOffset:  json['start_offset'],
      endOffset:  json['end_offset'],
      timerOffsetCheck:  json['notify_offset'],
      message:  json['alert_message'],
    );
  }

  /// Create a new constraint from [json]
  factory Constraint.fromJson(Map<String, dynamic> json) {
    List<Constraint> c = [];
    addSelectedConstraintsWithData(c, json['type'], json);
    c[0].setId(json['id']);
    c[0].isValid = true;
    return c[0];
  }
}
