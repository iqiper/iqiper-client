import 'dart:convert';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:iqiperclient/app/model/AppLocalizations.dart';
import 'package:iqiperclient/app/model/ClientApi.dart';
import 'package:iqiperclient/app/view/custom_widget/feedBackDialog.dart';
import 'package:iqiperclient/app/view/utils/common_functions.dart';
import 'package:iqiperclient/app/view_model/utils/common_functions.dart';
import 'package:iqiperclient/app/model/Constraint.dart';
import 'package:iqiperclient/app/model/Contact.dart';
import 'package:iqiperclient/app/model/Communication.dart';

class Activity {
  String id;
  String title;
  String message = "";
  int icon;

  List<Constraint> constraints = [];
  List<Contact> contacts = [];

  String createdAt;
  String updatedAt;
  String endDate;

  Activity({this.id, this.title, this.message, this.icon, this.contacts, this.constraints});
  Activity.full({this.id, this.title, this.message, this.icon, this.contacts, this.constraints, this.createdAt, this.updatedAt, this.endDate});

  setTitle(String nm) {
    this.title = nm;
  }
  setAlertMessage(String msg) {
    this.message = msg;
  }
  setIcon(int ic) {
    this.icon = ic;
  }
  setConstraintsList(List<Constraint> c) {
    constraints = [];
    c.forEach((element) {
      constraints.add(element);
    });
  }
  setContactList(List<Contact> selectedContacts) {
    contacts = [];
    selectedContacts.forEach((element) {
      contacts.add(element);
    });
  }

  Map<String, dynamic> toJson() => {
    "name": title,
    "alert_message": message == null ? "" : message,
    "icon": icon,
    "contacts": contacts.map((e) => e.uid).toList(),
    "constraints": constraints.map((e) => e.postConstraintModelToJson()).toList(),
  };
  Map<String, dynamic> iconJson() => {
    "icon": icon
  };
  Map<String, dynamic> titleJson() => {
    "name": title
  };
  Map<String, dynamic> alertMessageJson() => {
    "alert_message": message == null ? "" : message
  };
  Map<String, dynamic> newActivityToJson(String title, String message, int icon) => {
    "name": title,
    "alert_message": message == null ? "" : message,
    "icon": icon,
  };

  /// Send an action [action] request with this activity from the following possibilities:
  /// finish, alert and alert/correct
  alertActionActivity(String action, ClientApi api) async {
    return await api.post('/activity/' + this.id + '/' + action, '',
      false, api.config.maxNumberOfRequestAttempt);
  }

  /// Delete this activity using client api [api]
  deleteActivity(ClientApi api) async {
    return await api.delete('/activity/' + this.id,
      api.config.maxNumberOfRequestAttempt);
  }

  /// Create a new activity from [json]
  factory Activity.fromForm(Map<String, dynamic> json) {
    List<Contact> activityContacts = new List<Contact>();
    List<Constraint> activityConstraints = new List<Constraint>();
    List<dynamic> newConstraint = json['constraints'];
    List<dynamic> newContact = json['contacts'];

    newContact.forEach((e) => {
      activityContacts.add(new Contact.fromJson(e, 'uid'))
    });

    newConstraint.forEach((e) {
      activityConstraints.add(new Constraint.formToList(e));
    });
    return new Activity.full(
        id: json['id'],
        title: json['name'],
        message: json['alert_message'] == null ? "" : json['alert_message'],
        icon: json['icon'] == null ? 58740 : json['icon'],
        contacts: activityContacts,
        constraints: activityConstraints,
        createdAt: json['created_at'],
        updatedAt: json['updated_at']
    );
  }

  /// Create a new activity from [json]
  factory Activity.fromJson(Map<String, dynamic> json) {
    List<Contact> activityContacts = new List<Contact>();
    List<String> _constraintsType = new List<String>();
    List<dynamic> newConstraint = json['constraints'];
    List<dynamic> newContact = json['contacts'];
    newConstraint.forEach((e) => {
      if (e['type'] != null) {
        _constraintsType.add(e['type'].toString())
      },
    });

    newContact.forEach((e) => {
      activityContacts.add(new Contact.fromJson(e, 'id'))
    });

    return new Activity.full(
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      id: json['id'],
      title: json['name'],
      message: json['alert_message'] == null ? "" : json['alert_message'],
      icon: json['icon'] == null ? 58740 : json['icon'],
      contacts: activityContacts,
      constraints: getConstraintsFromStringList(_constraintsType, json['constraints'])
    );
  }

  /// Get this activity's communications and see if it has more alert than correction
  /// Return true if there are more alert than correction
  /// Return false if there are more correction than alert or both are equal
  /// Using client api [api]
  canBeFixed(ClientApi api, BuildContext context) async {
    int alertNumber = 0;
    List<Communication> comList = new List<Communication>();
  
    var responseActivityCommunications = await api.get(
        '/activity/' + this.id + '/communication',
        false, api.config.maxNumberOfRequestAttempt);
  
    if (responseActivityCommunications.statusCode == 200) {
      for (var e in json.decode(responseActivityCommunications.body)) {
        Communication tmpCom = communicationFromJson(e, this, null, api);
        await tmpCom.fetchLogs(api, context);
        comList.add(tmpCom);
      }
    } else {
      if (responseActivityCommunications.statusCode == 400) {
        String messageError = AppLocalizations.of(context).translate('error-occured');
        showDurationDialog(context, messageError, 1);
      } else if (responseActivityCommunications.statusCode == 500) {
        Scaffold.of(context).showSnackBar(
          SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
            style: TextStyle(
                fontSize: 18.0,
                color: DynamicTheme.of(context).data.primaryColorLight,
              ),
            ),
            backgroundColor: DynamicTheme.of(context).data.errorColor,
          )
        );
      }
    }
  
    for (Constraint tmpConstraint in this.constraints) {
      var responseConstraintsCommunications = await api.get(
        '/activity/' + this.id + '/constraint/'
        + tmpConstraint.id + '/communication',
        false, api.config.maxNumberOfRequestAttempt);
  
      if (responseConstraintsCommunications.statusCode == 200) {
        for (var e in json.decode(responseConstraintsCommunications.body)) {
          Communication tmpCom = communicationFromJson(e, this, tmpConstraint, api);
          await tmpCom.fetchLogs(api, context);
          comList.add(tmpCom);
        }
      } else {
        if (responseConstraintsCommunications.statusCode == 400) {
          String messageError = AppLocalizations.of(context).translate('error-occured');
          showDurationDialog(context, messageError, 1);
        } else if (responseConstraintsCommunications.statusCode == 500) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(AppLocalizations.of(context).translate('server-error'),
              style: TextStyle(
                  fontSize: 18.0,
                  color: DynamicTheme.of(context).data.primaryColorLight,
                ),
              ),
              backgroundColor: DynamicTheme.of(context).data.errorColor,
            )
          );
        }
      }
    }

    if (comList.length > 0) {
      for (Communication com in comList) {
        if (com.reason == 'alert' || com.reason == 'forced_alert') {
          alertNumber += 1;
        } else if (com.reason == 'correction') {
          alertNumber -= 1;
        }
      }
    }

    if (alertNumber > 0) {
      return true;
    } else {
      return false;
    }
  }
}

/// Return an activity with multiple requests with data stored in json [data]
/// Using the client api [api]
activityFromJson(Map<String, dynamic> data, ClientApi api) async {
  List<Contact> activityContacts = new List<Contact>();
  List<String> _constraintsType = new List<String>();

  final responseConstraint = await api.get(
    "/activity/" + data['id'] + "/constraint", false,
    api.config.maxNumberOfRequestAttempt);

  final responseContact = await api.get(
    "/activity/" + data['id'] + "/contact", false,
    api.config.maxNumberOfRequestAttempt);

  json.decode(responseConstraint.body).forEach((e) {
    if (e['type'] != null) {
      _constraintsType.add(e['type'].toString());
    }
  });

  json.decode(responseContact.body).forEach((e) {
    activityContacts.add(new Contact.fromJson(e, 'id'));
  });

  return new Activity.full(
      createdAt: data['created_at'],
      updatedAt: data['updated_at'],
      endDate: data['end_date'],
      id: data['id'],
      title: data['name'],
      message: data['alert_message'] == null ? "" : data['alert_message'],
      icon: data['icon'] == null ? 58740 : data['icon'],
      contacts: activityContacts,
      constraints: getConstraintsFromStringList(_constraintsType, json.decode(responseConstraint.body))
    );
}