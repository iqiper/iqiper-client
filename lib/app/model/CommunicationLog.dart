import 'package:iqiperclient/app/model/ClientApi.dart';
import 'package:iqiperclient/app/model/Contact.dart';

class CommunicationLog {
  String id;
  String states;
  String createdAt;
  Contact linkedContact;

  CommunicationLog({this.id, this.states, this.createdAt, this.linkedContact});
}

/// Return a communication log with data stored in json [data] and contact [c]
/// Using the client api [api]
logFromJson(Map<String, dynamic> data, Contact c, ClientApi api) {
  return new CommunicationLog(
    id: data['id'],
    createdAt: data['created_at'],
    states: data['status'],
    linkedContact: c
  );
}