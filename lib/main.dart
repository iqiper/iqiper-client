import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:iqiperclient/app/model/Notification.dart';
import 'package:provider/provider.dart';
import 'dart:core';
import 'package:iqiperclient/app/model/MainState.dart';
import 'app/view_model/home.dart';

void main() async {
  /// needed if you intend to initialize in the `main` function
  WidgetsFlutterBinding.ensureInitialized();
  
  /// initialization of importants vars
  final _mainState = MainState();
  await _mainState.initMainState();
  await _mainState.setupForProd();
  await _mainState.configureLocalTimeZone();

  /// initialization of the [flutter_local_notification] pluggin
  final IOSInitializationSettings initializationSettingsIOS =
      IOSInitializationSettings(
          requestAlertPermission: false,
          requestBadgePermission: false,
          requestSoundPermission: false,
          onDidReceiveLocalNotification:
              (int id, String title, String body, String payload) async {
            _mainState.didReceiveLocalNotificationSubject.add(ReceivedNotification(
                id: id, title: title, body: body, payload: payload));
          });
  final AndroidInitializationSettings initializationSettingsAndroid = new AndroidInitializationSettings('@drawable/iqiper_logo');
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );

  await _mainState.flutterLocalNotificationsPlugin.initialize(
    initializationSettings,
    onSelectNotification: (String payload) async {
    _mainState.selectNotificationSubject.add(payload);
  });

  void _requestPermissions() {
    _mainState.flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }
  _requestPermissions();

  /// initialization of the [Provider] which is like a store 
  /// containing a bunch of classes accessible within the app
  final _activityProvider = ActivityProvider(ms:_mainState);
  final _contactProvider = ContactProvider(ms:_mainState);
  final _modelProvider = ModelProvider(ms:_mainState);

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<MainState>.value(value: _mainState),
        ChangeNotifierProvider<ActivityProvider>.value(value: _activityProvider),
        ChangeNotifierProvider<ContactProvider>.value(value: _contactProvider),
        ChangeNotifierProvider<ModelProvider>.value(value: _modelProvider),
      ],
      child: App(),
    )
  );
}